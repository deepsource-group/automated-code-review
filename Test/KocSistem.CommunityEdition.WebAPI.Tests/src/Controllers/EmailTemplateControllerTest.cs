﻿// <copyright file="EmailTemplateControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplateTranslation.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Notification;
using KocSistem.CommunityEdition.Common.Enums;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.EmailTemplate;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class EmailTemplateControllerTest
    {
        private readonly Mock<IKsI18N> _localizationMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IEmailTemplateService> _emailTemplateServiceMock;
        private readonly Mock<IEmailNotificationService> _emailNotificationServiceMock;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMock;

        public EmailTemplateControllerTest()
        {
            _localizationMock = new Mock<IKsI18N>();
            _emailTemplateServiceMock = new Mock<IEmailTemplateService>();
            _emailNotificationServiceMock = new Mock<IEmailNotificationService>();
            _mapperMock = new Mock<IMapper>();
            _serviceResponseHelperMock = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController")]
        public async Task Get_AllEmailTemplates_ReturnsOkObjectResultForEmptyList()
        {
            var pagedResult = new PagedResult<EmailTemplateListResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<EmailTemplateListResponse>() { new EmailTemplateListResponse() {
                    Id = Guid.NewGuid(),
                    UpdatedDate = DateTime.UtcNow,
                }, new EmailTemplateListResponse() {
                    Id = Guid.NewGuid(),
                    UpdatedDate = DateTime.UtcNow,
                } },
            };
            var pagedResultDto = new PagedResultDto<EmailTemplateDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<EmailTemplateDto>() { new EmailTemplateDto() {
                    Id = Guid.NewGuid(),
                    Name = "name1",
                    To = "test@test.com",
                    Cc = "test@test.com",
                    Bcc = "test@test.com",
                    UpdatedUser = "testUser",
                    UpdatedDate = DateTime.UtcNow,
                    Translations = new List<EmailTemplateTranslationDto> { new EmailTemplateTranslationDto { EmailContent = "anyEmailContent", Subject = "anySubject", Language = Common.Enums.LanguageType.en } }
                }, new EmailTemplateDto() {
                    Id = Guid.NewGuid(),
                    Name = "name2",
                    To = "test@test.com",
                    Cc = "test@test.com",
                    Bcc = "test@test.com",
                    UpdatedUser = "testUser",
                    UpdatedDate = DateTime.UtcNow,
                    Translations = new List<EmailTemplateTranslationDto> { new EmailTemplateTranslationDto { EmailContent = "anyEmailContent", Subject = "anySubject", Language = Common.Enums.LanguageType.en } }
                }
                }
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };
            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<EmailTemplateListResponse>>())).Returns(new ServiceResponse<PagedResult<EmailTemplateListResponse>>(new PagedResult<EmailTemplateListResponse>()));
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);
            _ = _mapperMock.Setup(s => s.Map<PagedResultDto<EmailTemplateDto>, PagedResult<EmailTemplateListResponse>>(It.IsAny<PagedResultDto<EmailTemplateDto>>())).Returns(new PagedResult<EmailTemplateListResponse>());
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var serviceResponse = new ServiceResponse<PagedResultDto<EmailTemplateDto>>(pagedResultDto);

            _ = _emailTemplateServiceMock.Setup(r => r.GetEmailTemplateListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            var controller = this.CreateEmailTemplateController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController")]
        public async Task Get_AllEmailTemplates_ReturnsOkResult()
        {
            var pagedResult = new PagedResult<EmailTemplateListResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<EmailTemplateListResponse>() { new EmailTemplateListResponse() {
                    Id = Guid.NewGuid(),
                    UpdatedDate = DateTime.UtcNow
                }, new EmailTemplateListResponse() {
                    Id = Guid.NewGuid(),
                    UpdatedDate = DateTime.UtcNow,
                }
                }
            };
            var pagedResultDto = new PagedResultDto<EmailTemplateDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<EmailTemplateDto>() { new EmailTemplateDto() {
                    Id = Guid.NewGuid(),
                    Name = "name1",
                    To = "test@test.com",
                    Cc = "test@test.com",
                    Bcc = "test@test.com",
                    UpdatedUser = "testUser",
                    UpdatedDate = DateTime.UtcNow,
                    Translations = new List<EmailTemplateTranslationDto> { new EmailTemplateTranslationDto { EmailContent = "anyEmailContent", Subject = "anySubject", Language = Common.Enums.LanguageType.en } }
                }, new EmailTemplateDto() {
                    Id = Guid.NewGuid(),
                    Name = "name2",
                    To = "test@test.com",
                    Cc = "test@test.com",
                    Bcc = "test@test.com",
                    UpdatedUser = "testUser",
                    UpdatedDate = DateTime.UtcNow,
                    Translations = new List<EmailTemplateTranslationDto> { new EmailTemplateTranslationDto { EmailContent = "anyEmailContent", Subject = "anySubject", Language = Common.Enums.LanguageType.en } }
                }
                }
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };
            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<EmailTemplateListResponse>>())).Returns(new ServiceResponse<PagedResult<EmailTemplateListResponse>>(new PagedResult<EmailTemplateListResponse>()));
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);
            _ = _mapperMock.Setup(s => s.Map<PagedResultDto<EmailTemplateDto>, PagedResult<EmailTemplateListResponse>>(It.IsAny<PagedResultDto<EmailTemplateDto>>())).Returns(new PagedResult<EmailTemplateListResponse>());
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var serviceResponse = new ServiceResponse<PagedResultDto<EmailTemplateDto>>(pagedResultDto);

            _ = _emailTemplateServiceMock.Setup(r => r.GetEmailTemplateListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            var controller = this.CreateEmailTemplateController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController")]
        public async Task Get_EmailTemplate_ReturnsNotFoundObjectResult()
        {
            EmailTemplateDto emailTemplateDto = null;
            var resp = new ServiceResponse<EmailTemplateDto>(emailTemplateDto) { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status204NoContent) };

            _ = _emailTemplateServiceMock.Setup(m => m.GetEmailTemplateByIdAsync(It.IsAny<Guid>())).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = _mapperMock.Setup(s => s.Map<EmailTemplateGetWithTranslatesResponse>(It.IsAny<EmailTemplateDto>())).Returns(new EmailTemplateGetWithTranslatesResponse());

            var controller = this.CreateEmailTemplateController();

            var response = await controller.GetAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController")]
        public async Task Get_EmailTemplateByKey_ReturnsOkResult()
        {
            var emailTemplateResponseModel = new EmailTemplateListResponse();
            var emailTemplateDto = new EmailTemplateDto
            {
                Id = Guid.NewGuid(),
                Name = "name1",
                To = "test@test.com",
                Cc = "test@test.com",
                Bcc = "test@test.com",
                UpdatedUser = "testUser",
                UpdatedDate = DateTime.UtcNow,
                Translations = new List<EmailTemplateTranslationDto> { new EmailTemplateTranslationDto { EmailContent = "anyEmailContent", Subject = "anySubject", Language = Common.Enums.LanguageType.en } }
            };
            var emailTemplateResult = new ServiceResponse<EmailTemplateDto>(emailTemplateDto);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<EmailTemplateGetWithTranslatesResponse>())).Returns(new ServiceResponse<EmailTemplateGetWithTranslatesResponse>(new EmailTemplateGetWithTranslatesResponse()));
            _ = _emailTemplateServiceMock.Setup(x => x.GetEmailTemplateByIdAsync(It.IsAny<Guid>())).Returns(Task.FromResult(emailTemplateResult));
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = _mapperMock.Setup(mock => mock.Map<EmailTemplateGetWithTranslatesResponse>(It.IsAny<EmailTemplateDto>())).Returns(new EmailTemplateGetWithTranslatesResponse());

            var controller = this.CreateEmailTemplateController();

            var response = await controller.GetAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController")]
        public async Task Put_UpdateEmailTemplate_ReturnsBadRequestObjectResult()
        {
            List<EmailTemplateDto> emailTemplateDtos = null;
            var resp = new ServiceResponse<EmailTemplateDto>(null) { Result = null, IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status400BadRequest) };

            _ = _emailTemplateServiceMock.Setup(m => m.UpdateEmailTemplateAsync(null)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = _mapperMock.Setup(mock => mock.Map<IEnumerable<EmailTemplatePutRequest>, List<EmailTemplateDto>>(It.IsAny<IEnumerable<EmailTemplatePutRequest>>())).Returns(emailTemplateDtos);

            var controller = this.CreateEmailTemplateController();

            var response = await controller.PutAsync(null).ConfigureAwait(false);

            Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code); controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController")]
        public async Task Put_UpdateEmailTemplate_ReturnsOkResult()
        {
            EmailTemplatePutRequest emailTemplatePutRequest = new()
            {
                Id = Guid.NewGuid(),
                To = "test@test.com",
                Cc = "test@test.com",
                Bcc = "test@test.com",
                Translations = new List<EmailTemplateTranslationsModel> { new EmailTemplateTranslationsModel { EmailContent = "anyEmailContent", Subject = "anySubject", Language = Common.Enums.LanguageType.en } }
            };

            _ = _emailTemplateServiceMock.Setup(m => m.UpdateEmailTemplateAsync(_mapperMock.Object.Map<EmailTemplatePutRequest, EmailTemplateDto>(emailTemplatePutRequest))).Returns(Task.FromResult(new ServiceResponse<EmailTemplateDto>(null)));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<EmailTemplateDto>())).Returns(new ServiceResponse<EmailTemplateDto>(new EmailTemplateDto()));
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var controller = this.CreateEmailTemplateController();

            var response = await controller.PutAsync(emailTemplatePutRequest).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful); controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController")]
        public async Task SendEmail_EmailTemplate_ReturnsOkResult()
        {
            SendEmailRequest emailTemplateSendEmailRequest = new() { To = "test@test.com", Subject= "Test", Content= "Test"};

            _ = _emailNotificationServiceMock.Setup(m => m.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess()).Returns(new ServiceResponse());
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var controller = this.CreateEmailTemplateController();

            var response = await controller.SendEmailAsync(emailTemplateSendEmailRequest).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController")]
        public async Task Search_EmailTemplates_ReturnsOkResult()
        {
            var req = new EmailTemplateSearchRequest()
            {
                Name = "anyEmailTemplate"
            };
            var res = new EmailTemplateListResponse()
            {
                Id = Guid.NewGuid(),
                Name = "anyName",
                UpdatedDate = DateTime.UtcNow,
                SupportedLanguages = "en"
            };

            var pagedResult = new PagedResult<EmailTemplateListResponse>()
            {
                Items = new List<EmailTemplateListResponse>() { res },
            };
            var emailTemplates = new List<EmailTemplate>() { new EmailTemplate() {
                Id = Guid.NewGuid(),
                Name = "anyName",
                To = "test@test.com.tr",
                Bcc = "test@test.com.tr",
                Cc = "test@test.com.tr",
                InsertedUser = "testUser",
                InsertedDate = DateTime.UtcNow,
                UpdatedUser = "testUser",
                UpdatedDate = DateTime.UtcNow,
                IsDeleted = false,
                Translations = new List<EmailTemplateTranslation> { new EmailTemplateTranslation { EmailContent = "anyEmailContent", Subject = "anySubject", Language = LanguageType.en } }
            } }.AsQueryable().BuildMock();
            _ = _mapperMock.Setup(s => s.Map<EmailTemplateSearchDto>(It.IsAny<EmailTemplateSearchRequest>())).Returns(new EmailTemplateSearchDto());
            _ = _serviceResponseHelperMock.Setup(s => s.SetSuccess(It.IsAny<PagedResult<EmailTemplateListResponse>>())).Returns(new ServiceResponse<PagedResult<EmailTemplateListResponse>>(new PagedResult<EmailTemplateListResponse>()));
            _ = _mapperMock.Setup(s => s.Map<PagedResultDto<EmailTemplateDto>, PagedResult<EmailTemplateListResponse>>(It.IsAny<PagedResultDto<EmailTemplateDto>>())).Returns(new PagedResult<EmailTemplateListResponse>());

            var serviceResponse = new ServiceResponse<PagedResultDto<EmailTemplateDto>>(new PagedResultDto<EmailTemplateDto>()) { IsSuccessful = true };
            _ = _emailTemplateServiceMock.Setup(r => r.SearchAsync(It.IsAny<EmailTemplateSearchDto>())).ReturnsAsync(serviceResponse);

            using var controller = this.CreateEmailTemplateController();

            var response = await controller.SearchAsync(req).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        private EmailTemplateController CreateEmailTemplateController()
        {
            var controller = new EmailTemplateController(_emailTemplateServiceMock.Object, _emailNotificationServiceMock.Object, _mapperMock.Object);

            var request = new Mock<HttpRequest>();
            request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMock.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}