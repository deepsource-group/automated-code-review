﻿// <copyright file="ApplicationSettingControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.ApplicationSetting;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class ApplicationSettingControllerTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IApplicationSettingService> _applicationSettingServiceMock;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMock;

        public ApplicationSettingControllerTest()
        {
            _applicationSettingServiceMock = new Mock<IApplicationSettingService>();
            _mapperMock = new Mock<IMapper>();
            _serviceResponseHelperMock = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Get_AllApplicationSettings_ReturnsOkObjectResultForEmptyList()
        {
            var pagedResult = new PagedResult<ApplicationSettingResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingResponse>() { new ApplicationSettingResponse() { Key = "setting2", Value = "setting2", CategoryName = "setting2" }, new ApplicationSettingResponse() { Key = "setting1", Value = "setting1", CategoryName = "setting1" } },
            };
            var pagedResultDto = new PagedResultDto<ApplicationSettingDetailDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingDetailDto>() { new ApplicationSettingDetailDto() { Key = "setting2" }, new ApplicationSettingDetailDto() { Key = "setting1" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Key", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<ApplicationSettingResponse>>())).Returns(new ServiceResponse<PagedResult<ApplicationSettingResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var responsePagedResultDto = new ServiceResponse<PagedResultDto<ApplicationSettingDetailDto>>(pagedResultDto);

            _ = _applicationSettingServiceMock.Setup(r => r.GetListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(responsePagedResultDto);

            var controller = CreateApplicationSettingController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Get_AllApplicationSettings_ReturnsOkResult()
        {
            var pagedResult = new PagedResult<ApplicationSettingResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingResponse>() { new ApplicationSettingResponse() { Key = "setting2", Value = "setting2", CategoryName = "setting2" }, new ApplicationSettingResponse() { Key = "setting1", Value = "setting1", CategoryName = "setting1" } },
            };
            var pagedResultDto = new PagedResultDto<ApplicationSettingDetailDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingDetailDto>() { new ApplicationSettingDetailDto() { Key = "setting2" }, new ApplicationSettingDetailDto() { Key = "setting1" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Key", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<ApplicationSettingResponse>>())).Returns(new ServiceResponse<PagedResult<ApplicationSettingResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var responsePagedResultDto = new ServiceResponse<PagedResultDto<ApplicationSettingDetailDto>>(pagedResultDto);

            _ = _applicationSettingServiceMock.Setup(r => r.GetListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(responsePagedResultDto);

            var controller = CreateApplicationSettingController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Get_AllApplicationSettings_ReturnsBadRequestObjectResult()
        {
            var pagedResult = new PagedResult<ApplicationSettingResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingResponse>() { new ApplicationSettingResponse() { Key = "setting2", Value = "setting2", CategoryName = "setting2" }, new ApplicationSettingResponse() { Key = "setting1", Value = "setting1", CategoryName = "setting1" } },
            };
            var pagedResultDto = new PagedResultDto<ApplicationSettingDetailDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationSettingDetailDto>() { new ApplicationSettingDetailDto() { Key = "setting2" }, new ApplicationSettingDetailDto() { Key = "setting1" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Key", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<PagedResult<ApplicationSettingResponse>>(), It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse<PagedResult<ApplicationSettingResponse>>(pagedResult, false));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var serviceResponse = new ServiceResponse<PagedResultDto<ApplicationSettingDetailDto>>(pagedResultDto, new ErrorInfo(StatusCodes.Status204NoContent), false);

            _ = _applicationSettingServiceMock.Setup(r => r.GetListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            var controller = CreateApplicationSettingController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Get_ApplicationSettingByKey_ReturnsBadRequestObjectResult()
        {
            ApplicationSettingDto applicationSettingDto = null;
            var resp = new ServiceResponse<ApplicationSettingDto>(applicationSettingDto) { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status204NoContent) };
            _ = _applicationSettingServiceMock.Setup(m => m.GetByKeyAsync(string.Empty)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);

            var controller = CreateApplicationSettingController();

            var response = await controller.GetByKeyAsync(string.Empty).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Get_ApplicationSettingByKey_ReturnsOkResult()
        {
            var applicationSettingResponseModel = new ApplicationSettingResponse();
            var applicationSettingDto = new ApplicationSettingDto { Id = Guid.NewGuid(), Key = "Key", Value = "Value", ValueType = "ValueType", CategoryId = Guid.NewGuid() };
            var applicationSettingResult = new ServiceResponse<ApplicationSettingDto>(applicationSettingDto);
            _ = _applicationSettingServiceMock.Setup(x => x.GetByKeyAsync("Key")).Returns(Task.FromResult<ServiceResponse<ApplicationSettingDto>>(applicationSettingResult));
            _ = _mapperMock.Setup(mock => mock.Map<ApplicationSettingDto, ApplicationSettingResponse>(It.IsAny<ApplicationSettingDto>())).Returns(applicationSettingResponseModel);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<ApplicationSettingResponse>())).Returns(new ServiceResponse<ApplicationSettingResponse>(new ApplicationSettingResponse()));

            var controller = CreateApplicationSettingController();

            var response = await controller.GetByKeyAsync("Key").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task GetById_ReturnsNoContentResult()
        {
            var applicationSettingDto = new ApplicationSettingDto { Id = Guid.NewGuid(), Key = "Key", Value = "Value", ValueType = "ValueType", CategoryId = Guid.NewGuid() };
            var resp = new ServiceResponse<ApplicationSettingDto>(applicationSettingDto)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _applicationSettingServiceMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(resp);

            var controller = CreateApplicationSettingController();

            var response = await controller.GetByIdAsync(applicationSettingDto.Id).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task GetById_ReturnsOkObjectResult()
        {
            var applicationSettingDto = new ApplicationSettingDto { Id = Guid.NewGuid(), Key = "Key", Value = "Value", ValueType = "ValueType", CategoryId = Guid.NewGuid() };
            var resp = new ServiceResponse<ApplicationSettingDto>(applicationSettingDto);
            _ = _applicationSettingServiceMock.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(resp);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<ApplicationSettingResponse>())).Returns(new ServiceResponse<ApplicationSettingResponse>(new ApplicationSettingResponse()));

            var controller = CreateApplicationSettingController();

            var response = await controller.GetByIdAsync(applicationSettingDto.Id).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Post_CreateApplicationSetting_ReturnsBadRequestObjectResult()
        {
            List<ApplicationSettingDto> applicationSettingDtos = null;
            var resp = new ServiceResponse<ApplicationSettingDto>(result: null, isSuccessful: false, error: new ErrorInfo(StatusCodes.Status400BadRequest));
            _ = _applicationSettingServiceMock.Setup(m => m.CreateAsync(null)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _mapperMock.Setup(mock => mock.Map<IEnumerable<ApplicationSettingPostRequest>, List<ApplicationSettingDto>>(It.IsAny<IEnumerable<ApplicationSettingPostRequest>>())).Returns(applicationSettingDtos);

            var controller = CreateApplicationSettingController();

            var response = await controller.PostAsync(null).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Post_CreateApplicationSetting_ReturnsOkResult()
        {
            var applicationSettingPostRequest = new ApplicationSettingPostRequest { Key = "Key", Value = "Value", ValueType = "ValueType", CategoryId = Guid.NewGuid() };
            var dto = _mapperMock.Object.Map<ApplicationSettingPostRequest, ApplicationSettingDto>(applicationSettingPostRequest);
            _ = _applicationSettingServiceMock.Setup(m => m.CreateAsync(dto)).Returns(Task.FromResult(new ServiceResponse<ApplicationSettingDto>(result: dto)));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<ApplicationSettingResponse>())).Returns(new ServiceResponse<ApplicationSettingResponse>(new ApplicationSettingResponse()));

            var controller = CreateApplicationSettingController();

            var response = await controller.PostAsync(applicationSettingPostRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Put_CreateApplicationSetting_ReturnsBadRequestObjectResult()
        {
            List<ApplicationSettingDto> applicationSettingDtos = null;
            var resp = new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto()) { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status400BadRequest) };
            _ = _applicationSettingServiceMock.Setup(m => m.UpdateAsync(null)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _mapperMock.Setup(mock => mock.Map<IEnumerable<ApplicationSettingPutRequest>, List<ApplicationSettingDto>>(It.IsAny<IEnumerable<ApplicationSettingPutRequest>>())).Returns(applicationSettingDtos);

            var controller = CreateApplicationSettingController();

            var response = await controller.PutAsync(null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Put_CreateApplicationSetting_ReturnsOkResult()
        {
            var applicationSettingPutRequest = new ApplicationSettingPutRequest { Id = Guid.NewGuid(), Key = "Key", Value = "Value", ValueType = "ValueType", CategoryId = Guid.NewGuid() };
            _ = _applicationSettingServiceMock.Setup(m => m.UpdateAsync(_mapperMock.Object.Map<ApplicationSettingPutRequest, ApplicationSettingDto>(applicationSettingPutRequest))).Returns(Task.FromResult(new ServiceResponse<ApplicationSettingDto>(null)));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<ApplicationSettingResponse>())).Returns(new ServiceResponse<ApplicationSettingResponse>(new ApplicationSettingResponse()));

            var controller = CreateApplicationSettingController();

            var response = await controller.PutAsync(applicationSettingPutRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Delete_CreateApplicationSettingCategory_ReturnsNoContentResult()
        {
            var resp = new ServiceResponse() { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status204NoContent) };
            _ = _applicationSettingServiceMock.Setup(m => m.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);

            var controller = CreateApplicationSettingController();

            var response = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Delete_CreateApplicationSetting_ReturnsBadRequestObjectResult()
        {
            var resp = new ServiceResponse() { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status400BadRequest) };
            _ = _applicationSettingServiceMock.Setup(m => m.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);

            var controller = CreateApplicationSettingController();

            var response = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            Assert.Equal(result.StatusCode, StatusCodes.Status400BadRequest);
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Delete_CreateApplicationSetting_ReturnsOkResult()
        {
            _ = _applicationSettingServiceMock.Setup(m => m.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ServiceResponse()));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess()).Returns(new ServiceResponse());

            var controller = CreateApplicationSettingController();

            var response = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingController")]
        public async Task Search_Success_ReturnsOkObjectResult()
        {
            var req = new ApplicationSettingSearchRequest()
            {
                PageIndex = 10,
                Key = "anykey",
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            var res = new ApplicationSettingResponse();
            var pagedResult = new PagedResult<ApplicationSettingResponse>()
            {
                Items = new List<ApplicationSettingResponse>() { res },
            };

            _ = _mapperMock.Setup(r => r.Map<PagedResultDto<ApplicationSettingDetailDto>, PagedResult<ApplicationSettingResponse>>(It.IsAny<PagedResultDto<ApplicationSettingDetailDto>>())).Returns(pagedResult);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(pagedResult)).Returns(new ServiceResponse<PagedResult<ApplicationSettingResponse>>(pagedResult));

            var serviceResponse = new ServiceResponse<PagedResultDto<ApplicationSettingDetailDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _applicationSettingServiceMock.Setup(s => s.SearchAsync(It.IsAny<ApplicationSettingSearchDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateApplicationSettingController();
            var response = await controller.SearchAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        private ApplicationSettingController CreateApplicationSettingController()
        {
            var controller = new ApplicationSettingController(_applicationSettingServiceMock.Object, _mapperMock.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMock.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}