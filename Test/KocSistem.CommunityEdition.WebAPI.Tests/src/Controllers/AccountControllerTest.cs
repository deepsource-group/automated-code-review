// -----------------------------------------------------------------------
// <copyright file="AccountControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
// -----------------------------------------------------------------------

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Account;
using KocSistem.CommunityEdition.Application.Abstractions.Account.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplateTranslation.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Notification;
using KocSistem.CommunityEdition.Application.Abstractions.User.Contracts;
using KocSistem.CommunityEdition.Common.Authentication;
using KocSistem.CommunityEdition.Common.Helpers;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.ClaimHelper;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.CommunityEdition.WebAPI.Model.Profile;
using KocSistem.CommunityEdition.WebAPI.Model.User;
using KocSistem.OneFrame.Authentication.Utilities;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

using UserClaimPostRequest = KocSistem.CommunityEdition.WebAPI.Model.User.UserClaimPostRequest;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class AccountControllerTest
    {
        private readonly Mock<IAccountService> _accountServiceMoq;
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<IEmailNotificationService> _emailNotificationServiceMock;
        private readonly Mock<IApplicationSettingService> _applicationSettingServiceMoq;
        private readonly Mock<IConfiguration> _configurationMock;

        public AccountControllerTest()
        {
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _accountServiceMoq = new Mock<IAccountService>();
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<AccountController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _emailNotificationServiceMock = new Mock<IEmailNotificationService>();
            _applicationSettingServiceMoq = new Mock<IApplicationSettingService>();
            _configurationMock = new Mock<IConfiguration>();
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task AddClaimToUser_AddClaimFailCase_ReturnsBadRequest()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.AddClaimToUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.AddClaimToUserAsync("menu", new UserClaimPostRequest() { Name = "user" }).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task AddClaimToUser_AddClaimFailCase_ReturnsNotFound()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.AddClaimToUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();

            var response = await controller.AddClaimToUserAsync("anyName", new UserClaimPostRequest() { Name = "user" }).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task AddClaimToUser_ReturnsOkResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.AddClaimToUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.AddClaimToUserAsync("menu", new UserClaimPostRequest() { Name = "user" }).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ChangePassword_ChangePasswordUnsuccessful_ReturnsBadRequest()
        {
            var roleName = Role.Admin.ToString();
            var model = new ChangePasswordRequest()
            {
                CurrentPassword = "1234567",
                NewPassword = "123456",
                NewPasswordConfirmation = "123456",
            };
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Email = "test@test.com",
                Name = "Jane",
                Surname = "Deo",
            };
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, roleName),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                new Claim(JwtRegisteredClaimNames.FamilyName, user.Surname),
            };
            using var controller = CreateUserController(claims);

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.ChangePasswordAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            var response = await controller.ChangePasswordAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "ChangePassword")]
        public async Task ChangePassword_CheckIfResetUrlIsCorrect_ReturnsOkResult()
        {
            var loginRequest = new LoginRequest()
            {
                Email = "test@test.com",
                Password = "123",
            };
            var applicationUser = new ApplicationUser() { UserName = loginRequest.Email, Email = loginRequest.Email, Name = "Jane", Surname = "Deo" };
            var roleName = Role.Admin.ToString();
            var role = new ApplicationRole(roleName);
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, applicationUser.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, applicationUser.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, role.Name),
                new Claim(JwtRegisteredClaimNames.Email, applicationUser.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, applicationUser.Name),
                new Claim(JwtRegisteredClaimNames.FamilyName, applicationUser.Surname),
            };

            var model = new ChangePasswordRequest()
            {
                CurrentPassword = "1234567",
                NewPassword = "123456",
                NewPasswordConfirmation = "123456",
            };

            using var controller = CreateUserController(claims);

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.ChangePasswordAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            var response = await controller.ChangePasswordAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ChangePassword_NonExistingUser_ReturnsNotFound()
        {
            var roleName = Role.Admin.ToString();
            var model = new ChangePasswordRequest()
            {
                CurrentPassword = "1234567",
                NewPassword = "123456",
                NewPasswordConfirmation = "123456",
            };
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Email = "test@test.com",
                Name = "Jane",
                Surname = "Deo",
            };
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, roleName),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                new Claim(JwtRegisteredClaimNames.FamilyName, user.Surname),
            };
            using var controller = CreateUserController(claims);

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.ChangePasswordAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            var response = await controller.ChangePasswordAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ChangePasswordExpired_ChangePasswordExpiredFailCase_ReturnsBadRequest()
        {
            var model = new ChangePasswordExpiredRequest()
            {
                CurrentPassword = "123456",
                UserName = "test@test.com",
                NewPassword = "654321",
                NewPasswordConfirmation = "654321",
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.ChangePasswordExpiredAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();

            var response = await controller.ChangePasswordExpiredAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ChangePasswordExpired_ChangePasswordExpiredFailCase_ReturnsNotFound()
        {
            var model = new ChangePasswordExpiredRequest()
            {
                CurrentPassword = "123456",
                UserName = "test@test.com",
                NewPassword = "654321",
                NewPasswordConfirmation = "654321",
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.ChangePasswordExpiredAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();

            var response = await controller.ChangePasswordExpiredAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ChangePasswordExpired_ChangePasswordExpiredSuccessCase_ReturnsOkObject()
        {
            var model = new ChangePasswordExpiredRequest()
            {
                CurrentPassword = "123456",
                UserName = "test@test.com",
                NewPassword = "654321",
                NewPasswordConfirmation = "654321",
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status200OK },
            };
            _ = _accountServiceMoq.Setup(r => r.ChangePasswordExpiredAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();

            var response = await controller.ChangePasswordExpiredAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status200OK, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Delete_DeleteNotSucceded_ReturnsBadRequest()
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, "anyName")
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.DeleteUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController(claims);
            var response = await controller.DeleteAsync("anyUsername").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Delete_DeleteNotSucceded_ReturnsNotFound()
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, "anyName")
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.DeleteUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController(claims);
            var response = await controller.DeleteAsync("anyUsername").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Delete_Success_ReturnsOkResult()
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, "anyName")
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.DeleteUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController(claims);

            var response = await controller.DeleteAsync("anyUsername").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "ForgotPassword")]
        public async Task ForgotPassword_CheckIfResetUrlIsCorrect_ReturnsOkResult()
        {
            var model = new ForgotPasswordRequest()
            {
                Email = "test@test.com",
            };
            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.SendResetPasswordMailAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.ForgotPasswordAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetClaimsInUser_NonExistingUser_ReturnsOkObjectResult()
        {
            var serviceResponse = new ServiceResponse<List<ClaimDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.GetClaimsInUserAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.GetClaimsInUserAsync(null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetClaimsInUser_UserHasClaims_ReturnsOkObjectResult()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
            };

            var serviceResponse = new ServiceResponse<List<ClaimDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetClaimsInUserAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimResponse>>())).Returns(new ServiceResponse<List<ClaimResponse>>(new List<ClaimResponse>()));

            using var controller = CreateUserController();

            var response = await controller.GetClaimsInUserAsync(user.UserName).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetClaimsInUser_UserHasNoClaims_ReturnsEmptyList()
        {
            var serviceResponse = new ServiceResponse<List<ClaimDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetClaimsInUserAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimResponse>>())).Returns(new ServiceResponse<List<ClaimResponse>>(new List<ClaimResponse>()));

            using var controller = CreateUserController();

            var response = await controller.GetClaimsInUserAsync("testuser").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetRoleAssignments_NotExistingSurname_ReturnsNotFound()
        {
            var serviceResponse = new ServiceResponse<List<RoleAssignmentDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.GetRoleAssignmentsAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.GetRoleAssignmentsAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetRoleAssignments_Success_ReturnsOkObjectResult()
        {
            var serviceResponse = new ServiceResponse<List<RoleAssignmentDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetRoleAssignmentsAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<RoleAssignmentResponse>>())).Returns(new ServiceResponse<List<RoleAssignmentResponse>>(new List<RoleAssignmentResponse>()));

            using var controller = CreateUserController();

            var response = await controller.GetRoleAssignmentsAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetUserClaimsTree_NonExistingUser_ReturnsOkObject()
        {
            using var controller = CreateUserController();
            var userName = "anyName";
            var serviceResponse = new ServiceResponse<List<ClaimTreeViewItemDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetUserClaimsTreeAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimTreeViewItem>>())).Returns(new ServiceResponse<List<ClaimTreeViewItem>>(new List<ClaimTreeViewItem>()));

            var response = await controller.GetUserClaimsTreeAsync(userName).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetUserClaimsTree_NullParameter_ReturnsOkObject()
        {
            using var controller = CreateUserController();
            string userName = null;
            var serviceResponse = new ServiceResponse<List<ClaimTreeViewItemDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetUserClaimsTreeAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _mapperMoq.Setup(s => s.Map<List<ClaimTreeViewItemDto>, List<ClaimTreeViewItem>>(It.IsAny<List<ClaimTreeViewItemDto>>())).Returns(new List<ClaimTreeViewItem>());
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimTreeViewItem>>())).Returns(new ServiceResponse<List<ClaimTreeViewItem>>(new List<ClaimTreeViewItem>()));

            var response = await controller.GetUserClaimsTreeAsync(userName).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetUserClaimsTree_Success_ReturnsOkObject()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Email = "test@test.com",
                Name = "Jane",
                Surname = "Deo",
            };
            using var controller = CreateUserController();
            var serviceResponse = new ServiceResponse<List<ClaimTreeViewItemDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetUserClaimsTreeAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimTreeViewItem>>())).Returns(new ServiceResponse<List<ClaimTreeViewItem>>(new List<ClaimTreeViewItem>()));
            _ = _mapperMoq.Setup(s => s.Map<List<ClaimTreeViewItemDto>, List<ClaimTreeViewItem>>(It.IsAny<List<ClaimTreeViewItemDto>>())).Returns(new List<ClaimTreeViewItem>());

            var response = await controller.GetUserClaimsTreeAsync(user.UserName).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        public async Task IsCaptchaPassedAsync_Success_ReturnsOkObject()
        {
            var captcha = "testcaptcha";
            using var controller = CreateUserController();
            var serviceResponse = new ServiceResponse<bool>(true);
            _ = _accountServiceMoq.Setup(r => r.IsCaptchaPassedAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            var response = await controller.IsCaptchaPassedAsync(captcha).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        public async Task IsCaptchaPassedAsync_NullParameter_ReturnsOkObject()
        {
            string captcha = null;
            using var controller = CreateUserController();
            var serviceResponse = new ServiceResponse<bool>(true);
            _ = _accountServiceMoq.Setup(r => r.IsCaptchaPassedAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            var response = await controller.IsCaptchaPassedAsync(captcha).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetwithPagedRequest_PagedRequestwithDescendingOrder_ReturnsOkObject()
        {
            var user1 = new ApplicationUser() { Name = "user1" };
            var user2 = new ApplicationUser() { Name = "user2" };
            var pagedResult = new PagedResult<UserGetResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<UserGetResponse>() { new UserGetResponse() { Name = user1.Name }, new UserGetResponse() { Name = user2.Name } },
            };
            var req = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };
            var users = new List<ApplicationUser>() { user1, user2 }.AsQueryable().BuildMock();
            using var controller = CreateUserController();
            var serviceResponse = new ServiceResponse<PagedResultDto<UserDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetUserListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<PagedResult<UserGetResponse>>())).Returns(new ServiceResponse<PagedResult<UserGetResponse>>(pagedResult));

            var response = await controller.GetAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultItem = (ServiceResponse<PagedResult<UserGetResponse>>)result.Value;
            Assert.Equal(2, resultItem.Result.Items.Count);
            Assert.Equal(10, resultItem.Result.PageSize);
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetwithPagedRequest_PagedRequestwithNoOrders_ReturnsOkObject()
        {
            var user1 = new ApplicationUser() { Name = "user1" };
            var user2 = new ApplicationUser() { Name = "user2" };
            var pagedResult = new PagedResult<UserGetResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<UserGetResponse>() { new UserGetResponse() { Name = user1.Name }, new UserGetResponse() { Name = user2.Name } },
            };
            var req = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
            };
            using var controller = CreateUserController();
            var serviceResponse = new ServiceResponse<PagedResultDto<UserDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetUserListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<PagedResult<UserGetResponse>>())).Returns(new ServiceResponse<PagedResult<UserGetResponse>>(pagedResult));

            var response = await controller.GetAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultItem = (ServiceResponse<PagedResult<UserGetResponse>>)result.Value;
            Assert.Equal(2, resultItem.Result.Items.Count);
            Assert.Equal(10, resultItem.Result.PageSize);
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserContoller")]
        public async Task GetWithUsername_NonExistingSurname_ReturnsNotFound()
        {
            using var controller = CreateUserController();
            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            var response = await controller.GetAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserContoller")]
        public async Task GetWithUsername_Success_ReturnsOkObject()
        {
            var user = new UserDto()
            {
                Email = "anyName",
            };
            var res = new UserGetResponse()
            {
                Username = user.Email,
            };

            using var controller = CreateUserController();
            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.GetUserByUsernameAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<UserGetResponse>())).Returns(new ServiceResponse<UserGetResponse>(res));

            var response = await controller.GetAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var loginResponse = ((ServiceResponse<UserGetResponse>)result.Value).Result;
            Assert.Equal(expected: loginResponse.Username, actual: user.Email);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Login_BuildTokenUnsuccessful_ReturnsBadRequest()
        {
            var model = new LoginRequest()
            {
                Email = "test@test.com",
                Password = "123",
            };
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Email = model.Email,
                Name = "Jane",
                Surname = "Deo",
            };
            var roleName = Role.Admin.ToString();
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, roleName),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                new Claim(JwtRegisteredClaimNames.FamilyName, user.Surname),
            };
            var accessData = new AccessToken() { Token = Guid.NewGuid().ToString(), RefreshToken = Guid.NewGuid().ToString() };
            var failedToken = new ServiceResponse<AccessToken>(accessData)
            {
                IsSuccessful = false
            };

            var serviceResponse = new ServiceResponse<LoginDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.LoginAsync(It.IsAny<LoginRequestDto>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<List<Claim>>())).Returns(new ServiceResponse<List<Claim>>(claims));
            using var controller = CreateUserController(claims);

            var response = await controller.LoginAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Login_CheckPasswordNotSucceeded_ThrowsException()
        {
            var model = new LoginRequest()
            {
                Password = "123",
            };
            var serviceResponse = new ServiceResponse<LoginDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.LoginAsync(It.IsAny<LoginRequestDto>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, false)).Returns(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.LoginAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Login_Succeeded_ReturnsOkResult()
        {
            var model = new LoginRequest()
            {
                Email = "test@test.com",
                Password = "123",
            };
            var accessData = new AccessToken() { Token = Guid.NewGuid().ToString(), RefreshToken = Guid.NewGuid().ToString() };
            var serviceResponse = new ServiceResponse<LoginDto>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.LoginAsync(It.IsAny<LoginRequestDto>())).ReturnsAsync(serviceResponse);
            var data = new LoginResponse() { Token = accessData.Token, RefreshToken = accessData.RefreshToken };
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<LoginResponse>())).Returns(new ServiceResponse<LoginResponse>(data));
            using var controller = CreateUserController(null);

            var response = await controller.LoginAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var loginResponse = ((ServiceResponse<LoginResponse>)result.Value).Result;
            Assert.Equal(expected: loginResponse.Token, actual: accessData.Token);
            Assert.Equal(expected: loginResponse.RefreshToken, actual: accessData.RefreshToken);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Post_CreateUserUnsuccessful_ReturnsBadRequest()
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, "anyName")
            };
            var req = new UserPostRequest()
            {
                Email = "test@test.com",
                Name = "Jane",
                Surname = "Deo",
                PhoneNumber = "5555555555",
            };
            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.PostAsync(It.IsAny<UserDto>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController(claims);

            var response = await controller.PostAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Post_Success_ReturnsOkObject()
        {
            var user = new UserDto()
            {
                Email = "anyName",
            };
            var req = new UserPostRequest()
            {
                Email = "test@test.com",
                Name = "Jane",
                Surname = "Deo",
                PhoneNumber = "5555555555",
            };
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, user.Email)
            };
            var serviceResponse = new ServiceResponse<UserDto>(user)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.PostAsync(It.IsAny<UserDto>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<UserGetResponse>())).Returns(new ServiceResponse<UserGetResponse>(new UserGetResponse(), true));

            using var controller = CreateUserController(claims);

            var response = await controller.PostAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse<UserGetResponse>;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Put_NotExistingSurname_ReturnsNotFound()
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, "anyName")
            };

            var req = new UserPutRequest();
            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.PutAsync(It.IsAny<string>(), It.IsAny<UserUpdateDto>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController(claims);

            var response = await controller.PutAsync("anyName", req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Put_Success_ReturnsOkObjectResult()
        {
            var roleName = Role.Admin.ToString();
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Name = "Jane",
                Surname = "Deo",
                Email = "test@test.com",
            };
            var req = new UserPutRequest()
            {
                Email = "test@test.com",
                PhoneNumber = "5555555555",
                Name = "John",
                Surname = "Deo",
            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, roleName),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                new Claim(JwtRegisteredClaimNames.FamilyName, user.Surname),
            };

            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.PutAsync(It.IsAny<string>(), It.IsAny<UserUpdateDto>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<UserGetResponse>())).Returns(new ServiceResponse<UserGetResponse>(new UserGetResponse()));

            using var controller = CreateUserController(claims);

            var response = await controller.PutAsync(user.UserName, req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Put_UpdatedResultNonSuccessful_ReturnsBadRequest()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Name = "Jane",
                Surname = "Deo",
            };

            var req = new UserPutRequest()
            {
                Email = "test@test.com",
                PhoneNumber = "5555555555",
                Name = "Jane",
                Surname = "Deo",
            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
            };

            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };

            _mapperMoq.Setup(m => m.Map<UserUpdateDto>(It.IsAny<UserPutRequest>())).Returns(new UserUpdateDto());

            _ = _accountServiceMoq.Setup(r => r.PutAsync(It.IsAny<string>(), It.IsAny<UserUpdateDto>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController(claims);

            var response = await controller.PutAsync(user.UserName, req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task PutForUserRole_AddRolesResultNonSuccessful_ReturnsBadRequest()
        {
            var roleName = Role.Admin.ToString();
            var req = new UserRolePutRequest()
            {
                Username = "anyName",
                AssignedRoles = new List<string>() { roleName },
            };
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, req.Username)
            };
            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.PutForUserRoleAsync(It.IsAny<string>(), It.IsAny<UserRoleUpdateDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController(claims);

            var response = await controller.PutForUserRoleAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task PutForUserRole_RemoveRolesResultNonSuccessful_ReturnsBadRequest()
        {
            var roleName = Role.Admin.ToString();
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Name = "Jane",
                Surname = "Deo",
                Email = "test@test.com",
            };
            var req = new UserRolePutRequest()
            {
                Username = "anyName",
                AssignedRoles = new List<string>() { roleName, Role.Guest.ToString() },
                UnassignedRoles = new List<string>() { Role.Guest.ToString() },
            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
            };

            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.PutForUserRoleAsync(It.IsAny<string>(), It.IsAny<UserRoleUpdateDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController(claims);

            var response = await controller.PutForUserRoleAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task PutForUserRole_Success_ReturnsOkObjectResult()
        {
            var roleName = Role.Admin.ToString();
            var req = new UserRolePutRequest()
            {
                Username = "anyName",
                AssignedRoles = new List<string>() { roleName, Role.Guest.ToString() },
                UnassignedRoles = new List<string>() { Role.PowerUser.ToString() },
            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, req.Username)
            };

            _ = _accountServiceMoq.Setup(r => r.PutForUserRoleAsync(It.IsAny<string>(), It.IsAny<UserRoleUpdateDto>())).ReturnsAsync(new ServiceResponse());

            using var controller = CreateUserController(claims);

            var response = await controller.PutForUserRoleAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task PutForUser_Success_ReturnsOkObjectResult()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Name = "Jane",
                Surname = "Deo",
                Email = "test@test.com",
            };
            var req = new UserPutForUserProfileRequest() { Name = user.Name, Surname = user.Surname, Email = user.Email, PhoneNumber = "5555555555" };
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
            };
            var serviceResponse = new ServiceResponse<UserDto>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.PutForUserProfileAsync(It.IsAny<string>(), It.IsAny<UserUpdateDto>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<UserGetResponse>())).Returns(new ServiceResponse<UserGetResponse>(new UserGetResponse()));

            using var controller = CreateUserController(claims);

            var response = await controller.PutForUserAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public void Refresh_Success_ReturnsOkObject()
        {
            var req = new RefreshRequest()
            {
                RefreshToken = "anyString",
                Token = "OneMoreAnyString",
            };
            var accessToken = new AccessToken() { Token = Guid.NewGuid().ToString(), RefreshToken = Guid.NewGuid().ToString() };
            using var controller = CreateUserController();
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<LoginResponse>())).Returns(new ServiceResponse<LoginResponse>(new LoginResponse()));

            var serviceResponse = new ServiceResponse<LoginDto>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.Refresh(It.IsAny<string>())).Returns(serviceResponse);

            var response = controller.Refresh(req);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public void Refresh_ValidateRefreshTokenUnsuccesful_ReturnsBadRequest()
        {
            var req = new RefreshRequest()
            {
                RefreshToken = "anyString",
            };
            var serviceResponse = new ServiceResponse<LoginDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.Refresh(It.IsAny<string>())).Returns(serviceResponse);

            using var controller = CreateUserController();

            var response = controller.Refresh(req);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Register_BuildTokenUnsuccessful_ReturnsBadRequest()
        {
            var model = new UserRegisterRequest()
            {
                Email = "test@test.com",
                Password = "123",
                Name = "Jane",
                Surname = "Deo",
                PhoneNumber = "5555555555",
            };
            var serviceResponse = new ServiceResponse<RegisterDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.RegisterAsync(It.IsAny<UserDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.RegisterAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Register_CreateUserUnsuccessful_ReturnsBadRequest()
        {
            var model = new UserRegisterRequest()
            {
                Email = "test@test.com",
                Password = "123",
                Name = "Jane",
                Surname = "Deo",
                PhoneNumber = "5555555555",
            };
            var serviceResponse = new ServiceResponse<RegisterDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.RegisterAsync(It.IsAny<UserDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.RegisterAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Register_Succeeded_ReturnsOkResult()
        {
            var model = new UserRegisterRequest()
            {
                Email = "test@test.com",
                Password = "123",
                Name = "Jane",
                Surname = "Deo",
                PhoneNumber = "5555555555",
            };
            var serviceResponse = new ServiceResponse<RegisterDto>(null)
            {
                IsSuccessful = true,
            };
            var urlResponse = new ServiceResponse<string>(string.Empty)
            {
                IsSuccessful = true
            };
            var emailTemplateResponse = new ServiceResponse<EmailTemplateTranslationDto>(null)
            {
                IsSuccessful = true,
                Result = new EmailTemplateTranslationDto { Subject = "Something", EmailContent = "Some Content" }
            };
            var loginResponse = new LoginResponse();
            _ = _accountServiceMoq.Setup(r => r.RegisterAsync(It.IsAny<UserDto>())).ReturnsAsync(serviceResponse);
            _ = _accountServiceMoq.Setup(r => r.CreateEmailActivationUrlAsync(It.IsAny<string>())).ReturnsAsync(urlResponse);
            _ = _accountServiceMoq.Setup(r => r.GetEmailActivationTemplateAsync(It.IsAny<string>())).ReturnsAsync(emailTemplateResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<LoginResponse>())).Returns(new ServiceResponse<LoginResponse>(loginResponse));

            using var controller = CreateUserController();

            var response = await controller.RegisterAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var serviceLoginResponse = ((ServiceResponse<LoginResponse>)result.Value).Result;
            Assert.IsAssignableFrom<LoginResponse>(serviceLoginResponse);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task RemoveClaimFromRole_AddToRoleIsSuccessful_ReturnsOkObjectResult()
        {
            var serviceResponse = new ServiceResponse<bool>(true)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.RemoveClaimFromUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.RemoveClaimFromUserAsync("testuser", "testclaimValue").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task RemoveClaimFromRole_AddToRoleIsUnSuccessful_ReturnsNotFound()
        {
            var serviceResponse = new ServiceResponse<bool>(false)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.RemoveClaimFromUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.RemoveClaimFromUserAsync("testuser", "testclaimValue").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task RemoveClaimFromRole_GetClaimsReturnsNull_ReturnsOkObjectResult()
        {
            var serviceResponse = new ServiceResponse<bool>(false)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.RemoveClaimFromUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.RemoveClaimFromUserAsync("testuser", "testclaim").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task RemoveClaimFromRole_NonExistingClaim_ReturnsOkObjectResult()
        {
            var serviceResponse = new ServiceResponse<bool>(false)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.RemoveClaimFromUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.RemoveClaimFromUserAsync("testuser", "testclaim").ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task RemoveClaimFromUser_NonExistingUser_ReturnsOkObjectResult()
        {
            var serviceResponse = new ServiceResponse<bool>(false)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.RemoveClaimFromUserAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();

            var response = await controller.RemoveClaimFromUserAsync("testuser", "testclaim").ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "ResetPassword")]
        public async Task Reset_Succeeded_ReturnsOkResult()
        {
            var model = new ResetPasswordRequest()
            {
                Email = "test@test.com",
                Password = "abcdef@1",
                Token = Guid.NewGuid().ToString(),
                RefreshToken = Guid.NewGuid().ToString(),
                ConfirmPassword = "abcdef@1",
            };
            var serviceResponse = new ServiceResponse<string>(null)
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.ResetPasswordAsync(It.IsAny<ResetPasswordDto>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.ResetPasswordAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ResetPassword_NonExistingUser_ReturnsNotFound()
        {
            var model = new ResetPasswordRequest()
            {
                Email = "test@test.com",
                Password = "abcdef@1",
                Token = Guid.NewGuid().ToString(),
            };
            var serviceResponse = new ServiceResponse<string>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.ResetPasswordAsync(It.IsAny<ResetPasswordDto>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.ResetPasswordAsync(model).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ResetPassword_ResetPasswordUnsuccessful_ReturnsBadRequest()
        {
            var model = new ResetPasswordRequest()
            {
                Email = "test@test.com",
                Password = "abcdef@1",
                Token = Guid.NewGuid().ToString(),
            };
            var serviceResponse = new ServiceResponse<string>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _accountServiceMoq.Setup(r => r.ResetPasswordAsync(It.IsAny<ResetPasswordDto>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.ResetPasswordAsync(model).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task SaveUserClaims_NonExistingUser_ReturnsNotFound()
        {
            var model = new SaveUserClaimsModel()
            {
                Name = "anyName",
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _accountServiceMoq.Setup(r => r.SaveUserClaimsAsync(It.IsAny<SaveUserClaimsDto>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.SaveUserClaimsAsync(model).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task SaveUserClaims_Success_ReturnsOkObject()
        {
            var model = new SaveUserClaimsModel()
            {
                Name = "anyName",
                SelectedUserClaimList = new List<string>() { KsPermissionPolicy.ManagementRoleCreate, KsPermissionPolicy.ManagementRoleClaimList },
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _accountServiceMoq.Setup(r => r.SaveUserClaimsAsync(It.IsAny<SaveUserClaimsDto>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.SaveUserClaimsAsync(model).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task Search_Success_ReturnsOkObjectResult()
        {
            var req = new UserSearchRequest()
            {
                Username = "John",
                PageIndex = 0,
                PageSize = 10,
            };
            var serviceResponse = new ServiceResponse<PagedResultDto<UserDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _mapperMoq.Setup(s => s.Map<UserSearchDto>(req)).Returns(new UserSearchDto());
            _ = _accountServiceMoq.Setup(r => r.SearchAsync(It.IsAny<UserSearchDto>())).ReturnsAsync(serviceResponse);
            _ = _mapperMoq.Setup(s => s.Map<PagedResultDto<UserDto>, PagedResult<UserGetResponse>>(It.IsAny<PagedResultDto<UserDto>>())).Returns(new PagedResult<UserGetResponse>());
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<PagedResult<UserGetResponse>>())).Returns(new ServiceResponse<PagedResult<UserGetResponse>>(new PagedResult<UserGetResponse>()));

            using var controller = CreateUserController();

            var response = await controller.SearchAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task SetProfilePhoto_IsNullModel_ReturnsBadRequest()
        {
            ProfilePhotoModel model = new()
            {
                Photo = null
            };

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, false)).Returns(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.SetProfilePhotoAsync(model).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task SetProfilePhoto_IsNullOrEmptyProfilePhoto_ReturnsBadRequest()
        {
            var model = new ProfilePhotoModel()
            {
                Photo = string.Empty,
            };

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, false)).Returns(serviceResponse);

            using var controller = CreateUserController();

            var response = await controller.SetProfilePhotoAsync(model).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task SetProfilePhoto_Success_ReturnsOkObject()
        {
            var model = new ProfilePhotoModel()
            {
                Photo = "data:image/png;base64,testBase64String"
            };

            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Name = "Jane",
                Surname = "Deo",
                Email = "test@test.com",
            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
            };
            var serviceResponse = new ServiceResponse<UserDto>(new UserDto())
            {
                IsSuccessful = true,
            };

            var serviceResponseApplicationSettings = new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto())
            {
                IsSuccessful = true,
            };

            _ = _applicationSettingServiceMoq.Setup(s => s.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto() { Value="100" }, true));
            _ = _accountServiceMoq.Setup(s => s.SetProfilePhotoAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<ServiceResponse<UserDto>>())).Returns(new ServiceResponse<ServiceResponse<UserDto>>(serviceResponse));

            using var controller = CreateUserController(claims);

            var response = await controller.SetProfilePhotoAsync(model).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task DeleteProfilePhoto_Success_ReturnsOkObject()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Name = "Jane",
                Surname = "Deo",
                Email = "test@test.com",
            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
            };
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };

            _ = _accountServiceMoq.Setup(s => s.DeleteProfilePhotoAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<ServiceResponse>())).Returns(new ServiceResponse<ServiceResponse>(serviceResponse));

            using var controller = CreateUserController(claims);

            var response = await controller.DeleteProfilePhotoAsync().ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task ConfirmEmail_Success_ReturnsOkResult()
        {
            var serviceResponse = new ServiceResponse<string>(null) { IsSuccessful = true };
            _ = _accountServiceMoq.Setup(r => r.ConfirmEmailAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.ConfirmEmailAsync("test@test.com", "token").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            Assert.Equal(200, result.StatusCode);
            Assert.Null(result.Value);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task ConfirmEmail_UserNotFound_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse<string>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo
                {
                    Code = StatusCodes.Status204NoContent,
                }
            };
            _ = _accountServiceMoq.Setup(r => r.ConfirmEmailAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.ConfirmEmailAsync("test@test.com", "token").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var responseDetail = (ServiceResponse)result.Value;
            Assert.Equal(200, result.StatusCode);
            Assert.Equal(204, responseDetail.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task ConfirmEmail_EmailAlreadyConfirmed_ReturnsBadRequestResult()
        {
            var serviceResponse = new ServiceResponse<string>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo
                {
                    Code = StatusCodes.Status400BadRequest,
                }
            };
            _ = _accountServiceMoq.Setup(r => r.ConfirmEmailAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.ConfirmEmailAsync("test@test.com", "token").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var responseDetail = (ServiceResponse)result.Value;
            Assert.Equal(200, result.StatusCode);
            Assert.Equal(400, responseDetail.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task DeleteProfilePhoto_IsNullUser_ReturnsBadRequest()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };

            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Name = "Jane",
                Surname = "Deo",
                Email = "test@test.com",
            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
            };

            _ = _accountServiceMoq.Setup(s => s.DeleteProfilePhotoAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, false)).Returns(serviceResponse);

            using var controller = CreateUserController(claims);

            var response = await controller.DeleteProfilePhotoAsync().ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public void ExternalLogin_Success_ReturnsChallengeResult()
        {
            var properties = new AuthenticationProperties { AllowRefresh = true };
            var serviceResponse = new ServiceResponse<AuthenticationProperties>(properties) { IsSuccessful = true };
            _ = _accountServiceMoq.Setup(r => r.GetAuthenticationProperties(It.IsAny<string>(), It.IsAny<string>())).Returns(serviceResponse);
            using var controller = CreateUserController();
            var response = controller.ExternalLogin("facebook", string.Empty);

            _ = Assert.IsAssignableFrom<ChallengeResult>(response);
            var result = (ChallengeResult)response;
            Assert.Equal(properties, result.Properties);
        }

        [Fact]
        [Trait("Category", "AccountController")]
        public async Task ExternalLoginCallback_Success_ReturnsRedirect()
        {
            var loginDto = new LoginDto { Token = It.IsAny<string>(), RefreshToken=It.IsAny<string>() };
            var serviceResponse = new ServiceResponse<LoginDto>(loginDto) { IsSuccessful = true };
            _ = _accountServiceMoq.Setup(r => r.ExternalLoginAsync()).ReturnsAsync(serviceResponse);
            using var controller = CreateUserController();
            var response = await controller.ExternalLoginCallbackAsync(string.Empty).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<RedirectResult>(response);
            var result = (RedirectResult)response;
            Assert.Contains("token=&refreshToken", result.Url);
        }

        private AccountController CreateUserController(List<Claim> claims = null)
        {
            var controller = new AccountController(
                emailNotificationService: _emailNotificationServiceMock.Object,
                mapper: _mapperMoq.Object,
                accountService: _accountServiceMoq.Object,
                i18N: _localizationMoq.Object,
                applicationSettingService: _applicationSettingServiceMoq.Object,
                configuration: _configurationMock.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMoq.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            return controller;
        }
    }
}