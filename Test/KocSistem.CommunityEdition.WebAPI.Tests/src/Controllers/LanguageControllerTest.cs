﻿using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Language;
using KocSistem.CommunityEdition.Application.Abstractions.Language.Contracts;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.Language;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class LanguageControllerTest
    {
        private readonly Mock<IKsI18N> _localizationMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<ILanguageService> _languageServiceMock;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMock;

        public LanguageControllerTest()
        {
            _localizationMock = new Mock<IKsI18N>();
            _languageServiceMock = new Mock<ILanguageService>();            
            _mapperMock = new Mock<IMapper>();
            _serviceResponseHelperMock = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Get_AllLanguages_ReturnsOkObjectResultForEmptyList()
        {
            var pagedResultDto = new PagedResultDto<LanguageDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<LanguageDto>() { 
                new LanguageDto() {
                    Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                    Name = "English",
                    Code = "en-En",
                    Image = "",
                    IsActive = true,
                    IsDefault = true, 
                    UpdatedUser = "testUser",
                    UpdatedDate = DateTime.UtcNow,
                }, new LanguageDto() {
                    Id = new Guid("ddec791a-d71f-4c07-91d7-20e4eea2ad7a"),
                    Name = "Türkçe",
                    Code = "tr-TR",
                    Image = "",
                    IsActive = true,
                    IsDefault = true,
                    UpdatedUser = "testUser",
                    UpdatedDate = DateTime.UtcNow,
                }
            }
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };
            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };
            
            var serviceResponse = new ServiceResponse<PagedResultDto<LanguageDto>>(pagedResultDto);

            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);
            _ = _mapperMock.Setup(s => s.Map<PagedResultDto<LanguageDto>, PagedResult<LanguageListResponse>>(It.IsAny<PagedResultDto<LanguageDto>>())).Returns(new PagedResult<LanguageListResponse>());
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<LanguageListResponse>>())).Returns(new ServiceResponse<PagedResult<LanguageListResponse>>(new PagedResult<LanguageListResponse>()));
            _ = _languageServiceMock.Setup(r => r.GetLanguageListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);            

            var controller = this.CreateLanguageController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }


        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Get_AllLanguages_ReturnsOkResult()
        {
            var pagedResultDto = new PagedResultDto<LanguageDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<LanguageDto>() {
                new LanguageDto() {
                    Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                    Name = "English",
                    Code = "en-En",
                    Image = "",
                    IsActive = true,
                    IsDefault = true,
                    UpdatedUser = "testUser",
                    UpdatedDate = DateTime.UtcNow,
                }, new LanguageDto() {
                    Id = new Guid("ddec791a-d71f-4c07-91d7-20e4eea2ad7a"),
                    Name = "Türkçe",
                    Code = "tr-TR",
                    Image = "",
                    IsActive = true,
                    IsDefault = true,
                    UpdatedUser = "testUser",
                    UpdatedDate = DateTime.UtcNow,
                }
            }
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };
            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };

            var serviceResponse = new ServiceResponse<PagedResultDto<LanguageDto>>(pagedResultDto);

            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);
            _ = _mapperMock.Setup(s => s.Map<PagedResultDto<LanguageDto>, PagedResult<LanguageListResponse>>(It.IsAny<PagedResultDto<LanguageDto>>())).Returns(new PagedResult<LanguageListResponse>());
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<LanguageListResponse>>())).Returns(new ServiceResponse<PagedResult<LanguageListResponse>>(new PagedResult<LanguageListResponse>()));
            _ = _languageServiceMock.Setup(r => r.GetLanguageListAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            var controller = this.CreateLanguageController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Get_Language_ReturnsNotFoundObjectResult()
        {
            LanguageDto LanguageDto = null;
            var resp = new ServiceResponse<LanguageDto>(LanguageDto) { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status204NoContent) };

            _ = _languageServiceMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);            
            _ = _mapperMock.Setup(s => s.Map<LanguageGetByIdResponse>(It.IsAny<LanguageDto>())).Returns(new LanguageGetByIdResponse());

            var controller = this.CreateLanguageController();

            var response = await controller.GetAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Get_Language_ReturnsOkResult()
        {
            var languageGetByIdResponse = new LanguageGetByIdResponse();
            var LanguageDto = new LanguageDto
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,
                UpdatedUser = "testUser",
                UpdatedDate = DateTime.UtcNow,
            };

            var languageResult = new ServiceResponse<LanguageDto>(LanguageDto);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<LanguageGetByIdResponse>())).Returns(new ServiceResponse<LanguageGetByIdResponse>(new LanguageGetByIdResponse()));
            _ = _languageServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).Returns(Task.FromResult(languageResult));            
            _ = _mapperMock.Setup(mock => mock.Map<LanguageGetByIdResponse>(It.IsAny<LanguageDto>())).Returns(new LanguageGetByIdResponse());

            var controller = this.CreateLanguageController();

            var response = await controller.GetAsync(LanguageDto.Id).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Post_Language_ReturnsBadRequestObjectResult()
        {
            List<LanguageDto> LanguageDtos = null;
            var resp = new ServiceResponse<LanguageDto>(new LanguageDto()) { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status400BadRequest) };
            _ = _languageServiceMock.Setup(m => m.CreateAsync(null)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _mapperMock.Setup(mock => mock.Map<IEnumerable<LanguagePostRequest>, List<LanguageDto>>(It.IsAny<IEnumerable<LanguagePostRequest>>())).Returns(LanguageDtos);

            var controller = CreateLanguageController();

            var response = await controller.PostAsync(null).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Post_Language_ReturnsOkResult()
        {
            var languagePostRequest = new LanguagePostRequest {                
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,
            };

            var dto = _mapperMock.Object.Map<LanguagePostRequest, LanguageDto>(languagePostRequest);
            _ = _languageServiceMock.Setup(m => m.CreateAsync(dto)).Returns(Task.FromResult(new ServiceResponse<LanguageDto>(result: dto)));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<LanguageResponse>())).Returns(new ServiceResponse<LanguageResponse>(new LanguageResponse()));

            var controller = CreateLanguageController();

            var response = await controller.PostAsync(languagePostRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category","LanguageController")]
        public async Task Put_Language_ReturnsBadRequestObjectResult()
        {
            List<LanguageDto> LanguageDtos = null;
            var resp = new ServiceResponse<LanguageDto>(new LanguageDto()) { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status400BadRequest) };
            _ = _languageServiceMock.Setup(m => m.UpdateAsync(null)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
            _ = _mapperMock.Setup(mock => mock.Map<IEnumerable<LanguagePutRequest>, List<LanguageDto>>(It.IsAny<IEnumerable<LanguagePutRequest>>())).Returns(LanguageDtos);

            var controller = CreateLanguageController();

            var response = await controller.PutAsync(null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Put_UpdateEmailTemplate_ReturnsOkResult()
        {
            LanguagePutRequest languagePutRequest = new()
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "",
                IsActive = true,
                IsDefault = true,
            };

            _ = _languageServiceMock.Setup(m => m.UpdateAsync(_mapperMock.Object.Map<LanguagePutRequest, LanguageDto>(languagePutRequest))).Returns(Task.FromResult(new ServiceResponse<LanguageDto>(null)));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<LanguageResponse>())).Returns(new ServiceResponse<LanguageResponse>(new LanguageResponse()));


            var controller = this.CreateLanguageController();

            var response = await controller.PutAsync(languagePutRequest).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful); 
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Delete_CreateLanguage_ReturnsBadRequestObjectResult()
        {
            var resp = new ServiceResponse() { IsSuccessful = false, Error = new ErrorInfo(StatusCodes.Status400BadRequest) };
            _ = _languageServiceMock.Setup(m => m.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);

            var controller = CreateLanguageController();

            var response = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            Assert.Equal(result.StatusCode, StatusCodes.Status400BadRequest);
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Delete_Language_ReturnsOkResult()
        {
            _ = _languageServiceMock.Setup(m => m.DeleteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(new ServiceResponse()));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess()).Returns(new ServiceResponse());

            var controller = CreateLanguageController();

            var response = await controller.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LanguageController")]
        public async Task Search_Success_ReturnsOkObjectResult()
        {
            var req = new LanguageSearchRequest()
            {
                PageIndex = 10,
                Key = "anykey",
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            var res = new LanguageListResponse();
            var pagedResult = new PagedResult<LanguageListResponse>()
            {
                Items = new List<LanguageListResponse>() { res },
            };

            _ = _mapperMock.Setup(r => r.Map<PagedResultDto<LanguageDto>, PagedResult<LanguageListResponse>>(It.IsAny<PagedResultDto<LanguageDto>>())).Returns(pagedResult);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(pagedResult)).Returns(new ServiceResponse<PagedResult<LanguageListResponse>>(pagedResult));

            var serviceResponse = new ServiceResponse<PagedResultDto<LanguageDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _languageServiceMock.Setup(s => s.SearchAsync(It.IsAny<LanguageSearchDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateLanguageController();
            var response = await controller.SearchAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            controller.Dispose();
        }

        private LanguageController CreateLanguageController()
        {
            var controller = new LanguageController(_languageServiceMock.Object, _mapperMock.Object);

            var request = new Mock<HttpRequest>();
            request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMock.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}
