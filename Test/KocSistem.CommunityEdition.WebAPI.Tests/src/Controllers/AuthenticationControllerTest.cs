// -----------------------------------------------------------------------
// <copyright file="AuthenticationControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
// -----------------------------------------------------------------------

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Account;
using KocSistem.CommunityEdition.Application.Abstractions.Account.Contracts;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.Authentication;
using KocSistem.CommunityEdition.WebAPI.Model.User;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;


namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class AuthenticationControllerTest
    {
        private readonly Mock<IAuthenticationService> _authenticationServiceMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;

        public AuthenticationControllerTest()
        {
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _authenticationServiceMoq = new Mock<IAuthenticationService>();
        }

        [Fact]
        [Trait("Category", "AuthenticationController")]
              public async Task SendVerificationCode_ReturnsOkResult()
        {
            var serviceResponse = new ServiceResponse<string>(null, true);
            _ = _authenticationServiceMoq.Setup(r => r.SendVerificationCodeAsync(It.IsAny<TwoFactorVerificationDto>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ServiceResponse>())).Returns(new ServiceResponse<ServiceResponse>(new ServiceResponse()));

            using var controller = CreateAuthenticationController();

            var response = await controller.SendVerificationCodeAsync(It.IsAny<TwoFactorVerificationRequest>()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AuthenticationController")]
        public async Task SendVerificationCode_ReturnsBadRequest()
        {
            var serviceResponse = new ServiceResponse<string>(null, new ErrorInfo() { Code = StatusCodes.Status400BadRequest }, false);
            _ = _authenticationServiceMoq.Setup(r => r.SendVerificationCodeAsync(It.IsAny<TwoFactorVerificationDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateAuthenticationController();

            var response = await controller.SendVerificationCodeAsync(It.IsAny<TwoFactorVerificationRequest>()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "AuthenticationController")]
        public async Task GenerateAuthenticatorSharedKey_ReturnsOkResult()
        {
            _ = _authenticationServiceMoq.Setup(r => r.GenerateAuthenticatorSharedKeyAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<AuthenticatorResponseDto>(new AuthenticatorResponseDto()));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<AuthenticatorResponse>())).Returns(new ServiceResponse<AuthenticatorResponse>(new AuthenticatorResponse()));

            using var controller = CreateAuthenticationController();

            var response = await controller.GenerateAuthenticatorSharedKeyAsync(It.IsAny<string>()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AuthenticationController")]
        public async Task GenerateAuthenticatorSharedKey_ReturnsBadRequest()
        {

            _ = _authenticationServiceMoq.Setup(r => r.GenerateAuthenticatorSharedKeyAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<AuthenticatorResponseDto>(new AuthenticatorResponseDto()) { IsSuccessful = false });

            using var controller = CreateAuthenticationController();

            var response = await controller.GenerateAuthenticatorSharedKeyAsync(It.IsAny<string>()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AuthenticationController")]
        public async Task TwoFactorVerification_ReturnsOkResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _authenticationServiceMoq.Setup(r => r.TwoFactorVerificationAsync(It.IsAny<TwoFactorVerificationDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateAuthenticationController();

            var response = await controller.TwoFactorVerificationAsync(It.IsAny<TwoFactorVerificationRequest>()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
        }

        [Fact]
        [Trait("Category", "AuthenticationController")]
        public async Task TwoFactorVerification_ReturnsBadRequest()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _authenticationServiceMoq.Setup(r => r.TwoFactorVerificationAsync(It.IsAny<TwoFactorVerificationDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateAuthenticationController();

            var response = await controller.TwoFactorVerificationAsync(It.IsAny<TwoFactorVerificationRequest>()).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        private AuthenticationController CreateAuthenticationController()
        {
            var controller = new AuthenticationController(
                mapper: _mapperMoq.Object,
                authenticationService: _authenticationServiceMoq.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMoq.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}
