﻿// <copyright file="UserControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.User;
using KocSistem.CommunityEdition.Application.Abstractions.User.Contracts;
using KocSistem.CommunityEdition.Common.Helpers;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.User;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;
using KocSistem.CommunityEdition.WebAPI.Model.ClaimHelper;
using KocSistem.CommunityEdition.Application.Abstractions.UserConfirmationHistory.Contract;
using KocSistem.CommunityEdition.Common.Enums;
using KocSistem.OneFrame.Notification.Sms.Abstractions;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class UserControllerTest
    {
        private readonly Mock<IClaimManager> _claimManagerMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<IUserService> _userServiceMoq;

        public UserControllerTest()
        {
            _userServiceMoq = new Mock<IUserService>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _mapperMoq = new Mock<IMapper>();
            _claimManagerMoq = new Mock<IClaimManager>();
        }

        [Fact]
        [Trait("Category", "UserController")]
        public void GetUserClaims_UserHasClaims_ReturnsOkObjectResult()
        {
            var user = new ApplicationUser() { UserName = "anyName" };
            var basicUserInfoResponse = new BasicUserInfoResponse()
            {
                Email = "test@test.com",
                Name = "John",
                Surname = "Doe",
                PhoneNumber = "0000",
            };
            var roleName = Role.Admin.ToString();
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, roleName),
            };
            _ = _userServiceMoq.Setup(s => s.GetCurrentUserInfoAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<UserDto>(new UserDto()));
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimResponse>>())).Returns(new ServiceResponse<List<ClaimResponse>>(new List<ClaimResponse>()));
            using var controller = CreateUserController(claims);

            _ = _claimManagerMoq.Setup(s => s.GetClaims()).Returns(claims);

            var response = controller.GetUserClaims();

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetUserInfoAsync_GetCurrentUserInfoUnsuccessful_ReturnsNotFound()
        {
            var user = new ApplicationUser() { UserName = "anyName" };
            var roleName = Role.Admin.ToString();
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, roleName),
            };
            _ = _userServiceMoq.Setup(s => s.GetCurrentUserInfoAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<UserDto>(new UserDto(), new ErrorInfo()));
            using var controller = CreateUserController(claims);

            var response = await controller.GetUserInfoAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            // TODO: null exception
            //Assert.False(resultResponse.IsSuccessful);
            //Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetUserInfoAsync_Success_ReturnsOkObject()
        {
            var user = new ApplicationUser() { UserName = "anyName" };
            var basicUserInfoResponse = new BasicUserInfoResponse()
            {
                Email = "test@test.com",
                Name = "John",
                Surname = "Doe",
                PhoneNumber = "0000",
            };
            var roleName = Role.Admin.ToString();
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, roleName),
            };
            _ = _userServiceMoq.Setup(s => s.GetCurrentUserInfoAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<UserDto>(new UserDto()));
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<BasicUserInfoResponse>())).Returns(new ServiceResponse<BasicUserInfoResponse>(basicUserInfoResponse));
            using var controller = CreateUserController(claims);

            var response = await controller.GetUserInfoAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var userInfoResponse = ((ServiceResponse<BasicUserInfoResponse>)result.Value).Result;

            Assert.Equal(basicUserInfoResponse.PhoneNumber, userInfoResponse.PhoneNumber);
            Assert.Equal(basicUserInfoResponse.Surname, userInfoResponse.Surname);
            Assert.Equal(basicUserInfoResponse.Email, userInfoResponse.Email);
            Assert.Equal(basicUserInfoResponse.Name, userInfoResponse.Name);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetConfirmationCode_ConfirmationCodeIsNull_ReturnsBadRequest()
        {
            var userModel = new UserDto()
            {
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                Email = "test@test.com",
                PhoneNumber = "0000000000"
            };

            var confirmationHistoryDto = new UserConfirmationHistoryDto();
            var errorModel = new ErrorInfo() { Code = StatusCodes.Status400BadRequest };

            _ = _userServiceMoq
                .Setup(s => s.GetCurrentUserInfoAsync(It.IsAny<string>()))
                .ReturnsAsync(new ServiceResponse<UserDto>(userModel));

            _ = _userServiceMoq
                .Setup(s => s.GetConfirmationCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new ServiceResponse<UserConfirmationHistoryDto>(confirmationHistoryDto, errorModel));

            using var controller = CreateUserController();

            var identity = new ClaimsIdentity(authenticationType: "Test");
            var claimsPrincipal = new ClaimsPrincipal(identity: identity);
            var mockPrincipal = new Mock<IPrincipal>();
            _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);

            controller.ControllerContext.HttpContext.User = claimsPrincipal;

            var response = await controller.GetConfirmationCodeAsync("0000000000").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.Equal(400, resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task GetConfirmationCode_Success_ReturnsOkObject()
        {
            var userModel = new UserDto()
            {
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                Email = "test@test.com",
                PhoneNumber = "0000000000"
            };

            var expiredDate = DateTime.UtcNow.AddMinutes(2);
            var confirmationHistoryModel = new UserConfirmationHistoryDto()
            {
                Code = "123456",
                CodeType = (int)ConfirmationType.PhoneNumber,
                ExpiredDate = expiredDate,
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                IsUsed = false,
                PhoneNumber = "0000000000",
                UserId = Guid.Parse("00000000-0000-0000-0000-000000000000")
            };

            var confirmationCodeResponseModel = new ConfirmationCodeResponse()
            {
                Code = "123456",
                ExpiredDate = expiredDate,
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                PhoneNumber = "0000000000",
            };

            _userServiceMoq
                .Setup(s => s.GetCurrentUserInfoAsync(It.IsAny<string>()))
                .ReturnsAsync(new ServiceResponse<UserDto>(userModel));

            _ = _userServiceMoq
                .Setup(s => s.GetConfirmationCodeAsync(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new ServiceResponse<UserConfirmationHistoryDto>(confirmationHistoryModel));

            _ = _serviceResponseHelperMoq
                .Setup(s => s.SetSuccess(It.IsAny<ConfirmationCodeResponse>()))
                .Returns(new ServiceResponse<ConfirmationCodeResponse>(confirmationCodeResponseModel));

            using var controller = CreateUserController();

            var identity = new ClaimsIdentity(authenticationType: "Test");
            var claimsPrincipal = new ClaimsPrincipal(identity: identity);
            var mockPrincipal = new Mock<IPrincipal>();
            _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);

            controller.ControllerContext.HttpContext.User = claimsPrincipal;

            var response = await controller.GetConfirmationCodeAsync("0000000000").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse<ConfirmationCodeResponse>;
            Assert.True(resultResponse.IsSuccessful);
            Assert.Equal(confirmationHistoryModel.Code, resultResponse.Result.Code);
            Assert.Equal(confirmationHistoryModel.ExpiredDate, resultResponse.Result.ExpiredDate);
            Assert.Equal(confirmationHistoryModel.Id, resultResponse.Result.Id);
            Assert.Equal(confirmationHistoryModel.PhoneNumber, resultResponse.Result.PhoneNumber);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task SendCode_Success_ReturnsOkObject()
        {
            var expiredDate = DateTime.UtcNow.AddMinutes(2);
            var confirmationHistoryModel = new UserConfirmationHistoryDto()
            {
                Code = "123456",
                CodeType = (int)ConfirmationType.PhoneNumber,
                ExpiredDate = expiredDate,
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                IsUsed = false,
                PhoneNumber = "0000000000",
                UserId = Guid.Parse("00000000-0000-0000-0000-000000000000")
            };

            _ = _userServiceMoq.Setup(s => s.SendConfirmationCodeAsync(It.IsAny<UserConfirmationHistoryDto>())).ReturnsAsync(new ServiceResponse<SendSmsResponse>(null));

            _ = _userServiceMoq.Setup(s => s.CheckConfirmationCodeAsync(It.IsAny<UserConfirmationHistoryDto>())).ReturnsAsync(new ServiceResponse());

            using var controller = CreateUserController();

            var response = await controller.SendCodeAsync(confirmationHistoryModel).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ConfirmCode_Success_ReturnOkObject()
        {
            var confirmationRequest = new ConfirmationCodeRequest()
            {
                Code = "123456",
                Id = Guid.Parse("00000000-0000-0000-0000-000000000000"),
                PhoneNumber = "0000000000"
            };

            _ = _userServiceMoq.Setup(s => s.ConfirmCodeAsync(It.IsAny<UserConfirmationHistoryDto>())).ReturnsAsync(new ServiceResponse(null));

            _ = _userServiceMoq.Setup(s => s.CheckUserConfirmationAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse(null));

            using var controller = CreateUserController();

            var response = await controller.ConfirmCodeAsync(confirmationRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        [Trait("Category", "UserController")]
        public async Task ConfirmCode_CodeIsNull_ReturnBadRequestObject()
        {
            var confirmationRequest = new ConfirmationCodeRequest()
            {
                Code = "123456",
                Id = Guid.Parse("00000000-0000-0000-0000-000000000000"),
                PhoneNumber = "0000000000"
            };

            _ = _userServiceMoq
                .Setup(s => s.ConfirmCodeAsync(It.IsAny<UserConfirmationHistoryDto>()))
                .ReturnsAsync(new ServiceResponse(new ErrorInfo() { Code = StatusCodes.Status400BadRequest }, false));

            _ = _userServiceMoq.Setup(s => s.CheckUserConfirmationAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse(null));

            using var controller = CreateUserController();

            var response = await controller.ConfirmCodeAsync(confirmationRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.Equal(400, resultResponse.Error.Code);
        }

        private UserController CreateUserController(List<Claim> claims = null)
        {
            var controller = new UserController(
                userService: _userServiceMoq.Object,
                mapper: _mapperMoq.Object,
                claimManager: _claimManagerMoq.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMoq.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            return controller;
        }
    }
}