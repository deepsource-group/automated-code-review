﻿// <copyright file="ConfigurationControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.FileUpload;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class ConfigurationControllerTest
    {
        private readonly IOptions<FileUploaderConfigurationOptions> _options;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<IApplicationSettingService> _applicationSettingService;

        public ConfigurationControllerTest()
        {
            _options = Options.Create(new FileUploaderConfigurationOptions());
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _applicationSettingService = new Mock<IApplicationSettingService>();
        }

        [Fact]
        [Trait("Category", "ConfigurationController")]
        public async Task GetFileUploaderOptions_ReturnsOkResult()
        {
            using var controller = CreateConfigurationController(_options);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<FileUploaderConfigurationOptions>())).Returns(new ServiceResponse< FileUploaderConfigurationOptions>(new FileUploaderConfigurationOptions()));
            
            var response = await controller.GetFileUploaderOptionsAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        private ConfigurationController CreateConfigurationController(IOptions<FileUploaderConfigurationOptions> options = null)
        {
            var controller = new ConfigurationController(options: options, _applicationSettingService.Object);
            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMoq.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}