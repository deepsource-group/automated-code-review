﻿// <copyright file="EmailNotificationControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Notification;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class EmailNotificationControllerTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IEmailNotificationService> _emailNotificationServiceMock;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMock;

        public EmailNotificationControllerTest()
        {
            _emailNotificationServiceMock = new Mock<IEmailNotificationService>();
            _mapperMock = new Mock<IMapper>();
            _serviceResponseHelperMock = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "EmailNotificationController")]
        public async Task Get_AllEmailNotifications_ReturnsOkObjectResultForEmptyList()
        {
            var pagedResult = new PagedResult<EmailNotificationResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<EmailNotificationResponse>() { new EmailNotificationResponse() { Subject = "Subject", To = "To", Body = "Body" } },
            };
            var pagedResultDto = new PagedResultDto<EmailNotificationDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<EmailNotificationDto>() { new EmailNotificationDto() { Subject = "Subject", To = "To", Body = "Body" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "To", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "To", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<EmailNotificationResponse>>())).Returns(new ServiceResponse<PagedResult<EmailNotificationResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var result = new ServiceResponse<PagedResultDto<EmailNotificationDto>>(pagedResultDto);

            _ = _emailNotificationServiceMock.Setup(r => r.GetEmailNotificationsAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(result);

            var controller = CreateEmailNotificationController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailNotificationController")]
        public async Task Get_AllEmailNotifications_ReturnsOkResult()
        {
            var pagedResult = new PagedResult<EmailNotificationResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<EmailNotificationResponse>() { new EmailNotificationResponse() { Subject = "Subject", To = "To", Body = "Body" } },
            };
            var pagedResultDto = new PagedResultDto<EmailNotificationDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<EmailNotificationDto>() { new EmailNotificationDto() { Subject = "Subject", To = "To", Body = "Body" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "To", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "To", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<EmailNotificationResponse>>())).Returns(new ServiceResponse<PagedResult<EmailNotificationResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var result = new ServiceResponse<PagedResultDto<EmailNotificationDto>>(pagedResultDto);

            _ = _emailNotificationServiceMock.Setup(r => r.GetEmailNotificationsAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(result);

            var controller = CreateEmailNotificationController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailNotificationController")]
        public async Task Search_Success_ReturnsOkObjectResult()
        {
            var req = new EmailNotificationSearchRequest()
            {
                PageIndex = 10,
                Value = "anykey",
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "To", DirectionDesc = true } },
            };

            var res = new EmailNotificationResponse();
            var pagedResult = new PagedResult<EmailNotificationResponse>()
            {
                Items = new List<EmailNotificationResponse>() { res },
            };

            _ = _mapperMock.Setup(r => r.Map<PagedResultDto<EmailNotificationDto>, PagedResult<EmailNotificationResponse>>(It.IsAny<PagedResultDto<EmailNotificationDto>>())).Returns(pagedResult);
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(pagedResult)).Returns(new ServiceResponse<PagedResult<EmailNotificationResponse>>(pagedResult));

            var serviceResponse = new ServiceResponse<PagedResultDto<EmailNotificationDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _emailNotificationServiceMock.Setup(s => s.SearchNotificationAsync(It.IsAny<EmailNotificationSearchRequestDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateEmailNotificationController();
            var response = await controller.SearchAsync(req).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailNotificationController")]
        public async Task SendEmailById_ReturnsNotFoundObjectResult()
        {
            var id = Guid.NewGuid();
            var resp = new ServiceResponse<bool>(false) { Result = false, IsSuccessful = false };
            _ = _emailNotificationServiceMock.Setup(m => m.SendEmailByIdAsync(id)).Returns(Task.FromResult(resp));
            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(resp);
           
            var controller = CreateEmailNotificationController();

            var response = await controller.SendEmailByIdAsync(id).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<NotFoundObjectResult>(response);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "EmailNotificationController")]
        public async Task SendEmailById_ReturnsOkResult()
        {
            var id = Guid.NewGuid();
            _ = _emailNotificationServiceMock.Setup(m => m.SendEmailByIdAsync(id)).Returns(Task.FromResult(new ServiceResponse<bool>(true)));
            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess()).Returns(new ServiceResponse());

            var controller = CreateEmailNotificationController();

            var response = await controller.SendEmailByIdAsync(id).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            controller.Dispose();
        }

        private EmailNotificationController CreateEmailNotificationController()
        {
            var controller = new EmailNotificationController(_emailNotificationServiceMock.Object, _mapperMock.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMock.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}
