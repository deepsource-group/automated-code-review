﻿// -----------------------------------------------------------------------
// <copyright file="RoleControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
// -----------------------------------------------------------------------

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Account.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Role;
using KocSistem.CommunityEdition.Application.Abstractions.Role.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.RoleTranslation.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.User.Contracts;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.TestCommon.Helpers;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.ClaimHelper;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.CommunityEdition.WebAPI.Model.Role;
using KocSistem.CommunityEdition.WebAPI.Model.User;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

using ApplicationRole = KocSistem.CommunityEdition.Domain.ApplicationRole;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class RoleControllerTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IRoleService> _roleServiceMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;

        public RoleControllerTest()
        {
            _mapperMock = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _roleServiceMoq = new Mock<IRoleService>();
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task AddClaimToRole_AddClaimUnsuccessful_ReturnsBadRequest()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };

            _ = _mapperMock.Setup(s => s.Map<RoleClaimPostRequest, RoleClaimDto>(It.IsAny<RoleClaimPostRequest>())).Returns(new RoleClaimDto() { Name = "admin" });

            _ = _roleServiceMoq.Setup(r => r.AddClaimToRoleAsync(It.IsAny<RoleClaimDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.AddClaimToRoleAsync("menu", new RoleClaimPostRequest() { Name = "admin" }).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task AddClaimToRole_ReturnsNotFoundResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _mapperMock.Setup(s => s.Map<RoleClaimPostRequest, RoleClaimDto>(It.IsAny<RoleClaimPostRequest>())).Returns(new RoleClaimDto() { Name = "admin" });

            _ = _roleServiceMoq.Setup(r => r.AddClaimToRoleAsync(It.IsAny<RoleClaimDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.AddClaimToRoleAsync("menu", new RoleClaimPostRequest() { Name = "admin" }).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task AddClaimToRole_ReturnsOkResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _mapperMock.Setup(s => s.Map<RoleClaimPostRequest, RoleClaimDto>(It.IsAny<RoleClaimPostRequest>())).Returns(new RoleClaimDto() { Name = "admin" });

            _ = _roleServiceMoq.Setup(r => r.AddClaimToRoleAsync(It.IsAny<RoleClaimDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.AddClaimToRoleAsync("menu", new RoleClaimPostRequest() { Name = "admin" }).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task AddUserToRole_AddToRoleIsFailed_ReturnsBadRequestObjectResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.AddUserToRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.AddUserToRoleAsync("testrole", "testuser").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task AddUserToRole_AddToRoleIsSuccessful_ReturnsOkResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.AddUserToRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.AddUserToRoleAsync(null, null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task AddUserToRole_NonExistingRole_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.AddUserToRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.AddUserToRoleAsync("testrole", "testuser").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task AddUserToRole_NonExistingUser_ReturnsBadRequestObjectResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.AddUserToRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.AddUserToRoleAsync("testrole", "testuser").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Delete_DeleteRoleFailed_ReturnsOneFrameWebException()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.DeleteAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.DeleteAsync(null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Delete_ExistingRole_ReturnsNoContentResult()
        {
            var roleName = "admin";
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.DeleteAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.DeleteAsync(roleName).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Delete_NonExistingRole_ReturnsNotFoundResult()
        {
            _ = _serviceResponseHelperMoq.Setup(i => i.SetError(It.IsAny<string>(), StatusCodes.Status204NoContent, true)).Returns(new ServiceResponse());

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.DeleteAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.DeleteAsync("admin").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Delete_NonExistingRole_ReturnsBadRequestObjectResult()
        {
            _ = _serviceResponseHelperMoq.Setup(i => i.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse());

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.DeleteAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.DeleteAsync("admin").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public void Get_AllRoles_ReturnsOkResult()
        {
            var serviceResponse = new ServiceResponse<List<ApplicationRoleDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<List<ApplicationRoleModel>>())).Returns(new ServiceResponse<List<ApplicationRoleModel>>(new List<ApplicationRoleModel>()));
            _ = _mapperMock.Setup(s => s.Map<List<ApplicationRoleModel>>(It.IsAny<List<ApplicationRoleDto>>())).Returns(new List<ApplicationRoleModel>());
            _ = _roleServiceMoq.Setup(r => r.GetApplicationRoles()).Returns(serviceResponse);

            using var controller = CreateRoleController();

            var response = controller.Get();

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public void Get_AllRoles_ReturnsNoContentResult()
        {
            _ = _serviceResponseHelperMoq.Setup(i => i.SetError(It.IsAny<string>(), StatusCodes.Status204NoContent, true)).Returns(new ServiceResponse());

            var serviceResponse = new ServiceResponse<List<ApplicationRoleDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.GetApplicationRoles()).Returns(serviceResponse);

            using var controller = CreateRoleController();
            var response = controller.Get();

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Get_ExistingRole_ReturnsOkResult()
        {
            var role = new ApplicationRoleDto();
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<ApplicationRoleDto>())).Returns(new ServiceResponse<ApplicationRoleDto>(role));
            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(new ApplicationRoleDto())
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.GetRoleByNameAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _mapperMock.Setup(s => s.Map<RoleGetWithTranslatesResponse>(It.IsAny<ApplicationRoleDto>())).Returns(new RoleGetWithTranslatesResponse() { Name = "roleName" });
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<RoleGetWithTranslatesResponse>())).Returns(new ServiceResponse<RoleGetWithTranslatesResponse>(new RoleGetWithTranslatesResponse()));

            using var controller = CreateRoleController();
            var response = await controller.GetAsync("roleName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var roleGetWithTranslatesResponse = ((ServiceResponse<RoleGetWithTranslatesResponse>)result.Value).Result;
            Assert.NotNull(roleGetWithTranslatesResponse);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Get_ExistingRoles_ReturnsNotFoundResult()
        {
            var pagedResult = new PagedResult<RoleGetResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<RoleGetResponse>() { new RoleGetResponse() { Name = "role2", Description = "role2" }, new RoleGetResponse() { Name = "role1", Description = "role2" } },
            };
            var pagedResultDto = new PagedResultDto<ApplicationRoleDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationRoleDto>() { new ApplicationRoleDto() { Name = "role2" }, new ApplicationRoleDto() { Name = "role1" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };

            var returnsData = new List<ApplicationRoleDto>() { new ApplicationRoleDto("role1"), new ApplicationRoleDto("role2") }.AsQueryable().BuildMock();
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<PagedResult<RoleGetResponse>>())).Returns(new ServiceResponse<PagedResult<RoleGetResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var serviceResponse = new ServiceResponse<List<ApplicationRoleDto>>(null)
            {
                IsSuccessful = true,
            };
            var pagedResultDtoResponse = new ServiceResponse<PagedResultDto<ApplicationRoleDto>>(pagedResultDto);

            _ = _roleServiceMoq.Setup(r => r.GetApplicationRolesAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(pagedResultDtoResponse);

            using var controller = CreateRoleController();
            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var resultItem = (ServiceResponse<PagedResult<RoleGetResponse>>)result.Value;
            Assert.Equal(2, resultItem.Result.Items.Count);
            Assert.Equal(10, resultItem.Result.PageSize);
            Assert.Equal(2, resultItem.Result.TotalCount);
            Assert.Equal(0, resultItem.Result.PageIndex);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Get_NonExistingRole_ReturnsNotFoundResult()
        {
            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.GetRoleByNameAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.GetAsync(string.Empty).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Get_NonExistingRoles_ReturnsNotFoundResult()
        {
            var pagedResult = new PagedResult<RoleGetResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<RoleGetResponse>() { new RoleGetResponse() { Name = "role2", Description = "role2" }, new RoleGetResponse() { Name = "role1", Description = "role2" } },
            };
            var pagedResultDto = new PagedResultDto<ApplicationRoleDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<ApplicationRoleDto>() { new ApplicationRoleDto() { Name = "role2" }, new ApplicationRoleDto() { Name = "role1" } },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Name", DirectionDesc = true } },
            };

            var returnsData = new List<ApplicationRoleDto>() { new ApplicationRoleDto("role1"), new ApplicationRoleDto("role2") }.AsQueryable().BuildMock();
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<PagedResult<RoleGetResponse>>())).Returns(new ServiceResponse<PagedResult<RoleGetResponse>>(pagedResult));

            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var serviceResponse = new ServiceResponse<PagedResultDto<ApplicationRoleDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };

            _ = _roleServiceMoq.Setup(r => r.GetApplicationRolesAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetClaimsInRole_NonExistingRole_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse<List<ClaimDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.GetClaimsInRoleAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.GetClaimsInRoleAsync(null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetClaimsInRole_RoleHasClaims_ReturnsOkObjectResult()
        {
            var serviceResponse = new ServiceResponse<List<ClaimDto>>(new List<ClaimDto> { new ClaimDto { Name = "test-claim" } });
            _ = _roleServiceMoq.Setup(r => r.GetClaimsInRoleAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _mapperMock.Setup(s => s.Map<List<ClaimResponse>>(It.IsAny<List<ClaimDto>>())).Returns(new List<ClaimResponse> { new ClaimResponse { Name = "test-claim-name", Value = "test-claim" } });
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimResponse>>())).Returns(new ServiceResponse<List<ClaimResponse>>(new List<ClaimResponse> { new ClaimResponse { Name = "test-claim-name", Value = "test-claim" } }));

            using var controller = CreateRoleController();
            var response = await controller.GetClaimsInRoleAsync("testrole").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetClaimsInRole_RoleHasNoClaims_ReturnsEmptyList()
        {
            var serviceResponse = new ServiceResponse<List<ClaimDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _mapperMock.Setup(s => s.Map<List<ClaimResponse>>(It.IsAny<List<ClaimDto>>())).Returns(new List<ClaimResponse> { new ClaimResponse { Name = "test-claim-name", Value = "test-claim" } });
            _ = _roleServiceMoq.Setup(r => r.GetClaimsInRoleAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimResponse>>())).Returns(new ServiceResponse<List<ClaimResponse>>(new List<ClaimResponse> { new ClaimResponse { Name = "test-claim-name", Value = "test-claim" } }));

            using var controller = CreateRoleController();
            var response = await controller.GetClaimsInRoleAsync("testrole").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var resultItem = (ServiceResponse<List<ClaimResponse>>)result.Value;
            Assert.Single(resultItem.Result);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetRoleClaimsTree_NonExistingRole_ReturnsOkObject()
        {
            using var controller = CreateRoleController();
            var serviceResponse = new ServiceResponse<List<ClaimTreeViewItemDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.GetRoleClaimsTreeAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimTreeViewItem>>())).Returns(new ServiceResponse<List<ClaimTreeViewItem>>(new List<ClaimTreeViewItem>()));

            var response = await controller.GetRoleClaimsTreeAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetRoleClaimsTree_NullParameter_ReturnsOkObject()
        {
            var serviceResponse = new ServiceResponse<List<ClaimTreeViewItemDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.GetRoleClaimsTreeAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimTreeViewItem>>())).Returns(new ServiceResponse<List<ClaimTreeViewItem>>(new List<ClaimTreeViewItem>()));
            using var controller = CreateRoleController();

            var response = await controller.GetRoleClaimsTreeAsync(null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetRoleClaimsTree_Success_ReturnsOkObject()
        {
            using var controller = CreateRoleController();
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<ClaimTreeViewItem>>())).Returns(new ServiceResponse<List<ClaimTreeViewItem>>(new List<ClaimTreeViewItem>()));

            var serviceResponse = new ServiceResponse<List<ClaimTreeViewItemDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.GetRoleClaimsTreeAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            var response = await controller.GetRoleClaimsTreeAsync("roleName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetRoleUserInfo_NonExistingRole_ReturnsNotFound()
        {
            var serviceResponse = new ServiceResponse<List<UserRoleInfoDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.GetRoleUserInfoAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.GetRoleUserInfoAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetRoleUserInfo_Success_ReturnsOkObjectResult()
        {
            var userInRole = new ApplicationUser()
            {
                Id = Guid.NewGuid(),
                Name = "userInRole",
                Email = "test@test.com",
                Surname = "Doe",
                UserName = "John",
            };
            var userRoleInfoResponse = new UserRoleInfoResponse()
            {
                Id = userInRole.Id.ToString(),
                Email = "test@test.com",
                Name = userInRole.Name,
                Surname = "Doe",
                Username = "John",
            };

            var serviceResponse = new ServiceResponse<List<UserRoleInfoDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.GetRoleUserInfoAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            var responses = new List<UserRoleInfoResponse>() { userRoleInfoResponse };
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<List<UserRoleInfoResponse>>())).Returns(new ServiceResponse<List<UserRoleInfoResponse>>(responses));
            using var controller = CreateRoleController();

            var response = await controller.GetRoleUserInfoAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var resultItem = (ServiceResponse<List<UserRoleInfoResponse>>)result.Value;
            _ = Assert.Single(resultItem.Result);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetUsersInRole_NonExistingRole_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse<List<UserDto>>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.GetUsersInRoleAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.GetUsersInRoleAsync(null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetUsersInRole_RoleHasNoUsers_ReturnsEmptyList()
        {
            UserDto firstUser = new UserDto
            {
                Email = "firstuser@kocsistem.com.tr",
            };

            UserDto secondUser = new UserDto
            {
                Email = "seconduser@kocsistem.com.tr",
            };

            List<UserDto> users = new List<UserDto>
            {
                firstUser,
                secondUser,
            };
            var list = new List<UserInRoleResponse>() { new UserInRoleResponse() { Email = firstUser.Email }, new UserInRoleResponse() { Email = secondUser.Email } };
            var serviceResponse = new ServiceResponse<List<UserDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.GetUsersInRoleAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<List<UserInRoleResponse>>())).Returns(new ServiceResponse<List<UserInRoleResponse>>(list));
            
            using var controller = this.CreateRoleController();
            
            var response = await controller.GetUsersInRoleAsync("testrole").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;

            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);

            var resultItem = ((ServiceResponse<List<UserInRoleResponse>>)result.Value).Result;
            Assert.IsAssignableFrom<List<UserInRoleResponse>>(resultItem);

            var responseUsers = resultItem as List<UserInRoleResponse>;
            var firstResponseUserList = responseUsers.Where(u => u.Email == firstUser.Email);
            Assert.Single(firstResponseUserList);
            Assert.Equal(firstUser.Email, firstResponseUserList.First().Email);

            var secondResponseUserList = responseUsers.Where(u => u.Email == secondUser.Email);
            Assert.Single(secondResponseUserList);
            Assert.Equal(secondUser.Email, secondResponseUserList.First().Email);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task GetUsersInRole_RoleHasUsers_ReturnsUsersInRole()
        {
            var firstUser = new UserDto
            {
                Email = "firstuser@kocsistem.com.tr",
            };

            var secondUser = new UserDto
            {
                Email = "seconduser@kocsistem.com.tr",
            };

            var users = new List<UserDto>
            {
                firstUser,
                secondUser,
            };

            var list = new List<UserInRoleResponse>() { new UserInRoleResponse() { Email = firstUser.Email }, new UserInRoleResponse() { Email = secondUser.Email } };
            _ = _serviceResponseHelperMoq.Setup(i => i.SetSuccess(It.IsAny<List<UserInRoleResponse>>())).Returns(new ServiceResponse<List<UserInRoleResponse>>(list));
            var serviceResponse = new ServiceResponse<List<UserDto>>(users)
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.GetUsersInRoleAsync(It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.GetUsersInRoleAsync("testrole").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);

            var resultItem = ((ServiceResponse<List<UserInRoleResponse>>)result.Value).Result;
            _ = Assert.IsAssignableFrom<List<UserInRoleResponse>>(resultItem);

            var responseUsers = resultItem as List<UserInRoleResponse>;
            var firstResponseUserList = responseUsers.Where(u => u.Email == firstUser.Email);
            _ = Assert.Single(firstResponseUserList);
            Assert.Equal(firstUser.Email, firstResponseUserList.First().Email);

            var secondResponseUserList = responseUsers.Where(u => u.Email == secondUser.Email);
            _ = Assert.Single(secondResponseUserList);
            Assert.Equal(secondUser.Email, secondResponseUserList.First().Email);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Post_CreateRoleFailed_ReturnsOneFrameWebException()
        {
            var roleName = "admin";
            var roleDescription = "admin role desc";

            var model = new RolePostRequest
            {
                Name = roleName,
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = roleDescription, DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
            };

            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.PostAsync(It.IsAny<ApplicationRoleDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.PostAsync(model).ConfigureAwait(false);

            Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Put_AddToRoleUnsuccessful_ReturnsBadRequest()
        {
            var userInRole = new ApplicationUser() { UserName = "anyName" };
            var usersInRole = new List<string>() { userInRole.Id.ToString() };
            var req = new RolePutRequest()
            {
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersInRole = usersInRole,
            };

            var roleClaimDto = new RoleUpdateDto()
            {
                Translations = new List<ApplicationRoleTranslationDto>
                {
                    new ApplicationRoleTranslationDto { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersInRole = usersInRole,
            };

            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.PutAsync(It.IsAny<RoleUpdateDto>())).ReturnsAsync(serviceResponse);
            _ = _mapperMock.Setup(s => s.Map<RolePutRequest, RoleUpdateDto>(It.IsAny<RolePutRequest>())).Returns(roleClaimDto);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            using var controller = CreateRoleController();

            var response = await controller.PutAsync("anyName", req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Put_NonExistingRole_ReturnsNotFoundResult()
        {
            var rolePutModel = new RolePutRequest
            {
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = "desc", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
            };
            _ = _serviceResponseHelperMoq.Setup(i => i.SetError(It.IsAny<string>(), StatusCodes.Status204NoContent, true)).Returns(new ServiceResponse());
            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            var roleClaimDto = new RoleUpdateDto
            {
                Translations = new List<ApplicationRoleTranslationDto>
                {
                    new ApplicationRoleTranslationDto { Description = "desc", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
            };
            _ = _roleServiceMoq.Setup(r => r.PutAsync(It.IsAny<RoleUpdateDto>())).ReturnsAsync(serviceResponse);
            _ = _mapperMock.Setup(s => s.Map<RolePutRequest, RoleUpdateDto>(It.IsAny<RolePutRequest>())).Returns(roleClaimDto);

            using var controller = CreateRoleController();
            var response = await controller.PutAsync("admin", rolePutModel).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;

            // TODO: null exception
            //Assert.False(resultResponse.IsSuccessful);
            //Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Put_NonExistingUserInRole_ReturnsNotFound()
        {
            var userInRole = new ApplicationUser() { UserName = "anyName" };
            var usersInRole = new List<string>() { userInRole.Id.ToString() };
            var req = new RolePutRequest()
            {
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersInRole = usersInRole,
            };
            var roleClaimDto = new RoleUpdateDto()
            {
                Translations = new List<ApplicationRoleTranslationDto>
                {
                    new ApplicationRoleTranslationDto { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersInRole = usersInRole,
            };
            _ = _mapperMock.Setup(s => s.Map<RolePutRequest, RoleUpdateDto>(It.IsAny<RolePutRequest>())).Returns(roleClaimDto);

            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ =  _roleServiceMoq.Setup(r => r.PutAsync(It.IsAny<RoleUpdateDto>())).ReturnsAsync(serviceResponse);
            _ = _mapperMock.Setup(s => s.Map<ApplicationRoleDto, RolePutResponse>(It.IsAny<ApplicationRoleDto>())).Returns(new RolePutResponse());

            _ = _serviceResponseHelperMoq.Setup(s => s.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent)));
            
            using var controller = this.CreateRoleController();

            var response = await controller.PutAsync("anyName", req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Put_NonExistingUserNotInRole_ReturnsNotFound()
        {
            var userNotInRole = new ApplicationUser() { UserName = "anyName" };
            var usersNotInRole = new List<string>() { userNotInRole.Id.ToString() };
            var req = new RolePutRequest()
            {
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersNotInRole = usersNotInRole,
            };
            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            var roleClaimDto = new RoleUpdateDto()
            {
                Translations = new List<ApplicationRoleTranslationDto>
                {
                    new ApplicationRoleTranslationDto { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersInRole = usersNotInRole,
            };
            _ = _mapperMock.Setup(s => s.Map<RolePutRequest, RoleUpdateDto>(It.IsAny<RolePutRequest>())).Returns(roleClaimDto);
            _ = _roleServiceMoq.Setup(r => r.PutAsync(It.IsAny<RoleUpdateDto>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetError(It.IsAny<string>(), StatusCodes.Status204NoContent, true)).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent)));
            using var controller = CreateRoleController();

            var response = await controller.PutAsync("anyName", req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;

            // TODO: null exception
            //Assert.Equal(expected: StatusCodes.Status200OK, actual: result.StatusCode);
            //var resultResponse = result.Value as ServiceResponse;
            //Assert.False(resultResponse.IsSuccessful);
            //Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Put_RemoveFromUnsuccessful_ReturnsBadRequest()
        {
            var userNotInRole = new ApplicationUser() { UserName = "anyName" };
            var usersNotInRole = new List<string>() { userNotInRole.Id.ToString() };
            var req = new RolePutRequest()
            {
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersNotInRole = usersNotInRole,
            };
            var roleClaimDto = new RoleUpdateDto()
            {
                Translations = new List<ApplicationRoleTranslationDto>
                {
                    new ApplicationRoleTranslationDto { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersInRole = usersNotInRole,
            };
            _ = _mapperMock.Setup(s => s.Map<RolePutRequest, RoleUpdateDto>(It.IsAny<RolePutRequest>())).Returns(roleClaimDto);
            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.PutAsync(It.IsAny<RoleUpdateDto>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest)));
            using var controller = CreateRoleController();

            var response = await controller.PutAsync("anyName", req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Put_UpdateUnsuccessful_ReturnsBadRequest()
        {
            var req = new RolePutRequest()
            {
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
            };
            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            var roleClaimDto = new RoleUpdateDto()
            {
                Translations = new List<ApplicationRoleTranslationDto>
                {
                    new ApplicationRoleTranslationDto { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
            };
            _ =  _mapperMock.Setup(s => s.Map<RolePutRequest, RoleUpdateDto>(It.IsAny<RolePutRequest>())).Returns(roleClaimDto);
            _ = _roleServiceMoq.Setup(r => r.PutAsync(It.IsAny<RoleUpdateDto>())).ReturnsAsync(serviceResponse);
            
            using var controller = this.CreateRoleController();

            var response = await controller.PutAsync("anyName", req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Put_ValidModel_ReturnsOkResult()
        {
            var roleName = "admin";
            var roleDescription = "admin role desc";

            var model = new RolePutRequest
            {
                Translations = new List<RoleTranslationsModel>
                {
                    new RoleTranslationsModel { Description = roleDescription, DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
            };
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<RolePutResponse>())).Returns(new ServiceResponse<RolePutResponse>(new RolePutResponse() { Name = roleName, Description = roleDescription }));

            var serviceResponse = new ServiceResponse<ApplicationRoleDto>(new ApplicationRoleDto() { Name = roleName })
            {
                IsSuccessful = true,
            };
            var roleClaimDto = new RoleUpdateDto()
            {
                Translations = new List<ApplicationRoleTranslationDto>
                {
                    new ApplicationRoleTranslationDto { Description = roleDescription, DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
            };
            _ = _mapperMock.Setup(s => s.Map<RolePutRequest, RoleUpdateDto>(It.IsAny<RolePutRequest>())).Returns(roleClaimDto);
            _ = _roleServiceMoq.Setup(r => r.PutAsync(It.IsAny<RoleUpdateDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            controller.BindViewModel(model);

            var response = await controller.PutAsync(roleName, model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var putResponse = ((ServiceResponse<RolePutResponse>)result.Value).Result;
            Assert.Equal(roleName, putResponse.Name);
            Assert.Equal(roleDescription, putResponse.Description);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveClaimFromRole_AddToRoleIsSuccessful_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveClaimFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.RemoveClaimFromRoleAsync("testrole", "testclaimValue").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveClaimFromRole_NonExistingClaim_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveClaimFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.RemoveClaimFromRoleAsync("testrole", "testclaim").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveClaimFromRole_NonExistingRole_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveClaimFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.RemoveClaimFromRoleAsync("testrole", "testclaim").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveClaimFromRole_RemoveClaimUnsuccesful_ReturnsBadRequestObjectResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveClaimFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.RemoveClaimFromRoleAsync("testrole", "testclaim").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveClaimFromRole_RoleWithNoClaims_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveClaimFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.RemoveClaimFromRoleAsync("testrole", "testclaim").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveUserFromRole_AddToRoleIsFailed_ReturnsBadRequestObjectResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status400BadRequest },
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveUserFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            
            using var controller = CreateRoleController();
            
            var response = await controller.RemoveUserFromRoleAsync("testrole", "testuser").ConfigureAwait(false);

            Assert.IsAssignableFrom<BadRequestObjectResult>(response);
            var result = (BadRequestObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status400BadRequest, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveUserFromRole_AddToRoleIsSuccessful_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveUserFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);
            
            using var controller = this.CreateRoleController();
            
            var response = await controller.RemoveUserFromRoleAsync(null, null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveUserFromRole_NonExistingRole_ReturnsNoContentResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveUserFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.RemoveUserFromRoleAsync("testrole", "testuser").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task RemoveUserFromRole_NonExistingUser_ReturnsNotFoundResult()
        {
            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.RemoveUserFromRoleAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();
            var response = await controller.RemoveUserFromRoleAsync(null, null).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task SaveRoleClaims_NonExistingSelectedRole_ReturnsNotFound()
        {
            var req = new SaveRoleClaimsModel() { Name = "model" };

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = false,
                Error = new ErrorInfo() { Code = StatusCodes.Status204NoContent },
            };
            _ = _roleServiceMoq.Setup(r => r.SaveRoleClaimsAsync(It.IsAny<SaveRoleClaimsDto>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetError(It.IsAny<string>(), StatusCodes.Status204NoContent, true)).Returns(new ServiceResponse(false));
            using var controller = CreateRoleController();
            var response = await controller.SaveRoleClaimsAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task SaveRoleClaims_Success_ReturnsOkObjectResult()
        {
            var req = new SaveRoleClaimsModel() { Name = "model", SelectedRoleClaimList = new List<string>() { "permission", "ObsoleteSample", "Management_Role_AddClaim" } };
            var roles = new List<ApplicationRole>() { new ApplicationRole() { NormalizedName = "model" } }.AsQueryable().BuildMock();

            var serviceResponse = new ServiceResponse()
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.SaveRoleClaimsAsync(It.IsAny<SaveRoleClaimsDto>())).ReturnsAsync(serviceResponse);

            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess()).Returns(new ServiceResponse());
            using var controller = CreateRoleController();

            var response = await controller.SaveRoleClaimsAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleController")]
        public async Task Search_Success_ReturnsOkResult()
        {
            var req = new RoleSearchRequest()
            {
                Name = "anyRole",
            };
            var res = new RoleGetResponse()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "anyName",
                Description = "Desc",
            };
            var pagedResult = new PagedResult<RoleGetResponse>()
            {
                Items = new List<RoleGetResponse>() { res },
            };
            var roles = new List<ApplicationRole>() { new ApplicationRole() { NormalizedName = "roleName" } }.AsQueryable().BuildMock();
            _ = _mapperMock.Setup(s => s.Map<PagedResultDto<ApplicationRoleDto>, PagedResult<RoleGetResponse>>(It.IsAny<PagedResultDto<ApplicationRoleDto>>())).Returns(pagedResult);
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(pagedResult)).Returns(new ServiceResponse<PagedResult<RoleGetResponse>>(pagedResult));

            var serviceResponse = new ServiceResponse<PagedResultDto<ApplicationRoleDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _roleServiceMoq.Setup(r => r.SearchAsync(It.IsAny<RoleSearchDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateRoleController();

            var response = await controller.SearchAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var resultItem = ((ServiceResponse<PagedResult<RoleGetResponse>>)result.Value).Result;
            Assert.Equal(1, resultItem.Items.Count);
        }

        private RoleController CreateRoleController()
        {
            var controller = new RoleController(_mapperMock.Object, _roleServiceMoq.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMoq.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}