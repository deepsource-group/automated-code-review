﻿// <copyright file="LoginAuditLogControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Excel.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.LoginAuditLog.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.PdfExport.Contracts;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.WebAPI.Controllers;
using KocSistem.CommunityEdition.WebAPI.Model.LoginAuditLog;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Controllers
{
    public class LoginAuditLogControllerTest
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<ILoginAuditLogService> _loginAuditLogServiceMock;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMock;

        public LoginAuditLogControllerTest()
        {
            _loginAuditLogServiceMock = new Mock<ILoginAuditLogService>();
            _mapperMock = new Mock<IMapper>();
            _serviceResponseHelperMock = new Mock<IServiceResponseHelper>();
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<LoginAuditLogController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController")]
        public async Task Get_AllLoginAuditLogs_ReturnsOkObjectResultForEmptyList()
        {
            var pagedResult = new PagedResult<LoginAuditLogResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<LoginAuditLogResponse>() {
                    new LoginAuditLogResponse()
                    {
                        Id = Guid.NewGuid(),
                        Hostname = "Hostname 1",
                        Ip = "IP Address 1",
                        MacAddress = "MacAddress 1",
                        Message = string.Empty,
                        RequestHeaderInfo = "RequestHeaderInfo 1",
                        BrowserDetail = "BrowserDetail 1",
                        BrowserGuid = "BrowserGuid 1",
                    },
                    new LoginAuditLogResponse
                    {
                         Id = Guid.NewGuid(),
                         Hostname = "Hostname 2",
                         Ip = "IP Address 1",
                         MacAddress = "MacAddress 2",
                         Message = string.Empty,
                         RequestHeaderInfo = "RequestHeaderInfo 2",
                         BrowserDetail = "BrowserDetail 2",
                         BrowserGuid = "BrowserGuid 2",
                    },
                },
            };
            var pagedResultDto = new PagedResultDto<LoginAuditLogDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<LoginAuditLogDto>() {
                    new LoginAuditLogDto()
                    {
                        Id = Guid.NewGuid(),
                        Hostname = "Hostname 1",
                        Ip = "IP Address 1",
                        MacAddress = "MacAddress 1",
                        Message = string.Empty,
                        RequestHeaderInfo = "RequestHeaderInfo 1",
                        BrowserDetail = "BrowserDetail 1",
                        BrowserGuid = "BrowserGuid 1",
                    },
                    new LoginAuditLogDto
                    {
                         Id = Guid.NewGuid(),
                         Hostname = "Hostname 2",
                         Ip = "IP Address 1",
                         MacAddress = "MacAddress 2",
                         Message = string.Empty,
                         RequestHeaderInfo = "RequestHeaderInfo 2",
                         BrowserDetail = "BrowserDetail 2",
                         BrowserGuid = "BrowserGuid 2",
                    },
                },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Key", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<LoginAuditLogResponse>>())).Returns(new ServiceResponse<PagedResult<LoginAuditLogResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var serviceResponse = new ServiceResponse<PagedResultDto<LoginAuditLogDto>>(pagedResultDto);

            _ = _loginAuditLogServiceMock.Setup(r => r.GetLoginAuditLogsAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            var controller = CreateLoginAuditLogController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var resultItem = (ServiceResponse<PagedResult<LoginAuditLogResponse>>)result.Value;
            Assert.Equal(2, resultItem.Result.Items.Count);
            Assert.Equal(10, resultItem.Result.PageSize);
            Assert.Equal(0, resultItem.Result.PageIndex);
            Assert.Equal(1, resultItem.Result.TotalPages);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController")]
        public async Task Get_AllLoginAuditLogs_ReturnsNoContentResult()
        {
            var pagedResult = new PagedResult<LoginAuditLogResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<LoginAuditLogResponse>() {
                    new LoginAuditLogResponse()
                    {
                        Id = Guid.NewGuid(),
                        Hostname = "Hostname 1",
                        Ip = "IP Address 1",
                        MacAddress = "MacAddress 1",
                        Message = string.Empty,
                        RequestHeaderInfo = "RequestHeaderInfo 1",
                        BrowserDetail = "BrowserDetail 1",
                        BrowserGuid = "BrowserGuid 1",
                    },
                    new LoginAuditLogResponse
                    {
                         Id = Guid.NewGuid(),
                         Hostname = "Hostname 2",
                         Ip = "IP Address 1",
                         MacAddress = "MacAddress 2",
                         Message = string.Empty,
                         RequestHeaderInfo = "RequestHeaderInfo 2",
                         BrowserDetail = "BrowserDetail 2",
                         BrowserGuid = "BrowserGuid 2",
                    },
                },
            };
            var pagedResultDto = new PagedResultDto<LoginAuditLogDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<LoginAuditLogDto>() {
                    new LoginAuditLogDto()
                    {
                        Id = Guid.NewGuid(),
                        Hostname = "Hostname 1",
                        Ip = "IP Address 1",
                        MacAddress = "MacAddress 1",
                        Message = string.Empty,
                        RequestHeaderInfo = "RequestHeaderInfo 1",
                        BrowserDetail = "BrowserDetail 1",
                        BrowserGuid = "BrowserGuid 1",
                    },
                    new LoginAuditLogDto
                    {
                         Id = Guid.NewGuid(),
                         Hostname = "Hostname 2",
                         Ip = "IP Address 1",
                         MacAddress = "MacAddress 2",
                         Message = string.Empty,
                         RequestHeaderInfo = "RequestHeaderInfo 2",
                         BrowserDetail = "BrowserDetail 2",
                         BrowserGuid = "BrowserGuid 2",
                    },
                },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Key", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetError(It.IsAny<PagedResult<LoginAuditLogResponse>>(), It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse<PagedResult<LoginAuditLogResponse>>(pagedResult, false));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var serviceResponse = new ServiceResponse<PagedResultDto<LoginAuditLogDto>>(pagedResultDto, new ErrorInfo(StatusCodes.Status204NoContent), false);

            _ = _loginAuditLogServiceMock.Setup(r => r.GetLoginAuditLogsAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            var controller = CreateLoginAuditLogController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.False(resultResponse.IsSuccessful);
            Assert.Equal(expected: StatusCodes.Status204NoContent, actual: resultResponse.Error.Code);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController")]
        public async Task Get_AllLoginAuditLogs_ReturnsOkResult()
        {
            var pagedResult = new PagedResult<LoginAuditLogResponse>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<LoginAuditLogResponse>() {
                    new LoginAuditLogResponse()
                    {
                        Id = Guid.NewGuid(),
                        Hostname = "Hostname 1",
                        Ip = "IP Address 1",
                        MacAddress = "MacAddress 1",
                        Message = string.Empty,
                        RequestHeaderInfo = "RequestHeaderInfo 1",
                        BrowserDetail = "BrowserDetail 1",
                        BrowserGuid = "BrowserGuid 1",
                    },
                    new LoginAuditLogResponse
                    {
                         Id = Guid.NewGuid(),
                         Hostname = "Hostname 2",
                         Ip = "IP Address 1",
                         MacAddress = "MacAddress 2",
                         Message = string.Empty,
                         RequestHeaderInfo = "RequestHeaderInfo 2",
                         BrowserDetail = "BrowserDetail 2",
                         BrowserGuid = "BrowserGuid 2",
                    },
                },
            };
            var pagedResultDto = new PagedResultDto<LoginAuditLogDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalPages = 1,
                TotalCount = 2,
                Items = new List<LoginAuditLogDto>() {
                    new LoginAuditLogDto()
                    {
                        Id = Guid.NewGuid(),
                        Hostname = "Hostname 1",
                        Ip = "IP Address 1",
                        MacAddress = "MacAddress 1",
                        Message = string.Empty,
                        RequestHeaderInfo = "RequestHeaderInfo 1",
                        BrowserDetail = "BrowserDetail 1",
                        BrowserGuid = "BrowserGuid 1",
                    },
                    new LoginAuditLogDto
                    {
                         Id = Guid.NewGuid(),
                         Hostname = "Hostname 2",
                         Ip = "IP Address 1",
                         MacAddress = "MacAddress 2",
                         Message = string.Empty,
                         RequestHeaderInfo = "RequestHeaderInfo 2",
                         BrowserDetail = "BrowserDetail 2",
                         BrowserGuid = "BrowserGuid 2",
                    },
                },
            };
            var req = new PagedRequestDto()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Key", DirectionDesc = true } },
            };

            var req1 = new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 10,
                Orders = new List<PagedRequestOrder>() { new PagedRequestOrder() { ColumnName = "Key", DirectionDesc = true } },
            };

            _ = _serviceResponseHelperMock.Setup(i => i.SetSuccess(It.IsAny<PagedResult<LoginAuditLogResponse>>())).Returns(new ServiceResponse<PagedResult<LoginAuditLogResponse>>(pagedResult));
            _ = _mapperMock.Setup(s => s.Map<List<PagedRequestOrder>, List<PagedRequestOrderDto>>(It.IsAny<List<PagedRequestOrder>>())).Returns(req.Orders);
            _ = _mapperMock.Setup(s => s.Map<PagedRequest, PagedRequestDto>(It.IsAny<PagedRequest>())).Returns(req);

            var serviceResponse = new ServiceResponse<PagedResultDto<LoginAuditLogDto>>(pagedResultDto);

            _ = _loginAuditLogServiceMock.Setup(r => r.GetLoginAuditLogsAsync(It.IsAny<PagedRequestDto>())).ReturnsAsync(serviceResponse);

            var controller = CreateLoginAuditLogController();

            var response = await controller.GetAsync(req1).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
            var resultItem = (ServiceResponse<PagedResult<LoginAuditLogResponse>>)result.Value;
            Assert.Equal(2, resultItem.Result.Items.Count);
            Assert.Equal(10, resultItem.Result.PageSize);
            Assert.Equal(0, resultItem.Result.PageIndex);
            Assert.Equal(1, resultItem.Result.TotalPages);
            controller.Dispose();
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController")]
        public async Task Search_Success_ReturnsOkResult()
        {
            var req = new LoginAuditLogSearchRequest()
            {
                Value = "anyIP",
            };

            var res = new LoginAuditLogResponse()
            {
                Id = Guid.NewGuid(),
                Hostname = "Hostname 1",
                Ip = "IP Address 1",
                MacAddress = "MacAddress 1",
                Message = string.Empty,
                RequestHeaderInfo = "RequestHeaderInfo 1",
                BrowserDetail = "BrowserDetail 1",
                BrowserGuid = "BrowserGuid 1",
            };
            var pagedResult = new PagedResult<LoginAuditLogResponse>()
            {
                Items = new List<LoginAuditLogResponse>() { res },
            };
            _ = _mapperMock.Setup(s => s.Map<LoginAuditLogSearchDto>(It.IsAny<LoginAuditLogSearchRequest>())).Returns(new LoginAuditLogSearchDto());
            _ = _mapperMock.Setup(s => s.Map<IPagedList<LoginAuditLog>, PagedResult<LoginAuditLogResponse>>(It.IsAny<IPagedList<LoginAuditLog>>())).Returns(pagedResult);
            _ = _serviceResponseHelperMock.Setup(s => s.SetSuccess(It.IsAny<PagedResult<LoginAuditLogResponse>>())).Returns(new ServiceResponse<PagedResult<LoginAuditLogResponse>>(pagedResult));

            var serviceResponse = new ServiceResponse<PagedResultDto<LoginAuditLogDto>>(null)
            {
                IsSuccessful = true,
            };
            _ = _loginAuditLogServiceMock.Setup(r => r.SearchAsync(It.IsAny<LoginAuditLogSearchDto>())).ReturnsAsync(serviceResponse);

            using var controller = CreateLoginAuditLogController();

            var response = await controller.SearchAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
            var result = (OkObjectResult)response;
            var resultResponse = result.Value as ServiceResponse;
            Assert.True(resultResponse.IsSuccessful);
        }

        private LoginAuditLogController CreateLoginAuditLogController()
        {
            var controller = new LoginAuditLogController(_loginAuditLogServiceMock.Object, _mapperMock.Object);

            var request = new Mock<HttpRequest>();
            _ = request.Setup(expression: x => x.Scheme).Returns(value: "http");

            var httpContext = Mock.Of<HttpContext>(predicate: c => c.Request == request.Object);

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
               .Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper)))
               .Returns(_serviceResponseHelperMock.Object);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController")]
        public async Task ExcelExport_Success_ReturnOkResult()
        {
            var req = new LoginAuditLogFilterRequest()
            {
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1)
            };

            var res = new ExcelExportResponse()
            {
                FileByteArray = new Byte[64],
                FileName = "FileName"
            };

            _ = _mapperMock.Setup(s => s.Map<LoginAuditLogFilterDto, LoginAuditLogFilterRequest>(It.IsAny<LoginAuditLogFilterDto>())).Returns(req);
            _ = _serviceResponseHelperMock.Setup(s => s.SetSuccess(res)).Returns(new ServiceResponse<ExcelExportResponse>(res));

            var serviceResponse = new ServiceResponse<ExcelExportDto>(null) { IsSuccessful = true };

            _ = _loginAuditLogServiceMock.Setup(r => r.SearchForExcelExportAsync(It.IsAny<LoginAuditLogFilterDto>())).ReturnsAsync(serviceResponse);

            var controller = CreateLoginAuditLogController();

            var response = await controller.ExcelExportAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController")]
        public async Task ExportPdf_Success_ReturnOkResult()
        {
            var req = new LoginAuditLogFilterRequest()
            {
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1)
            };

            var res = new PdfExportResponseModel()
            {
                FileByteArray = new Byte[64],
                FileName = "FileName"
            };

            _ = _mapperMock.Setup(s => s.Map<LoginAuditLogFilterDto, LoginAuditLogFilterRequest>(It.IsAny<LoginAuditLogFilterDto>())).Returns(req);
            _ = _serviceResponseHelperMock.Setup(s => s.SetSuccess(res)).Returns(new ServiceResponse<PdfExportResponseModel>(res));

            var serviceResponse = new ServiceResponse<PdfExportDto>(null) { IsSuccessful = true };

            _ = _loginAuditLogServiceMock.Setup(r => r.SearchForPdfExportAsync(It.IsAny<LoginAuditLogFilterDto>())).ReturnsAsync(serviceResponse);

            var controller = CreateLoginAuditLogController();

            var response = await controller.PdfExportAsync(req).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(response);



        }
    }
}