﻿// <copyright file="ValidateModelAttributeTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.WebAPI.Filters;
using KocSistem.OneFrame.Common.Extensions;
using KocSistem.OneFrame.I18N;
using Moq;
using System;
using System.Collections.Generic;
using KocSistem.OneFrame.ErrorHandling;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using Xunit;

namespace KocSistem.CommunityEdition.WebAPI.Tests.Filters
{
    public class ValidateModelAttributeTest
    {
        private readonly Mock<IKsI18N> _localizationMoq;

        public ValidateModelAttributeTest()
        {
            _localizationMoq = new Mock<IKsI18N>();
        }

        [Fact]
        [Trait("Category", "Filters")]
        public void ValidateModelAttribute_InvalidModelState_ThrowsOneFrameWebException()
        {
            var validator = new ValidateModelAttribute(_localizationMoq.Object);

            _ = Assert.IsType<ValidateModelAttribute>(validator);
        }

        [Fact]
        [Trait("Category", "Filters")]
        public void ValidationExtensions_InvalidModelState_ReturnsBadRequestObjectResult()
        {
            var modelState = new ModelStateDictionary();
            modelState.AddModelError("", "error");
            var httpContext = new DefaultHttpContext();
            var context = new ActionExecutingContext(
                new ActionContext(
                    httpContext: httpContext,
                    routeData: new RouteData(),
                    actionDescriptor: new ActionDescriptor(),
                    modelState: modelState
                ),
                new List<IFilterMetadata>(),
                new Dictionary<string, object>(),
                new Mock<Controller>().Object);

            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<ValidateModelAttribute>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var validator = new ValidateModelAttribute(_localizationMoq.Object);

            void Action()
            {
                validator.OnActionExecuting(context);
            }

            _ = Assert.Throws<OneFrameValidationException>((Action)Action);
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void ValidationExtensions_ThrowIfNull_ThrowsArgumentNullException()
        {
            object obj = null;

            void Action()
            {
                _ = obj.ThrowIfNull(nameof(obj));
            }

            _ = Assert.Throws<ArgumentNullException>((Action)Action);
        }
    }
}