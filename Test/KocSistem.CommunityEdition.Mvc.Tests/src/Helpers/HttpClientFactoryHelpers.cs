﻿// <copyright file="HttpClientFactoryHelpers.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace KocSistem.CommunityEdition.Mvc.Tests.Helpers
{
    public static class HttpClientFactoryHelpers
    {
        public static IHttpClientFactory CreateHttpClientFactory(this Mock<IHttpClientFactory> httpClientFactoryMock, HttpStatusCode returnStatusCode, string returnContent)
        {
            using var httpResponseMessage = new HttpResponseMessage()
            {
                StatusCode = returnStatusCode,
                Content = new StringContent(returnContent),
            };
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               // Setup the PROTECTED method to mock
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>())
               // prepare the expected response of the mocked http call
               .ReturnsAsync(httpResponseMessage)
               .Verifiable();

            // use real http client with mocked handler here
            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://test.com/"),
            };

            _ = httpClientFactoryMock.Setup(httpClientFactory => httpClientFactory.CreateClient(It.IsAny<string>())).Returns(httpClient);

            return httpClientFactoryMock.Object;
        }
    }
}
