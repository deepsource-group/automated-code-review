﻿// <copyright file="ProxyHelpers.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using Moq;
using System.Net.Http;
using System.Threading.Tasks;

namespace KocSistem.CommunityEdition.Mvc.Tests.Helpers
{
    public static class ProxyHelpers
    {
        public static IClientProxy GetProxyHelper<T>(this Mock<IClientProxy> proxyHelperMock, T result)
        {
            _ = proxyHelperMock.Setup(x => x.GetJwtIssuerHttpClient()).Returns(() => new HttpClient());

            _ = proxyHelperMock.Setup(c => c.GetApiRequest<T>(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(result));

            _ = proxyHelperMock.Setup(c => c.PutApiRequest<T>(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
               .Returns(Task.FromResult(result));

            _ = proxyHelperMock.Setup(c => c.PostApiRequest<T>(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
               .Returns(Task.FromResult(result));

            _ = proxyHelperMock.Setup(c => c.DeleteApiRequest<T>(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
               .Returns(Task.FromResult(result));

            return proxyHelperMock.Object;
        }
    }
}
