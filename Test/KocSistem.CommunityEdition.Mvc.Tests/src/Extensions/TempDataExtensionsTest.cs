﻿// <copyright file="TempDataExtensionsTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Mvc.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Extensions
{
    public class TempDataExtensionsTest
    {
        [Fact]
        [Trait("Category", "Extensions-MVC")]
        public void Get_RandomKey_ReturnsNull()
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var result = tempData.Get<string>("anyKey");

            Assert.Null(result);
        }
    }
}
