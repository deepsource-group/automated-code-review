﻿// <copyright file="AccountControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.Infrastructure.Helpers.Captcha;
using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Helpers;
using KocSistem.CommunityEdition.Mvc.Models.Account;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using KocSistem.OneFrame.Notification.Email;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class AccountControllerTest
    {
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly Mock<ILogger<AccountController>> _loggerMoq;
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<IUrlHelperFactory> _urlHelperFactoryMock;
        private readonly Mock<IUrlHelper> _urlHelperMock;
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";
        private readonly Mock<ICaptchaValidator> _captchaValidatorMoq;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IClaimHelper> _claimHelperMoq;

        public AccountControllerTest()
        {
            _configurationMoq = new Mock<IConfiguration>();
            _captchaValidatorMoq = new Mock<ICaptchaValidator>();
            _loggerMoq = new Mock<ILogger<AccountController>>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
            _urlHelperFactoryMock = new Mock<IUrlHelperFactory>();
            _urlHelperMock = new Mock<IUrlHelper>();
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<AccountController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _claimHelperMoq = new Mock<IClaimHelper>();
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public void AccessDenied_Success_ReturnsViewModel()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = controller.AccessDenied();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public void CheckEmail_Success_ReturnsView()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(null, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = controller.CheckEmail("anyToken", "test@test.com", false);
            var response = Assert.IsAssignableFrom<ViewResult>(result);
            var model = (ResetPasswordViewModel)response.Model;

            Assert.Equal("test@test.com", model.Email);
            Assert.Equal("anyToken", model.Token);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ConfirmEmail_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var urlHelperFactory = CreateUrlHelperFactory();
            var data = new ServiceResponse(true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper, urlHelperFactory);

            var result = await controller.ConfirmEmailAsync("anyToken", "test@test.com").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ConfirmEmail_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var urlHelperFactory = CreateUrlHelperFactory();
            var data = new ServiceResponse(new ErrorInfo { Code = StatusCodes.Status404NotFound });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper, urlHelperFactory);

            var result = await controller.ConfirmEmailAsync("anyToken", "test@test.com").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ConfirmEmail_BadRequestResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.BadRequest, _sampleResponse);
            var urlHelperFactory = CreateUrlHelperFactory();
            var data = new ServiceResponse(new ErrorInfo { Code = StatusCodes.Status400BadRequest });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper, urlHelperFactory);

            var result = await controller.ConfirmEmailAsync("anyToken", "test@test.com").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ForgotPassword_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(new ErrorInfo { Code = StatusCodes.Status404NotFound });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);
            var model = CreateForgotPasswordViewModel();

            var result = await controller.ForgotPasswordAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ForgotPassword_UnAuthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(new ErrorInfo { Code = StatusCodes.Status401Unauthorized });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);
            var model = CreateForgotPasswordViewModel();

            var result = await controller.ForgotPasswordAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ForgotPassword_OkAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse(true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);
            var model = CreateForgotPasswordViewModel();

            var result = await controller.ForgotPasswordAsync(model).ConfigureAwait(false) as OkObjectResult;

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
            var resultValue = result.Value as ServiceResponse;
            Assert.True(resultValue.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task Login_NotNullReturnURL_ReturnsRedirectResult()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Email = "test@test.com",
                Name = "Jane",
                Surname = "Deo",
            };
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                new Claim(JwtRegisteredClaimNames.FamilyName, user.Surname),
            };
            var model = CreateLoginViewModel();
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var urlHelperFactory = CreateUrlHelperFactory();
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper, urlHelperFactory, claims);

            var result = await controller.LoginAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<RedirectResult>(result);
            var response = (RedirectResult)result;
            Assert.Equal(model.ReturnUrl, response.Url);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task Login_NullReturnURL_ReturnsRedirectToActionResult()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Email = "test@test.com",
                Name = "Jane",
                Surname = "Deo",
            };
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                new Claim(JwtRegisteredClaimNames.FamilyName, user.Surname),
            };
            var model = CreateLoginViewModel();
            model.ReturnUrl = null;
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var urlHelperFactory = CreateUrlHelperFactory();
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper, urlHelperFactory, claims);

            var result = await controller.LoginAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<RedirectToActionResult>(result);
            var response = (RedirectToActionResult)result;
            Assert.Equal("Index", response.ActionName);
        }

        [Trait("Category", "AccountController-MVC")]
        [Fact]
        public void ExternalLogin_ReturnsRedirectUrl()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var urlHelperFactory = CreateUrlHelperFactory();
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper, urlHelperFactory, null);

            var result = controller.ExternalLogin("Facebook", It.IsAny<string>());

            _ = Assert.IsAssignableFrom<RedirectResult>(result);
            var response = (RedirectResult)result;
            Assert.Contains("accounts/external-login-callback", response.Url);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ExternalLoginCallback_ReturnsView()
        {
            var model = CreateLoginViewModel();
            model.ReturnUrl = null;
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var urlHelperFactory = CreateUrlHelperFactory();
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ6b25laW5mbyI6IkV1cm9wZS9NaW5zayIsImdpdmVuX25hbWUiOiJuYW1lIiwiZmFtaWx5X25hbWUiOiJzdXJuYW1lIn0.LyfCux-qdcAOMR_8fGSNrCNgMZ878RGOuM8BR3Sdwmc";

            using var controller = CreateAccountController(httpClientFactory, proxyHelper, urlHelperFactory, null);

            var result = await controller.ExternalLoginCallbackAsync(token, "refreshToken").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task Login_NotFoundResponse_ReturnsToastError()
        {
            var model = CreateLoginViewModel();
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<LoginResponseViewModel>(new LoginResponseViewModel() { Token = "token", RefreshToken = "RefreshToken" }, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.LoginAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task Login_UnAuthorizedAPIResponse_ReturnsToastError()
        {
            var model = CreateLoginViewModel();
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<LoginResponseViewModel>(new LoginResponseViewModel() { Token = "token", RefreshToken = "RefreshToken" }, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.LoginAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public void PasswordExpired_Success_ReturnsViewModel()
        {
            var email = "testemail";
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = controller.PasswordExpired(email);

            Assert.IsAssignableFrom<ViewResult>(result);
            var response = (ViewResult)result;
            Assert.IsAssignableFrom<PasswordExpiredViewModel>(response.Model);
            var model = (PasswordExpiredViewModel)response.Model;
            Assert.Equal(email, model.UserName);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task PasswordExpiredChange_CurrentPasswordEqualNewPassword_ReturnToastError()
        {
            var passwordExpiredViewModel = new PasswordExpiredViewModel()
            {
                CurrentPassword = "currentPassword",
                NewPassword = "currentPassword",
                NewPasswordConfirmation = "currentPassword",
                UserName = "test@test.com",
            };

            using var controller = CreateAccountController(null, null);

            var result = await controller.PasswordExpiredChangeAsync(passwordExpiredViewModel).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task PasswordExpiredChange_NewPasswordNotEqualNewPasswordConfirmation_ReturnToastError()
        {
            var passwordExpiredViewModel = new PasswordExpiredViewModel()
            {
                CurrentPassword = "currentPassword",
                NewPassword = "newPassword",
                NewPasswordConfirmation = "anyPassword",
                UserName = "test@test.com",
            };

            using var controller = CreateAccountController(null, null);

            var result = await controller.PasswordExpiredChangeAsync(passwordExpiredViewModel).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task PasswordExpiredChange_NotFound_ReturnToastError()
        {
            var passwordExpiredViewModel = new PasswordExpiredViewModel()
            {
                CurrentPassword = "currentPassword",
                NewPassword = "newPassword",
                NewPasswordConfirmation = "newPassword",
                UserName = "test@test.com",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.PasswordExpiredChangeAsync(passwordExpiredViewModel).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task PasswordExpiredChange_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var passwordExpiredViewModel = new PasswordExpiredViewModel()
            {
                CurrentPassword = "currentPassword",
                NewPassword = "newPassword",
                NewPasswordConfirmation = "newPassword",
                UserName = "test@test.com",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.PasswordExpiredChangeAsync(passwordExpiredViewModel).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task PasswordExpiredChange_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var passwordExpiredViewModel = new PasswordExpiredViewModel()
            {
                CurrentPassword = "currentPassword",
                NewPassword = "newPassword",
                NewPasswordConfirmation = "newPassword",
                UserName = "test@test.com",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.PasswordExpiredChangeAsync(passwordExpiredViewModel).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task Register_DifferentPasswordsInTheModel_ReturnsToastError()
        {
            var model = CreateRegisterViewModel();
            model.ConfirmPassword = "NotAnyPassword";
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.RegisterAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task Register_NotFoundResponse_ReturnsToastError()
        {
            var model = CreateRegisterViewModel();
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.RegisterAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task Register_UnaouthorizedAPIResponse_ReturnsToastError()
        {
            var model = CreateRegisterViewModel();
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.RegisterAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ResetPassword_NotFoundResponse_ReturnsToastError()
        {
            var model = new ResetPasswordViewModel()
            {
                ConfirmPassword = "anyPassword",
                Password = "anyPassword",
                Email = "test@test.com",
                Token = "anyString",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.ResetPasswordAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "AccountController-MVC")]
        public async Task ResetPassword_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var model = new ResetPasswordViewModel()
            {
                ConfirmPassword = "anyPassword",
                Password = "anyPassword",
                Email = "test@test.com",
                Token = "anyString",
            };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateAccountController(httpClientFactory, proxyHelper);

            var result = await controller.ResetPasswordAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        private static LoginViewModel CreateLoginViewModel()
        {
            var email = "test@test.com";
            var password = "anyPassword";
            var loginViewModel = new LoginViewModel()
            {
                Email = email,
                Password = password,
                RememberMe = true,
                ReturnUrl = "anyUrl",
            };

            return loginViewModel;
        }

        private static RegisterViewModel CreateRegisterViewModel()
        {
            var email = "test@test.com";
            var password = "anyPassword";
            var registerViewModel = new RegisterViewModel()
            {
                Email = email,
                Password = password,
                ConfirmPassword = password,
            };
            return registerViewModel;
        }

        private static ForgotPasswordViewModel CreateForgotPasswordViewModel()
        {
            var email = "test@test.com";

            var forgotPasswordViewModel = new ForgotPasswordViewModel()
            {
                Email = email,
            };
            return forgotPasswordViewModel;
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private AccountController CreateAccountController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null, List<Claim> claims = null)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new AccountController(
                _loggerMoq.Object,
                _localizationMoq.Object,
                _captchaValidatorMoq.Object,
                _configurationMoq.Object,
                _claimHelperMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            _ = _captchaValidatorMoq.Setup(s => s.IsCaptchaPassedAsync(It.IsAny<string>())).ReturnsAsync(true);
            _ = _configurationMoq.SetupGet(x => x[It.IsAny<string>()]).Returns("true");

            return controller;
        }

        private IUrlHelperFactory CreateUrlHelperFactory()
        {
            _ = _urlHelperMock.Setup(s => s.Action(It.IsAny<UrlActionContext>())).Returns("anyString");
            _ = _urlHelperFactoryMock.Setup(s => s.GetUrlHelper(It.IsAny<ActionContext>())).Returns(_urlHelperMock.Object);
            return _urlHelperFactoryMock.Object;
        }
    }
}