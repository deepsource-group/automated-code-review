﻿// <copyright file="UserControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.ClaimHelper;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using KocSistem.CommunityEdition.Mvc.Models.Paging;
using KocSistem.CommunityEdition.Mvc.Models.User;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class UserControllerTest
    {
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IKsI18N> _localizationMock;
        private readonly Mock<ILogger<UserController>> _loggerMock;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly string _errorMessage = "sometext";
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":8,\"totalPages\":8,\"items\":[{\"id\":\"0725eb17-5a6a-4b07-8b36-c9ded6b0f14e\",\"username\":\"admin6u3612ser@kocsistem.com.tr\"}]}";

        public UserControllerTest()
        {
            _localizationMock = new Mock<IKsI18N>();
            _loggerMock = new Mock<ILogger<UserController>>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public void Create_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = controller.Create();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Delete_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync("AnyName").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Delete_OkAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync("AnyName").ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Delete_UnauthorizedResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.DeleteAsync("AnyName").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Get_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var data = new ServiceResponse<PagedResult<ApiUserGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Get_OkAPIResponse_ReturnsOK()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiUserGetResponseModel>>(new PagedResult<ApiUserGetResponseModel>(), true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Get_OkAPIResponsewithSearchValue_ReturnsOK()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiUserGetResponseModel>>(new PagedResult<ApiUserGetResponseModel>(), true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, new Search("anyText", true), null, null);

            var result = await controller.GetAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Get_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var data = new ServiceResponse<PagedResult<ApiUserGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public void Get_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = controller.Get();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task GetRoleAssignments_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiRoleAssignmentResponse>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleAssignmentsAsync("anyName").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }


        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task GetRoleAssignments_OkAPIResponse_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));

            var responseData = new List<ApiRoleAssignmentResponse>();
            responseData.Add(new ApiRoleAssignmentResponse() { IsAssigned = false, RoleName = "anyrole" });
            responseData.Add(new ApiRoleAssignmentResponse() { IsAssigned = true, RoleName = "anyAsssignedRole" });

            var data = new ServiceResponse<List<ApiRoleAssignmentResponse>>(responseData);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleAssignmentsAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<PartialViewResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task GetRoleAssignments_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiRoleAssignmentResponse>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.GetRoleAssignmentsAsync("anyName").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task GetUserInfo_NotFoundResponse_RedirectUserList()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<UserViewModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.GetUserInfoAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task GetUserInfo_UnauthorizedAPIResponse_RedirectUserList()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<UserViewModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.GetUserInfoAsync("anyName").ConfigureAwait(false);

            Assert.IsAssignableFrom<ViewResult>(result);
            var response = (ViewResult)result;
            Assert.Equal("Get", response.ViewName);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task GetUserInfo_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<UserViewModel>(new UserViewModel() { Name = "John" });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.GetUserInfoAsync("John").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<PartialViewResult>(result);
            var viewResult = (PartialViewResult)result;
            var resultItem = (UserViewModel)viewResult.Model;
            Assert.Equal("John", resultItem.Name);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Post_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new UserViewModel()
            {
                Email = "test@test.com",
                Id = Guid.NewGuid().ToString(),
                Name = "John",
                PhoneNumber = "00000000000",
                Surname = "Doe",
            };

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Post_OkAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(null, true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new UserViewModel()
            {
                Email = "test@test.com",
                Id = Guid.NewGuid().ToString(),
                Name = "John",
                PhoneNumber = "00000000000",
                Surname = "Doe",
            };

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Post_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new UserViewModel()
            {
                Email = "test@test.com",
                Id = Guid.NewGuid().ToString(),
                Name = "John",
                PhoneNumber = "00000000000",
                Surname = "Doe",
            };

            var result = await controller.PostAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Put_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(new ErrorInfo(_errorMessage), false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new UserPutViewModel() { };

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Put_OkAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(null, true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new UserPutViewModel() { };

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task PutUserRole_OkAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(null, true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new UserRolePutViewModel()
            {
                Username = "guestuser@kocsistem.com.tr",
                AssignedRoles = new List<string>() { "role" },
                UnassignedRoles = new List<string>() { "role" },
            };

            var result = await controller.PutUserRoleAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task PutUserRole_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new UserRolePutViewModel()
            {
                Username = "guestuser@kocsistem.com.tr",
                AssignedRoles = new List<string>() { "role" },
                UnassignedRoles = new List<string>() { "role" },
            };

            var result = await controller.PutUserRoleAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Put_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new UserPutViewModel() { };

            var result = await controller.PutAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task SaveUserClaims_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new SaveUserClaimsModel()
            {
                Name = "anyName",
                SelectedUserClaimList = new List<string>() { "claim" },
            };

            var result = await controller.SaveUserClaimsAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task SaveUserClaims_OkAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(null, true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new SaveUserClaimsModel()
            {
                Name = "anyName",
                SelectedUserClaimList = new List<string>() { "claim" },
            };

            var result = await controller.SaveUserClaimsAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task SaveUserClaims_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var model = new SaveUserClaimsModel()
            {
                Name = "anyName",
                SelectedUserClaimList = new List<string>() { "claim" },
            };

            var result = await controller.SaveUserClaimsAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Search_OkAPIResponse_ReturnsOK()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiUserGetResponseModel>>(new PagedResult<ApiUserGetResponseModel>(), true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var userSearchRequest = CreateUserSearchRequest();

            var result = await controller.SearchAsync(userSearchRequest).ConfigureAwait(false) as OkObjectResult;

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Search_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiUserGetResponseModel>>(new PagedResult<ApiUserGetResponseModel>(), _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var userSearchRequest = CreateUserSearchRequest();

            var result = await controller.SearchAsync(userSearchRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task Search_NotFound_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiUserGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateUserController(httpClientFactory, proxyHelper);
            var userSearchRequest = CreateUserSearchRequest();

            var result = await controller.SearchAsync(userSearchRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task UserClaims_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<UserViewModel>>(new PagedResult<UserViewModel>(), _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.UserClaimsAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task UserClaims_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<UserViewModel>>(new PagedResult<UserViewModel>(), _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.UserClaimsAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task UserClaims_Success_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var models = new List<UserViewModel>() { new UserViewModel() { Name = "John", Surname = "Doe", Email = "test@test.com" } };
            var data = new ServiceResponse<PagedResult<UserViewModel>>(new PagedResult<UserViewModel>() { Items = models });
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.UserClaimsAsync().ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task UserClaimswithUsername_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiClaimTreeViewItem>>(new List<ApiClaimTreeViewItem>(), _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.UserClaimsAsync("anyName").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task UserClaimswithUsername_OkAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiClaimTreeViewItem>>(new List<ApiClaimTreeViewItem>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.UserClaimsAsync("anyName").ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "UserController-MVC")]
        public async Task UserClaimswithUsername_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<UserController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiClaimTreeViewItem>>(new List<ApiClaimTreeViewItem>(), _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateUserController(httpClientFactory, proxyHelper);

            var result = await controller.UserClaimsAsync("anyName").ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private static DataTablesRequest CreateDataTablesRequest()
        {
            return new DataTablesRequest(1, 0, 10, null, null, null);
        }

        private static PagedRequest CreatePagedRequest()
        {
            return new PagedRequest()
            {
                PageIndex = 0,
                PageSize = 1,
            };
        }

        private UserController CreateUserController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new UserController(_localizationMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();

            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
              .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
              .Returns(proxyHelper);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }

        private static UserSearchRequest CreateUserSearchRequest()
        {
            return new UserSearchRequest()
            {
                Username = "admin6u3612ser@kocsistem.com.tr",
            };
        }
    }
}
