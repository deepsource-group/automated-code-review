﻿// <copyright file="LoginAuditLogControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using KocSistem.CommunityEdition.Mvc.Models.LoginAuditLog;
using KocSistem.CommunityEdition.Mvc.Models.Paging;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;


namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class LoginAuditLogControllerTest
    {
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";

        public LoginAuditLogControllerTest()
        {
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<LoginAuditLogController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public void Get_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse(true);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = controller.Get();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task GetList_RequestModelSearchNotNull_ReturnOkObjectResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LoginAuditLogGetResponseModel>>(new PagedResult<LoginAuditLogGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "name", true, true, "name", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
            Assert.Equal("name", dataColumn.Name);
            Assert.True(dataColumn.Searchable);
            Assert.True(dataColumn.Orderable);
            Assert.Equal("name", dataColumn.SearchValue);
            Assert.True(dataColumn.SearchRegEx);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task GetList_Success_ReturnOkObjectResult()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LoginAuditLogGetResponseModel>>(new PagedResult<LoginAuditLogGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task GetList_NotFound_ReturnToastError()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LoginAuditLogGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task GetList_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LoginAuditLogGetResponseModel>>(new PagedResult<LoginAuditLogGetResponseModel>(), _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }


        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task Search_NotFound_ReturnToastError()
        {
            var model = new LoginAuditLogSearchRequest() { Value = "anyvalue", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LoginAuditLogGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task Search_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var model = new LoginAuditLogSearchRequest() { Value = "anyvalue", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LoginAuditLogGetResponseModel>>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task Search_Success_ReturnOkObjectResult()
        {
            var model = new LoginAuditLogSearchRequest() { Value = "anyvalue", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PagedResult<LoginAuditLogGetResponseModel>>(new PagedResult<LoginAuditLogGetResponseModel>());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task ExcelExport_NotFound_ReturnToastError()
        {
            var model = new LoginAuditLogFilterRequest() { StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1) };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<ExcelExportResponseModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.ExcelExportAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task ExcelExport_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var model = new LoginAuditLogFilterRequest() { StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1) };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<ExcelExportResponseModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.ExcelExportAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task ExcelExport_Success_ReturnOkObjectResult()
        {
            var model = new LoginAuditLogFilterRequest() { StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1) };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<ExcelExportResponseModel>(new ExcelExportResponseModel());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.ExcelExportAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task PdfExport_NotFound_ReturnToastError()
        {
            var model = new LoginAuditLogFilterRequest() { StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1) };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse<PdfExportResponseModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.PdfExportAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task PdfExport_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var model = new LoginAuditLogFilterRequest() { StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1) };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            var data = new ServiceResponse<PdfExportResponseModel>(null, _errorInfo, false);
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.PdfExportAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "LoginAuditLogController-MVC")]
        public async Task PdfExport_Success_ReturnOkObjectResult()
        {
            var model = new LoginAuditLogFilterRequest() { StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddDays(1).AddSeconds(-1) };
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse<PdfExportResponseModel>(new PdfExportResponseModel());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateLoginAuditLogController(httpClientFactory, proxyHelper);

            var result = await controller.PdfExportAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private LoginAuditLogController CreateLoginAuditLogController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null, List<Claim> claims = null)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new LoginAuditLogController(_localizationMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            return controller;
        }
    }
}
