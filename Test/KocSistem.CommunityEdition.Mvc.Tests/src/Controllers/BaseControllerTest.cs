﻿// <copyright file="BaseControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Captcha;
using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Helpers;
using KocSistem.CommunityEdition.Mvc.Jwt.JwtTokenValidationSettings;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class BaseControllerTest
    {
        private readonly Mock<IJwtTokenValidationSettings> _validationMoq;
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly Mock<ILogger<AccountController>> _loggerMoq;
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";
        private readonly Mock<ICaptchaValidator> _captchaValidatorMoq;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IClaimHelper> _claimHelperMoq;

        public BaseControllerTest()
        {
            _configurationMoq = new Mock<IConfiguration>();
            _captchaValidatorMoq = new Mock<ICaptchaValidator>();
            _validationMoq = new Mock<IJwtTokenValidationSettings>();
            _loggerMoq = new Mock<ILogger<AccountController>>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<AccountController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _claimHelperMoq = new Mock<IClaimHelper>();
        }

        [Fact]
        public void ToastError_ErrorInfo()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateAccountController(httpClientFactory, proxyHelper);
            var result = controller.ToastError(_errorInfo);

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        public void ToastError_StringMessage()
        {
            var message = "testMessage";
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateAccountController(httpClientFactory, proxyHelper);
            var result = controller.ToastError(message);

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        public void ToastError_ErrorInfo_StringMessage()
        {
            var message = "testMessage";
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateAccountController(httpClientFactory, proxyHelper);
            var result = controller.ToastError(_errorInfo, message);

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        private AccountController CreateAccountController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null, List<Claim> claims = null)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new AccountController(_loggerMoq.Object, _localizationMoq.Object, _captchaValidatorMoq.Object, _configurationMoq.Object, _claimHelperMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            _ = _captchaValidatorMoq.Setup(s => s.IsCaptchaPassedAsync(It.IsAny<string>())).ReturnsAsync(true);
            _ = _configurationMoq.SetupGet(x => x[It.IsAny<string>()]).Returns("true");

            return controller;
        }
    }
}