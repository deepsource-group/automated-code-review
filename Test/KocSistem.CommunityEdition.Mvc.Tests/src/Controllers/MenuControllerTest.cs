﻿using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.Menu;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class MenuControllerTest
    {
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IKsI18N> _localizationMock;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";

        public MenuControllerTest()
        {
            _localizationMock = new Mock<IKsI18N>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
        }

        [Fact]
        [Trait("Category", "MenuController-MVC")]
        public void MenuOrder_ReturnsViewResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateMenuController(httpClientFactory, proxyHelper);

            var result = controller.MenuOrder();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }
        
        [Fact]
        [Trait("Category", "MenuController-MVC")]
        public async Task MenuTree_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<MenuController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiMenuTreeViewItem>>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateMenuController(httpClientFactory, proxyHelper);

            var result = await controller.MenuTreeAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "MenuController-MVC")]
        public async Task MenuTree_OkAPIResponse_ReturnsToastSuccess()
        {
            var claimState = new ApiMenuTreeViewItemStateInfo()
            {
                Disabled = false,
                Opened = true,
                Selected = true,
            };

            var claimTree = new ApiMenuTreeViewItem()
            {
                Id = "1",
                Text = "Menu name",
                OrderId = 0,
                State = claimState,
                Children = new List<ApiMenuTreeViewItem>(),
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<MenuController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiMenuTreeViewItem>>(new List<ApiMenuTreeViewItem>() { claimTree });

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateMenuController(httpClientFactory, proxyHelper);

            var result = await controller.MenuTreeAsync().ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(result);
            var response = (OkObjectResult)result;
            var resultItem = (List<ApiMenuTreeViewItem>)response.Value;
            Assert.Single(resultItem);
        }

        [Fact]
        [Trait("Category", "MenuController-MVC")]
        public async Task MenuTree_UnauthorizedResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<MenuController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<List<ApiMenuTreeViewItem>>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateMenuController(httpClientFactory, proxyHelper);

            var result = await controller.MenuTreeAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "MenuController-MVC")]
        public async Task SaveMenuOrder_NotFoundResponse_ReturnsToastError()
        {
            var menuList = new List<SaveMenuOrderItemModel>()
            {
                new SaveMenuOrderItemModel()
                {
                    Id = 1,
                    ParentId = null,
                    OrderId = 0,
                },
                new SaveMenuOrderItemModel()
                {
                    Id = 2,
                    ParentId = 0,
                    OrderId = 1,
                },
            };

            var model = new SaveMenuOrderModel()
            {
                MenuList = menuList,
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<MenuController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateMenuController(httpClientFactory, proxyHelper);

            var result = await controller.SaveMenuOrderAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "MenuController-MVC")]
        public async Task SaveMenuOrder_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var menuList = new List<SaveMenuOrderItemModel>()
            {
                new SaveMenuOrderItemModel()
                {
                    Id = 1,
                    ParentId = null,
                    OrderId = 0,
                },
                new SaveMenuOrderItemModel()
                {
                    Id = 2,
                    ParentId = 0,
                    OrderId = 1,
                },
            };

            var model = new SaveMenuOrderModel()
            {
                MenuList = menuList,
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<MenuController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(null, true);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateMenuController(httpClientFactory, proxyHelper);

            var result = await controller.SaveMenuOrderAsync(model).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "MenuController-MVC")]
        public async Task SaveMenuOrder_UnauthorizedResponse_ReturnsToastError()
        {
            var menuList = new List<SaveMenuOrderItemModel>()
            {
                new SaveMenuOrderItemModel()
                {
                    Id = 1,
                    ParentId = null,
                    OrderId = 0,
                },
                new SaveMenuOrderItemModel()
                {
                    Id = 2,
                    ParentId = 0,
                    OrderId = 1,
                },
            };

            var model = new SaveMenuOrderModel()
            {
                MenuList = menuList,
            };

            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMock.SetupGet(x => x.GetLocalizer<MenuController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(_errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateMenuController(httpClientFactory, proxyHelper);

            var result = await controller.SaveMenuOrderAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private MenuController CreateMenuController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null, List<Claim> claims = null)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new MenuController(_localizationMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
                .Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            return controller;
        }
    }
}
