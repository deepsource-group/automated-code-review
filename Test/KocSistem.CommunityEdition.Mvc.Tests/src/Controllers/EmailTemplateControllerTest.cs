﻿// <copyright file="EmailTemplateControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Common.Enums;
using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using KocSistem.CommunityEdition.Mvc.Models.EmailTemplate;
using KocSistem.CommunityEdition.Mvc.Models.Paging;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class EmailTemplateControllerTest
    {
        private readonly ErrorInfo errorInfo = new("sometext");
        private readonly Mock<IKsI18N> localizationMock;
        private readonly Mock<IHttpClientFactory> httpClientFactoryMock;
        private readonly Mock<IClientProxy> proxyHelperMock;
        private readonly string sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";

        public EmailTemplateControllerTest()
        {
            localizationMock = new Mock<IKsI18N>();
            httpClientFactoryMock = new Mock<IHttpClientFactory>();
            proxyHelperMock = new Mock<IClientProxy>();
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public void Get_Success_ReturnsViewResult()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse();
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);

            var result = controller.Get();

            Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task GetEmailTemplateInfo_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<EmailTemplateViewModel>(new EmailTemplateViewModel
            {
                Id = Guid.NewGuid(),
                Name = "anyName",
                To = "test@test.com",
                Cc = "test@test.com",
                Bcc = "test@test.com",
                UpdatedUser = "testUser",
                UpdatedDate = DateTime.UtcNow,
                Translations = new List<EmailTemplateTranslationsModel> { new EmailTemplateTranslationsModel { EmailContent = "anyEmailContent", Subject = "anySubject", Language = LanguageType.en } }
            }, errorInfo, false);

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);

            var result = await controller.GetEmailTemplateInfoAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task GetEmailTemplateInfo_Success_ReturnsViewResult()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse<EmailTemplateViewModel>(new EmailTemplateViewModel()
            {
                Id = Guid.Parse("9459E146-558A-44C5-972E-28A2469B4D1A"),
                Name = "emailTemplateName",
                To = "test@test.com",
                Cc = "test@test.com",
                Bcc = "test@test.com",
                UpdatedUser = "testUser",
                UpdatedDate = DateTime.UtcNow,
                Translations = new List<EmailTemplateTranslationsModel> { new EmailTemplateTranslationsModel { EmailContent = "anyEmailContent", Subject = "anySubject", Language = LanguageType.en } }
            });
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);

            var result = await controller.GetEmailTemplateInfoAsync(Guid.Parse("9459e146-558a-44c5-972e-28a2469b4d1a")).ConfigureAwait(false);

            Assert.IsAssignableFrom<ViewResult>(result);
            var viewResult = (ViewResult)result;
            var resultItem = (EmailTemplateViewModel)viewResult.Model;
            Assert.Equal("emailTemplateName", resultItem.Name);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task GetEmailTemplateInfo_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<EmailTemplateViewModel>(null, errorInfo, false);

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);

            var result = await controller.GetEmailTemplateInfoAsync(Guid.NewGuid()).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task GetList_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiEmailTemplateGetResponseModel>>(null, errorInfo, false);

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task GetList_OkAPIResponse_ReturnsOK()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiEmailTemplateGetResponseModel>>(new PagedResult<ApiEmailTemplateGetResponseModel>(), true);

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task GetList_RequestModelSearchNotNull_ReturnsOk()
        {
            var res = new ApiEmailTemplateGetResponseModel()
            {
                Id = Guid.NewGuid(),
                Name = "anyName",
                UpdatedDate = DateTime.UtcNow,
                SupportedLanguages = "en"
            };
            var pagedResult = new PagedResult<ApiEmailTemplateGetResponseModel>()
            {
                PageIndex = 0,
                PageSize = 10,
                TotalCount = 1,
                TotalPages = 1,
                Items = new List<ApiEmailTemplateGetResponseModel>() { res },
            };
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse<PagedResult<ApiEmailTemplateGetResponseModel>>(pagedResult);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "name", true, true, "name", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });

            var response = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            Assert.IsAssignableFrom<OkObjectResult>(response);
            Assert.Equal("name", dataColumn.Name);
            Assert.True(dataColumn.Searchable);
            Assert.True(dataColumn.Orderable);
            Assert.Equal("name", dataColumn.SearchValue);
            Assert.True(dataColumn.SearchRegEx);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task GetList_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiEmailTemplateGetResponseModel>>(null, errorInfo, false);

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var dataTablesRequest = CreateDataTablesRequest();

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task Put_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(errorInfo, false);

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var emailTemplateViewModel = CreateEmailTemplatePutViewModel();

            var result = await controller.PutAsync(emailTemplateViewModel).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task Put_OkAPIResponse_ReturnsToastSuccess()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse();

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var emailTemplateViewModel = CreateEmailTemplatePutViewModel();

            var result = await controller.PutAsync(emailTemplateViewModel).ConfigureAwait(false) as JsonResult;

            Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task Put_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(errorInfo, false);

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var emailTemplateViewModel = CreateEmailTemplatePutViewModel();

            var result = await controller.PutAsync(emailTemplateViewModel).ConfigureAwait(false) as JsonResult;

            Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task SendEmail_OkAPIResponse_ReturnsToastSuccess()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse();

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var sendTryMailModel = CreateSendTryMailViewModel();

            var result = await controller.SendEmailAsync(sendTryMailModel).ConfigureAwait(false) as JsonResult;

            Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task SendTryMail_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse(errorInfo, false);

            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);
            var sendTryMailModel = CreateSendTryMailViewModel();

            var result = await controller.SendEmailAsync(sendTryMailModel).ConfigureAwait(false) as JsonResult;

            Assert.IsAssignableFrom<JsonResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailTemplateController-MVC")]
        public async Task Search_NotFound_ReturnsToastError()
        {
            var req = new EmailTemplateSearchRequest()
            {
                Name = "anyName"
            };
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, sampleResponse);
            localizationMock.SetupGet(x => x.GetLocalizer<EmailTemplateController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<PagedResult<ApiEmailTemplateGetResponseModel>>(null, errorInfo, false);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailTemplateController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(req).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private static DataTablesRequest CreateDataTablesRequest()
        {
            return new DataTablesRequest(1, 0, 10, null, null, null);
        }

        private static EmailTemplatePutModel CreateEmailTemplatePutViewModel()
        {
            return new EmailTemplatePutModel()
            {
                Id = Guid.NewGuid(),
                To = "test@test.com",
                Bcc = "test@test.com",
                Cc = "test@test.com",
                Translations = new List<EmailTemplateTranslationsModel>
                {
                    new EmailTemplateTranslationsModel {   EmailContent= "anyEmailContent", Subject = "anySubject", Language = LanguageType.en },
                }
            };
        }

        private static SendTryMailModel CreateSendTryMailViewModel()
        {
            return new SendTryMailModel()
            {
                To = "test@test.com",
                Subject = "test subject",
                Content = "test content"
            };
        }

        private EmailTemplateController CreateEmailTemplateController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new EmailTemplateController(localizationMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}