﻿// <copyright file="ConfigurationControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Models.FileUpload;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class ConfigurationControllerTest
    {
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly ErrorInfo _errorInfo = new("sometext");
        private readonly string _sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"name\":\"Admin\",\"description\":\"Admin desc\"}]}";

        public ConfigurationControllerTest()
        {
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<ConfigurationController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _proxyHelperMock = new Mock<IClientProxy>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
        }

        [Fact]
        [Trait("Category", "ConfigurationController-MVC")]
        public async Task GetFileUploaderOptions_UnauthorizedAPIResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, _sampleResponse);
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<ConfigurationController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<FileUploaderConfigurationOptions>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateConfigurationController(httpClientFactory, proxyHelper);

            var result = await controller.GetFileUploaderOptionsAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ConfigurationController-MVC")]
        public async Task GetFileUploaderOptions_NotFoundResponse_ReturnsToastError()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, _sampleResponse);
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<ConfigurationController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<FileUploaderConfigurationOptions>(null, _errorInfo, false);

            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateConfigurationController(httpClientFactory, proxyHelper);

            var result = await controller.GetFileUploaderOptionsAsync().ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "ConfigurationController-MVC")]
        public async Task GetFileUploaderOptions_ReturnsContentResult()
        {
            var httpClientFactory = _httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, _sampleResponse);
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<ConfigurationController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            var data = new ServiceResponse<FileUploaderConfigurationOptions>(new FileUploaderConfigurationOptions());
            var proxyHelper = _proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateConfigurationController(httpClientFactory, proxyHelper);

            var result = await controller.GetFileUploaderOptionsAsync().ConfigureAwait(false);
            
            Assert.IsAssignableFrom<ContentResult>(result);
            var response = (ContentResult)result;
            Assert.IsAssignableFrom<string>(response.Content);
            Assert.NotNull(response.Content);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private ConfigurationController CreateConfigurationController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new ConfigurationController(_localizationMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }
    }
}
