﻿// <copyright file="AuthenticationControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Helpers;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text.Encodings.Web;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class AuthenticationControllerTest
    {
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMock;
        private readonly Mock<IClientProxy> _proxyHelperMock;
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<IUrlHelperFactory> _urlHelperFactoryMock;
        private readonly Mock<IUrlHelper> _urlHelperMock;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IClaimHelper> _claimHelperMoq;
        private readonly Mock<UrlEncoder> _urlEncoderMoq;
        //private readonly string dbConfiguration = String.Empty;

        public AuthenticationControllerTest()
        {
            _configurationMoq = new Mock<IConfiguration>();
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _proxyHelperMock = new Mock<IClientProxy>();
            _urlHelperFactoryMock = new Mock<IUrlHelperFactory>();
            _urlHelperMock = new Mock<IUrlHelper>();
            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<AuthenticationController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _claimHelperMoq = new Mock<IClaimHelper>();
            _urlEncoderMoq = new Mock<UrlEncoder>();

            //var applicationSettingList = new Dictionary<string, dynamic>
            //{
            //    { ConfigurationConstant.Identity_2FASettings_IsEnabled, true },
            //    { ConfigurationConstant.Identity_2FASettings_Type, Login2FA.EMAIL.ToString() },
            //    { ConfigurationConstant.Identity_2FASettings_VerificationTime, 60 },
            //    { ConfigurationConstant.Identity_2FASettings_AuthenticatorLinkName, "text" },
            //};

            //dbConfiguration = JsonConvert.SerializeObject(applicationSettingList);
        }

        //[Fact]
        //[Trait("Category", "AuthenticationController-MVC")]
        //public async Task Login2FA_Success_ReturnsViewModel()
        //{
        //    var httpClientFactory = this.httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, this.sampleResponse);
        //    var data = new ServiceResponse();
        //    var proxyHelper = this.proxyHelperMock.GetProxyHelper(data);
        //    var urlHelperFactory = this.CreateUrlHelperFactory();

        //    using var controller = this.CreateAuthenticationController(httpClientFactory, proxyHelper, urlHelperFactory);
        //    var result = await controller.Login2FA().ConfigureAwait(false);

        //    Assert.IsAssignableFrom<ViewResult>(result);
        //}

        //[Fact]
        //[Trait("Category", "AuthenticationController-MVC")]
        //public async Task Login2FA_Authenticator_Success_ReturnsViewModel()
        //{
        //    var httpClientFactory = this.httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, this.sampleResponse);
        //    var data = new ServiceResponse<AuthenticatorResponseViewModel>(new AuthenticatorResponseViewModel() { HasAuthenticatorKey = true, IsActivated = true, SharedKey = It.IsAny<string>() }, this.errorInfo, false);
        //    var proxyHelper = this.proxyHelperMock.GetProxyHelper(data);
        //    var urlHelperFactory = this.CreateUrlHelperFactory();

        //    using var controller = this.CreateAuthenticationController(httpClientFactory, proxyHelper, urlHelperFactory);
        //    var result = await controller.Login2FA().ConfigureAwait(false);

        //    Assert.IsAssignableFrom<ViewResult>(result);
        //}

        //[Fact]
        //[Trait("Category", "AuthenticationController-MVC")]
        //public async Task Login2FA_Verification_ReturnsToastSuccess()
        //{
        //    this.configurationMoq.SetupGet(x => x[It.IsAny<string>()]).Returns(Login2FA.EMAIL.ToString());

        //    var httpClientFactory = this.httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, this.sampleResponse);
        //    var data = new ServiceResponse(null, true);
        //    var proxyHelper = this.proxyHelperMock.GetProxyHelper(data);
        //    var data2 = new ServiceResponse<LoginResponseViewModel>(It.IsAny<LoginResponseViewModel>(), true);
        //    proxyHelper = this.proxyHelperMock.GetProxyHelper(data2);
        //    var urlHelperFactory = this.CreateUrlHelperFactory();

        //    using var controller = this.CreateAuthenticationController(httpClientFactory, proxyHelper, urlHelperFactory);

        //    var model = new Login2FAViewModel()
        //    {
        //        VerificationCode = It.IsAny<string>()
        //    };

        //    var result = await controller.Login2FA(model).ConfigureAwait(false) as JsonResult;
        //    this.AssertToastSuccess(result);
        //}

        //[Fact]
        //[Trait("Category", "AuthenticationController-MVC")]
        //public async Task Login2FA_Verification_ReturnsToastError()
        //{
        //    var httpClientFactory = this.httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, this.sampleResponse);
        //    var data = new ServiceResponse(this.errorInfo, false);
        //    var proxyHelper = this.proxyHelperMock.GetProxyHelper(data);
        //    var urlHelperFactory = this.CreateUrlHelperFactory();
        //    this.configurationMoq.SetupGet(x => x[It.IsAny<string>()]).Returns(Login2FA.EMAIL.ToString());

        //    using var controller = this.CreateAuthenticationController(httpClientFactory, proxyHelper, urlHelperFactory);
        //    var model = new Login2FAViewModel()
        //    {
        //        VerificationCode = It.IsAny<string>()
        //    };

        //    var result = await controller.Login2FA(model).ConfigureAwait(false) as JsonResult;

        //    this.AssertToastError(result);
        //}

        private AuthenticationController CreateAuthenticationController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());
            var test = "{ 'Email':'adminuser@kocsistem.com.tr','Password':'123456','RememberMe':false,'ReturnUrl':null}";
            tempData.Add("LoginModel", test);

            var controller = new AuthenticationController(_localizationMoq.Object, _urlEncoderMoq.Object, _claimHelperMoq.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory))).Returns(httpClientFactory);

            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy))).Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            return controller;
        }

        private IUrlHelperFactory CreateUrlHelperFactory()
        {
            _ = _urlHelperMock.Setup(s => s.Action(It.IsAny<UrlActionContext>())).Returns("anyString");
            _ = _urlHelperFactoryMock.Setup(s => s.GetUrlHelper(It.IsAny<ActionContext>())).Returns(_urlHelperMock.Object);
            return _urlHelperFactoryMock.Object;
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private static void AssertToastSuccess(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("True", resultValue["IsRedirect"].Value<string>());
        }
    }
}