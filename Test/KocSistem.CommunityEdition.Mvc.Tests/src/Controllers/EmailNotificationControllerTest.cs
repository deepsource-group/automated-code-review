﻿// <copyright file="EmailNotificationControllerTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using KocSistem.CommunityEdition.Mvc.Models.Paging;
using KocSistem.CommunityEdition.Mvc.Tests.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Localization;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Mvc.Tests.Controllers
{
    public class EmailNotificationControllerTest
    {
        private readonly ErrorInfo errorInfo = new("sometext");
        private readonly Mock<IKsI18N> localizationMock;
        private readonly Mock<IHttpClientFactory> httpClientFactoryMock;
        private readonly Mock<IClientProxy> proxyHelperMock;
        private readonly string sampleResponse = "{\"pageIndex\":0,\"pageSize\":1,\"totalCount\":2,\"totalPages\":1,\"items\":[{\"id\":\"2d27aa6e-ba08-4468-8614-280351e7d642\",\"subject\":\"Konu\",\"to\":\"Kime\"}]}";

        public EmailNotificationControllerTest()
        {
            httpClientFactoryMock = new Mock<IHttpClientFactory>();
            proxyHelperMock = new Mock<IClientProxy>();
            localizationMock = new Mock<IKsI18N>();
            _ = localizationMock.SetupGet(x => x.GetLocalizer<EmailNotificationController>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public void Get_ReturnsViewResult()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse(true);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = controller.Get();

            _ = Assert.IsAssignableFrom<ViewResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task GetList_RequestModelSearchNotNull_ReturnOkObjectResult()
        {
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>(new PagedResult<EmailNotificationGetResponseModel>());
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);
            var search = new Search("anyString", true);
            var order = new Order(0, "anyString");
            var dataColumn = new DataTableColumn("data", "to", true, true, "to", true);
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, search, new List<Order>() { order }, new List<DataTableColumn>() { dataColumn });

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
            Assert.Equal("to", dataColumn.Name);
            Assert.True(dataColumn.Searchable);
            Assert.True(dataColumn.Orderable);
            Assert.Equal("to", dataColumn.SearchValue);
            Assert.True(dataColumn.SearchRegEx);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task GetList_Success_ReturnOkObjectResult()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>(new PagedResult<EmailNotificationGetResponseModel>());
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task GetList_NotFound_ReturnToastError()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);

            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, sampleResponse);
            var data = new ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>(null, errorInfo, false);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task GetList_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var dataTablesRequest = new DataTablesRequest(1, 0, 10, null, null, null);
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, sampleResponse);
            var data = new ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>(new PagedResult<EmailNotificationGetResponseModel>(), errorInfo, false);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);

            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.GetListAsync(dataTablesRequest).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task Search_NotFound_ReturnToastError()
        {
            var model = new EmailNotificationSearchRequest() { Value = "anyvalue", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, sampleResponse);
            var data = new ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>(null, errorInfo, false);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task Search_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var model = new EmailNotificationSearchRequest() { Value = "anyvalue", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, sampleResponse);
            var data = new ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>(null, errorInfo, false);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task Search_Success_ReturnOkObjectResult()
        {
            var model = new EmailNotificationSearchRequest() { Value = "anyvalue", Orders = new List<PagedRequestOrder>(), PageIndex = 1, PageSize = 2 };
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>(new PagedResult<EmailNotificationGetResponseModel>());
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.SearchAsync(model).ConfigureAwait(false);

            _ = Assert.IsAssignableFrom<OkObjectResult>(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task SendEmailById_NotFound_ReturnToastError()
        {
            var id = Guid.NewGuid();
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.NotFound, sampleResponse);
            var data = new ServiceResponse<bool>(false, errorInfo, false);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.SendEmailByIdAsync(id).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task SendEmailById_UnaouthorizedAPIResponse_ReturnToastError()
        {
            var id = Guid.NewGuid();
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.Unauthorized, sampleResponse);
            var data = new ServiceResponse<bool>(false, errorInfo, false);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.SendEmailByIdAsync(id).ConfigureAwait(false) as JsonResult;

            AssertToastError(result);
        }

        [Fact]
        [Trait("Category", "EmailNotificationController-MVC")]
        public async Task SendEmailById_OkAPIResponse_ReturnsToastSuccessForRedirect()
        {
            var id = Guid.NewGuid();
            var httpClientFactory = httpClientFactoryMock.CreateHttpClientFactory(HttpStatusCode.OK, sampleResponse);
            var data = new ServiceResponse<bool>(true, true);
            var proxyHelper = proxyHelperMock.GetProxyHelper(data);
            using var controller = CreateEmailNotificationController(httpClientFactory, proxyHelper);

            var result = await controller.SendEmailByIdAsync(id).ConfigureAwait(false) as JsonResult;

            _ = Assert.IsAssignableFrom<JsonResult>(result);
        }

        private static void AssertToastError(JsonResult toastResult)
        {
            var resultValue = JObject.Parse(JsonConvert.SerializeObject(toastResult.Value));
            Assert.Equal("Error", resultValue["Type"].Value<string>());
            Assert.StartsWith("sometext", resultValue["Message"].Value<string>(), StringComparison.CurrentCulture);
            Assert.Equal("False", resultValue["IsRedirect"].Value<string>());
        }

        private EmailNotificationController CreateEmailNotificationController(IHttpClientFactory httpClientFactory, IClientProxy proxyHelper, IUrlHelperFactory urlHelperFactory = null, List<Claim> claims = null)
        {
            var httpContext = new DefaultHttpContext();
            var tempData = new TempDataDictionary(httpContext, Mock.Of<ITempDataProvider>());

            var controller = new EmailNotificationController(i18N: localizationMock.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                },
                TempData = tempData,
            };

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock
                .Setup(serviceProvider => serviceProvider.GetService(typeof(IHttpClientFactory)))
                .Returns(httpClientFactory);

            _ = serviceProviderMock
             .Setup(serviceProvider => serviceProvider.GetService(typeof(IClientProxy)))
             .Returns(proxyHelper);

            if (urlHelperFactory != null)
            {
                _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IUrlHelperFactory))).Returns(urlHelperFactory);
            }

            controller.ControllerContext.HttpContext.RequestServices = serviceProviderMock.Object;

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");
                var claimsPrincipal = new ClaimsPrincipal(identity: identity);
                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);

                controller.ControllerContext.HttpContext.User = claimsPrincipal;
            }

            return controller;
        }
    }
}
