﻿// <copyright file="UserPasswordHistoryTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.UserPasswordHistory;
using KocSistem.CommunityEdition.Domain;
using KocSistem.OneFrame.Data;
using KocSistem.OneFrame.Data.Relational;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Configuration;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.UserPasswordHistory
{
    public class UserPasswordHistoryTest
    {
        private readonly Mock<IRepository<ApplicationUserPasswordHistory>> _applicationUserPasswordHistoryRepositoryMoq;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IDataManager> _dataManagerMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IPasswordHasher<ApplicationUser>> _passwordHasherMoq;

        public UserPasswordHistoryTest()
        {
            _applicationUserPasswordHistoryRepositoryMoq = new Mock<IRepository<ApplicationUserPasswordHistory>>();
            _configurationMoq = new Mock<IConfiguration>();
            _dataManagerMoq = new Mock<IDataManager>();
            _mapperMoq = new Mock<IMapper>();
            _passwordHasherMoq = new Mock<IPasswordHasher<ApplicationUser>>();
        }

        [Fact]
        [Trait("Category", "UserPasswordHistoryService")]
        public void PasswordHistoryValidation_ReturnsTrue()
        {
            var password = "newPassword";
            var applicationUser = new ApplicationUser();
            var query = new List<ApplicationUserPasswordHistory>().AsQueryable().BuildMock();
            _ = _applicationUserPasswordHistoryRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<ApplicationUserPasswordHistory, bool>>>(),
                  It.IsAny<Func<IQueryable<ApplicationUserPasswordHistory>, IOrderedQueryable<ApplicationUserPasswordHistory>>>(),
                  It.IsAny<Func<IQueryable<ApplicationUserPasswordHistory>, IIncludableQueryable<ApplicationUserPasswordHistory, object>>>(),
                  It.IsAny<bool>())).Returns(query);
            var service = new UserPasswordHistoryService(_applicationUserPasswordHistoryRepositoryMoq.Object, _mapperMoq.Object, _dataManagerMoq.Object, _configurationMoq.Object, _passwordHasherMoq.Object);

            var result = service.PasswordHistoryValidation(applicationUser, password);
            Assert.True(result);
        }

        [Fact]
        [Trait("Category", "UserPasswordHistoryService")]
        public void PasswordHistoryValidation_ReturnsFalse()
        {
            var password = "newPassword";
            var applicationUser = new ApplicationUser() { Id = Guid.NewGuid() };
            var applicationUserPasswordHistories = new List<ApplicationUserPasswordHistory> { new ApplicationUserPasswordHistory() { UserId = applicationUser.Id } };

            var queryApplicationUserPasswordHistories = applicationUserPasswordHistories.AsQueryable();
            _ = _applicationUserPasswordHistoryRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<ApplicationUserPasswordHistory, bool>>>(),
                  It.IsAny<Func<IQueryable<ApplicationUserPasswordHistory>, IOrderedQueryable<ApplicationUserPasswordHistory>>>(),
                  It.IsAny<Func<IQueryable<ApplicationUserPasswordHistory>, IIncludableQueryable<ApplicationUserPasswordHistory, object>>>(),
                  It.IsAny<bool>())).Returns(queryApplicationUserPasswordHistories);

            var passwordVerificationResult = PasswordVerificationResult.Success;
            _ = _passwordHasherMoq.Setup(r => r.VerifyHashedPassword(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).Returns(passwordVerificationResult);
            _ = _configurationMoq.SetupGet(x => x[It.Is<string>(s => s == "Identity:Policy:Password:HistoryLimit")]).Returns("10");
            var service = new UserPasswordHistoryService(_applicationUserPasswordHistoryRepositoryMoq.Object, _mapperMoq.Object, _dataManagerMoq.Object, _configurationMoq.Object, _passwordHasherMoq.Object);

            var result = service.PasswordHistoryValidation(applicationUser, password);
            Assert.False(result);
        }
    }
}