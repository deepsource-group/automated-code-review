﻿// <copyright file="EmailTemplateServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate.Contracts;
using KocSistem.CommunityEdition.Application.EmailTemplate;
using KocSistem.CommunityEdition.Application.Tests.Helpers;
using KocSistem.CommunityEdition.Common.Enums;
using KocSistem.OneFrame.Data;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Localization;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplateTranslation.Contracts;
using Xunit;
using KocSistem.OneFrame.DesignObjects.Models;

namespace KocSistem.CommunityEdition.Application.Tests.EmailTemplate
{
    public class EmailTemplateServiceTest
    {
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<IRepository<Domain.EmailTemplate>> _repositoryMoq;
        private readonly Mock<ILookupNormalizer> _keyNormalizerMoq;
        private readonly Mock<IDataManager> _dataManagerMoq;

        public EmailTemplateServiceTest()
        {
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _repositoryMoq = new Mock<IRepository<Domain.EmailTemplate>>();
            _keyNormalizerMoq = new Mock<ILookupNormalizer>();
            _dataManagerMoq = new Mock<IDataManager>();
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task GetEmailTemplateList_WithPagedRequestWithOrdersDirectionDescFalseCaseSuccess_ReturnsSuccessfulResponse()
        {
            var pagedRequestModel = new PagedRequestDto { PageIndex = 0, PageSize = 10, Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { DirectionDesc = false, ColumnName = "Name" } } };

            var responseData = new PagedResultDto<EmailTemplateDto>()
            {
                Items = new List<EmailTemplateDto>()
                {
                    new EmailTemplateDto
                    {
                        Translations = new List<EmailTemplateTranslationDto>()
                    }
                }
            };

            var dataQueryableMoq = new List<Domain.EmailTemplate>() { new Domain.EmailTemplate() }.AsQueryable().BuildMock();

            _ = _repositoryMoq.Setup(s =>
                    s.GetQueryable(
                        It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                        It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                        It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                        It.IsAny<bool>()))
                .Returns(dataQueryableMoq);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<EmailTemplateDto>>())).Returns(new ServiceResponse<PagedResultDto<EmailTemplateDto>>(responseData, true));
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.EmailTemplate>, PagedResultDto<EmailTemplateDto>>(It.IsAny<IPagedList<Domain.EmailTemplate>>())).Returns(responseData);

            var service = CreateEmailTemplateService();

            var response = await service.GetEmailTemplateListAsync(pagedRequestModel).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(responseData.PageIndex, response.Result.PageIndex);
            Assert.Equal(responseData.PageSize, response.Result.PageSize);
            Assert.Equal(responseData.Items, response.Result.Items);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task GetEmailTemplateList_WithPagedRequestWithOrdersDirectionDescTrueCaseSuccess_ReturnsSuccessfulResponse()
        {
            var pagedRequestModel = new PagedRequestDto { PageIndex = 0, PageSize = 10, Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { DirectionDesc = true, ColumnName = "Name" } } };

            var dataQueryableMoq = new List<Domain.EmailTemplate>() { new Domain.EmailTemplate() }.AsQueryable().BuildMock();

            var responseData = new PagedResultDto<EmailTemplateDto>()
            {
                Items = new List<EmailTemplateDto>()
                {
                    new EmailTemplateDto
                    {
                        Translations = new List<EmailTemplateTranslationDto>()
                    }
                }
            };

            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.EmailTemplate>, PagedResultDto<EmailTemplateDto>>(It.IsAny<IPagedList<Domain.EmailTemplate>>())).Returns(responseData);

            _ = _repositoryMoq.Setup(s =>
                    s.GetQueryable(
                        It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                        It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                        It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                        It.IsAny<bool>()))
                .Returns(dataQueryableMoq);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<EmailTemplateDto>>())).Returns(new ServiceResponse<PagedResultDto<EmailTemplateDto>>(responseData, true));

            var service = CreateEmailTemplateService();

            var response = await service.GetEmailTemplateListAsync(pagedRequestModel).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(responseData.PageIndex, response.Result.PageIndex);
            Assert.Equal(responseData.PageSize, response.Result.PageSize);
            Assert.Equal(responseData.Items, response.Result.Items);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task GetEmailTemplateList_WithPagedRequestWithoutOrdersCaseSuccess_ReturnsSuccessfulResponse()
        {
            var pagedRequestModel = new PagedRequestDto { PageIndex = 0, PageSize = 10 };

            var dataQueryableMoq = new List<Domain.EmailTemplate>() { new Domain.EmailTemplate() }.AsQueryable().BuildMock();
            var responseData = new PagedResultDto<EmailTemplateDto>()
            {
                Items = new List<EmailTemplateDto>()
                {
                    new EmailTemplateDto
                    {
                        Translations = new List<EmailTemplateTranslationDto>()
                    }
                }
            };

            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.EmailTemplate>, PagedResultDto<EmailTemplateDto>>(It.IsAny<IPagedList<Domain.EmailTemplate>>())).Returns(responseData);
            _ = _repositoryMoq.Setup(s =>
                    s.GetQueryable(
                        It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                        It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                        It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                        It.IsAny<bool>()))
                .Returns(dataQueryableMoq);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<EmailTemplateDto>>())).Returns(new ServiceResponse<PagedResultDto<EmailTemplateDto>>(responseData, true));

            var service = CreateEmailTemplateService();

            var response = await service.GetEmailTemplateListAsync(pagedRequestModel).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(responseData.PageIndex, response.Result.PageIndex);
            Assert.Equal(responseData.PageSize, response.Result.PageSize);
            Assert.Equal(responseData.Items, response.Result.Items);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task UpdateEmailTemplate_EmailTemplateNotFound_ReturnsUnsuccessfulResponse()
        {
            var model = new EmailTemplateDto();

            var emailTemplateMoqList = GetEmailTemplateList(new List<Guid> { Guid.NewGuid() }, new List<Guid> { Guid.NewGuid() });

            _ = _repositoryMoq.Setup(s =>
                           s.GetQueryable(
                               It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                               It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                               It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                               It.IsAny<bool>()))
                       .Returns(emailTemplateMoqList.AsQueryable().BuildMock());

            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<EmailTemplateDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<EmailTemplateDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = this.CreateEmailTemplateService();

            var response = await service.UpdateEmailTemplateAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task UpdateEmailTemplate_UpdateAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            var emailTemplateMoqList = GetEmailTemplateList(new List<Guid> { Guid.NewGuid() }, new List<Guid> { Guid.NewGuid() });

            _ = _repositoryMoq.Setup(s =>
                           s.GetQueryable(
                               It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                               It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                               It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                               It.IsAny<bool>()))
                       .Returns(emailTemplateMoqList.AsQueryable().BuildMock());

            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<EmailTemplateDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<EmailTemplateDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = CreateEmailTemplateService();

            var response = await service.UpdateEmailTemplateAsync(TestConstants.EmailTemplateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task UpdateEmailTemplate_Success_ReturnsSuccessfulResponse()
        {
            var emailTemplateMoqList = GetEmailTemplateList(new List<Guid> { Guid.NewGuid() }, new List<Guid> { Guid.NewGuid() });
            var emailTemplateDto = new EmailTemplateDto();

            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                    It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(emailTemplateMoqList.First());

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<EmailTemplateDto>())).Returns(new ServiceResponse<EmailTemplateDto>(emailTemplateDto, true));

            var service = CreateEmailTemplateService();

            var response = await service.UpdateEmailTemplateAsync(TestConstants.UpdateEmailTemplateDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(emailTemplateDto.UpdatedDate, response.Result.UpdatedDate);
            Assert.Equal(emailTemplateDto.UpdatedUser, response.Result.UpdatedUser);
            Assert.Equal(emailTemplateDto.Id, response.Result.Id);
            Assert.Equal(emailTemplateDto.Name, response.Result.Name);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task GetEmailTemplateById_EmailTemplateNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<EmailTemplateDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<EmailTemplateDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));

            var service = this.CreateEmailTemplateService();

            var response = await service.GetEmailTemplateByIdAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task GetEmailTemplateById_Success_ReturnsSuccessfulResponse()
        {
            var emailTemplateMoqList = GetEmailTemplateList(new List<Guid> { Guid.NewGuid() }, new List<Guid> { Guid.NewGuid() });
            var emailTemplateDto = new EmailTemplateDto();
            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                    It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(emailTemplateMoqList.First());
            _ = _mapperMoq.Setup(r => r.Map<Domain.EmailTemplate, EmailTemplateDto>(It.IsAny<Domain.EmailTemplate>())).Returns(emailTemplateDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<EmailTemplateDto>())).Returns(new ServiceResponse<EmailTemplateDto>(emailTemplateDto, true));

            var service = this.CreateEmailTemplateService();

            var response = await service.GetEmailTemplateByIdAsync(Guid.NewGuid()).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(emailTemplateDto.Id, response.Result.Id);
            Assert.Equal(emailTemplateDto.Name, response.Result.Name);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task GetEmailTemplateByName_EmailTemplateNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<EmailTemplateDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<EmailTemplateDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));

            var service = this.CreateEmailTemplateService();

            var response = await service.GetEmailTemplateByNameAsync(It.IsAny<string>()).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task GetEmailTemplateByName_Success_ReturnsSuccessfulResponse()
        {
            var emailTemplateMoqList = GetEmailTemplateList(new List<Guid> { Guid.NewGuid() }, new List<Guid> { Guid.NewGuid() });
            var emailTemplateDto = new EmailTemplateDto();
            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                    It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(emailTemplateMoqList.First());
            _ = _mapperMoq.Setup(r => r.Map<Domain.EmailTemplate, EmailTemplateDto>(It.IsAny<Domain.EmailTemplate>())).Returns(emailTemplateDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<EmailTemplateDto>())).Returns(new ServiceResponse<EmailTemplateDto>(emailTemplateDto, true));

            var service = this.CreateEmailTemplateService();

            var response = await service.GetEmailTemplateByNameAsync("anyName").ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(emailTemplateDto.Name, response.Result.Name);
            Assert.Equal(emailTemplateDto.Id, response.Result.Id);
        }

        [Fact]
        [Trait("Category", "EmailTemplateService")]
        public async Task Search_Success_ReturnsSuccessfulResponse()
        {
            var model = new EmailTemplateSearchDto() { Name = "anyName" };
            var pagedResultDto = new PagedResultDto<EmailTemplateDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<EmailTemplateDto>(),
            };
            var emailTemplateMoqList = GetEmailTemplateList(new List<Guid> { Guid.NewGuid() }, new List<Guid> { Guid.NewGuid() });

            _ = _repositoryMoq.Setup(s =>
                           s.GetQueryable(
                               It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                               It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                               It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                               It.IsAny<bool>()))
                       .Returns(emailTemplateMoqList.AsQueryable().BuildMock());

            _ = _repositoryMoq.Setup(s => s.GetFirstOrDefaultAsync(
                    It.IsAny<Expression<Func<Domain.EmailTemplate, bool>>>(),
                    It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IOrderedQueryable<Domain.EmailTemplate>>>(),
                    It.IsAny<Func<IQueryable<Domain.EmailTemplate>, IIncludableQueryable<Domain.EmailTemplate, object>>>(),
                    It.IsAny<bool>())
                ).ReturnsAsync(emailTemplateMoqList.First());

            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("anyName");

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<EmailTemplateDto>>())).Returns(new ServiceResponse<PagedResultDto<EmailTemplateDto>>(pagedResultDto, true));

            var service = CreateEmailTemplateService();

            var response = await service.SearchAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<EmailTemplateDto>>(response.Result.Items);
        }

        private static List<Domain.EmailTemplate> GetEmailTemplateList(List<Guid> emailTemplateIdList = null, List<Guid> emailTemplateTranslationIdList = null)
        {
            var emailTemplateMoqList = new List<Domain.EmailTemplate>();
            if (emailTemplateIdList != null)
            {
                foreach (var item in emailTemplateIdList)
                {
                    var emailTemplateDto =
                        new Domain.EmailTemplate()
                        {
                            Id = item,
                            Name = "anyName",
                            To = "test@test.com",
                            Bcc = "test@test.com",
                            Cc = "test@test.com",
                            InsertedUser = "testUser",
                            InsertedDate = DateTime.UtcNow,
                            UpdatedUser = "testUser",
                            UpdatedDate = DateTime.UtcNow
                        };
                    if (emailTemplateTranslationIdList != null)
                    {
                        foreach (var translationItem in emailTemplateTranslationIdList)
                        {
                            emailTemplateDto.Translations = new List<Domain.EmailTemplateTranslation>
                            {
                                new Domain.EmailTemplateTranslation() { Id = translationItem, EmailContent = "anyEmailContent", Subject = "anySubject", Language = LanguageType.en }
                            };
                        }
                    }
                    else
                    {
                        emailTemplateDto.Translations = new List<Domain.EmailTemplateTranslation>
                            {
                                new Domain.EmailTemplateTranslation() { Id = Guid.NewGuid(),EmailContent = "anyEmailContent", Subject = "anySubject", Language = LanguageType.en }
                            };
                    }
                    emailTemplateMoqList.Add(emailTemplateDto);
                }
            }

            return emailTemplateMoqList;
        }

        private EmailTemplateService CreateEmailTemplateService()
        {
            var localizationMoq = new Mock<IKsStringLocalizer<EmailTemplateService>>();
            localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            iKsI18Nmoq.Setup(r => r.GetLocalizer<EmailTemplateService>()).Returns(localizationMoq.Object);

            var service = new EmailTemplateService(
                emailTemplateRepository: _repositoryMoq.Object,
                mapper: _mapperMoq.Object,
                dataManager: _dataManagerMoq.Object,
                i18N: iKsI18Nmoq.Object,
                serviceResponseHelper: _serviceResponseHelperMoq.Object,
                keyNormalizer: _keyNormalizerMoq.Object
            );

            return service;
        }
    }
}