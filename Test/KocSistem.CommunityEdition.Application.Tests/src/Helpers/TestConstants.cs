﻿// <copyright file="TestConstants.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Application.Abstractions.Account.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Role.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.RoleTranslation.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.User.Contracts;
using KocSistem.CommunityEdition.Common.Authentication;
using KocSistem.CommunityEdition.Domain;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace KocSistem.CommunityEdition.Application.Tests.Helpers
{
    public static class TestConstants
    {
        public static readonly ApplicationUser AppliationUser = new()
        {
            Id = Guid.NewGuid(),
            Email = "test@test.com",
            UserName = "testName",
            EmailConfirmed = true,
            Name = "testName",
            PhoneNumber = "anyNumber",
            Surname = "testSurname",
        };

        public static readonly ApplicationRoleDto ApplicationRoleDto = new()
        {
            Name = "anyName",
            Translations = new List<ApplicationRoleTranslationDto>
            {
                new ApplicationRoleTranslationDto() { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
            },
        };

        public static readonly List<Claim> ClaimList = new() { new Claim("claimType", "claimValue") };

        public static readonly ResetPasswordDto ResetPasswordDto = new()
        {
            Token = "anyToken",
            RefreshToken = "anyToken",
            Email = "test@test.com",
            Password = "123456",
            ConfirmPassword = "123456",
        };

        public static readonly RoleClaimDto RoleClaimDto = new()
        {
            RoleName = "testRole",
            Name = "claimValue",
        };

        public static readonly RoleUpdateDto RoleUpdateDto = new()
        {
            RoleName = "roleName",
            Translations = new List<ApplicationRoleTranslationDto>
            {
                new ApplicationRoleTranslationDto() { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
            },
            UsersInRole = new List<string>() { "userId1" },
            UsersNotInRole = new List<string>() { "userId2" },
        };

        public static readonly SaveUserClaimsDto SaveUserClaimsDto = new()
        {
            Name = "userName",
            SelectedUserClaimList = new List<string>() { KsPermissionPolicy.ManagementRoleClaimList },
        };

        public static readonly UserDto UserDto = new()
        {
            Id = Guid.NewGuid(),
            Email = "test@test.com",
            Username = "testName",
            EmailConfirmed = true,
            Name = "testName",
            PhoneNumber = "anyNumber",
            Surname = "testSurname",
            Password = "123456",
        };

        public static readonly UserUpdateDto UserUpdateDto = new()
        {
            Email = "test@test.com",
            Name = "testName",
            PhoneNumber = "anyNumber",
            Surname = "testSurname",
            AssignedRoles = new List<string>() { "testRole" },
            UnassignedRoles = new List<string>() { "testRole2" },
        };

        public static readonly UserRoleUpdateDto UserRoleUpdateDto = new()
        {
            AssignedRoles = new List<string>() { "testRole" },
            UnassignedRoles = new List<string>() { "testRole2" },
        };

        public static readonly EmailTemplateDto EmailTemplateDto = new()
        {
            Id = Guid.NewGuid(),
            Name = "testName",
            To = "test@test.com",
            Cc = "test@test.com",
            Bcc = "test@test.com",
            UpdatedUser = "testUser",
            UpdatedDate = DateTime.UtcNow,
            Translations = new List<Abstractions.EmailTemplateTranslation.Contracts.EmailTemplateTranslationDto>() {
             new Abstractions.EmailTemplateTranslation.Contracts.EmailTemplateTranslationDto {
                 Id  = Guid.NewGuid(),
                 EmailContent = "anyEmailContent",
                 Subject = "anySubject",
                 Language = Common.Enums.LanguageType.en
             }}
        };

        public static readonly EmailTemplateDto UpdateEmailTemplateDto = new()
        {
            Id = Guid.Parse("9459e146-558a-44c5-972e-28a2469b4d1a"),
            Name = "testName",
            To = "test@test.com",
            Cc = "test@test.com",
            Bcc = "test@test.com",
            UpdatedUser = "testUser",
            UpdatedDate = DateTime.UtcNow,
            Translations = new List<Abstractions.EmailTemplateTranslation.Contracts.EmailTemplateTranslationDto>() {
             new Abstractions.EmailTemplateTranslation.Contracts.EmailTemplateTranslationDto {
                 Id  = Guid.Parse("8d396fb3-675c-477c-8e14-278bb549fbae"),
                 ReferenceId  = Guid.Parse("9459e146-558a-44c5-972e-28a2469b4d1a"),
                 EmailContent = "anyEmailContent",
                 Subject = "anySubject",
                 Language = Common.Enums.LanguageType.en
             }}
        };
    }
}
