﻿// <copyright file="ApplicationSettingCategoryServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSettingCategory;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSettingCategory.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Setting;
using KocSistem.OneFrame.Data;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.ApplicationSettingCategory
{
    public class ApplicationSettingCategoryServiceTest
    {
        private readonly Mock<IRepository<Domain.ApplicationSettingCategory>> _applicationSettingCategoryRepositoryMoq;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IDataManager> _dataManagerMoq;
        private readonly Mock<ILookupNormalizer> _keyNormalizerMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;

        public ApplicationSettingCategoryServiceTest()
        {
            _applicationSettingCategoryRepositoryMoq = new Mock<IRepository<Domain.ApplicationSettingCategory>>();
            _configurationMoq = new Mock<IConfiguration>();
            _dataManagerMoq = new Mock<IDataManager>();
            _keyNormalizerMoq = new Mock<ILookupNormalizer>();
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task CreateApplicationSetting_IsExistTrue_ReturnsUnsuccessfulResponse()
        {
            var applicationSettingCategory = new Domain.ApplicationSettingCategory() { Name = "" };
            var applicationSettingDto = new ApplicationSettingCategoryDto()
            {
                Id = Guid.NewGuid(),
                Description = "testdescription",
                Name = "",
            };
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSettingCategory, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IOrderedQueryable<Domain.ApplicationSettingCategory>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IIncludableQueryable<Domain.ApplicationSettingCategory, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(applicationSettingCategory);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingCategoryDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.CreateApplicationSettingCategoryAsync(applicationSettingDto).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task CreateApplicationSetting_ReturnsSuccessfulResponse()
        {
            Domain.ApplicationSettingCategory entity = null;
            var applicationSettingDto = new ApplicationSettingCategoryDto()
            {
                Id = Guid.NewGuid(),
                Description = "testdescription",
                Name = "testname",
            };

            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSettingCategory, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IOrderedQueryable<Domain.ApplicationSettingCategory>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IIncludableQueryable<Domain.ApplicationSettingCategory, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(entity);

            _ = _mapperMoq.Setup(r => r.Map<Domain.ApplicationSetting, ApplicationSettingCategoryDto>(It.IsAny<Domain.ApplicationSetting>())).Returns(applicationSettingDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationSettingCategoryDto>())).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(applicationSettingDto, true));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.CreateApplicationSettingCategoryAsync(applicationSettingDto).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(applicationSettingDto.Name, response.Result.Name);
            Assert.Equal(applicationSettingDto.Description, response.Result.Description);
            Assert.Equal(applicationSettingDto.Id, response.Result.Id);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task UpdateApplicationSetting_ApplicationSettingDtoNull_ReturnsUnsuccessfulResponse()
        {
            ApplicationSettingCategoryDto applicationSettingCategoryDto = null;
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingCategoryDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.UpdateApplicationSettingCategoryAsync(applicationSettingCategoryDto).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task UpdateApplicationSetting_ApplicationSettingToUpdateNull_ReturnsUnsuccessfulResponse()
        {
            var applicationSettingCategoryDto = new ApplicationSettingCategoryDto();
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(null, null, null, true)).ReturnsAsync(new Domain.ApplicationSettingCategory());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingCategoryDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.UpdateApplicationSettingCategoryAsync(applicationSettingCategoryDto).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task UpdateApplicationSetting_ReturnsSuccessfulResponse()
        {
            var applicationSettingToUpdate = new Domain.ApplicationSettingCategory()
            {
                Id = Guid.NewGuid(),
                Description = "testdescription",
                Name = "testname",
            };
            var applicationSettingDto = new ApplicationSettingCategoryDto()
            {
                Id = Guid.NewGuid(),
                Description = "testdescription",
                Name = "testname",
            };
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSettingCategory, Domain.ApplicationSettingCategory>>>(),
                  It.IsAny<Expression<Func<Domain.ApplicationSettingCategory, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IOrderedQueryable<Domain.ApplicationSettingCategory>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IIncludableQueryable<Domain.ApplicationSettingCategory, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(applicationSettingToUpdate);

            _ = _mapperMoq.Setup(r => r.Map<Domain.ApplicationSettingCategory, ApplicationSettingCategoryDto>(It.IsAny<Domain.ApplicationSettingCategory>())).Returns(applicationSettingDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationSettingCategoryDto>())).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(applicationSettingDto,  true));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.UpdateApplicationSettingCategoryAsync(applicationSettingDto).ConfigureAwait(false);
            
            Assert.True(response.IsSuccessful);
            Assert.Equal(applicationSettingDto.Name, response.Result.Name);
            Assert.Equal(applicationSettingDto.Description, response.Result.Description);
            Assert.Equal(applicationSettingDto.Id, response.Result.Id);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task DeleteApplicationSettingCategory_ApplicationSettingToDeleteNull_ReturnsUnsuccessfulResponse()
        {
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(null, null, null, true)).ReturnsAsync(new Domain.ApplicationSettingCategory());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.DeleteApplicationSettingCategoryAsync(Guid.NewGuid()).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task DeleteApplicationSettingCategory_ReturnsSuccessfulResponse()
        {
            var applicationSettingToDelete = new Domain.ApplicationSettingCategory()
            {
                Description = "testDescription",
                Name = "testname",
                Id = Guid.NewGuid(),
            };
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSettingCategory, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IOrderedQueryable<Domain.ApplicationSettingCategory>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IIncludableQueryable<Domain.ApplicationSettingCategory, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(applicationSettingToDelete);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(null,  true));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.DeleteApplicationSettingCategoryAsync(applicationSettingToDelete.Id).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task GetApplicationSettingById_ApplicationSettingNull_ReturnsUnsuccessfulResponse()
        {
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(null, null, null, true)).ReturnsAsync(new Domain.ApplicationSettingCategory());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingCategoryDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.GetApplicationSettingCategoryByIdAsync(Guid.NewGuid()).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task GetApplicationSettingById_ApplicationSettingNotNull_ReturnsSuccessfulResponse()
        {
            var applicationSettingCategory = new Domain.ApplicationSettingCategory
            {
                Id = Guid.NewGuid(),
                Description = "testdescription",
                Name = "testname",
            };
            var applicationSettingCategoryDto = new ApplicationSettingCategoryDto
            {
                Id = Guid.NewGuid(),
                Description = "testdescription",
                Name = "testname",
            };
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSettingCategory, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IOrderedQueryable<Domain.ApplicationSettingCategory>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IIncludableQueryable<Domain.ApplicationSettingCategory, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(applicationSettingCategory);

            _ = _mapperMoq.Setup(r => r.Map<Domain.ApplicationSettingCategory, ApplicationSettingCategoryDto>(It.IsAny<Domain.ApplicationSettingCategory>())).Returns(applicationSettingCategoryDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationSettingCategoryDto>())).Returns(new ServiceResponse<ApplicationSettingCategoryDto>(applicationSettingCategoryDto, true));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.GetApplicationSettingCategoryByIdAsync(applicationSettingCategory.Id).ConfigureAwait(false);
            
            Assert.True(response.IsSuccessful);
            Assert.Equal(applicationSettingCategoryDto.Name, response.Result.Name);
            Assert.Equal(applicationSettingCategoryDto.Description, response.Result.Description);
            Assert.Equal(applicationSettingCategoryDto.Id, response.Result.Id);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task Search_Success_ReturnsSuccessfulResponse()
        {
            var applicationSettingGetRequest = new ApplicationSettingCategorySearchDto() { Name = "anyname" };
            var pagedResultDto = new PagedResultDto<ApplicationSettingCategoryDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<ApplicationSettingCategoryDto>(),
            };
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("anykey");
            var query = new List<Domain.ApplicationSettingCategory>() { new Domain.ApplicationSettingCategory() { Name = "anykey" } }.AsQueryable().BuildMock();
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.ApplicationSettingCategory, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IOrderedQueryable<Domain.ApplicationSettingCategory>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IIncludableQueryable<Domain.ApplicationSettingCategory, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.ApplicationSettingCategory>, PagedResultDto<ApplicationSettingCategoryDto>>(It.IsAny<IPagedList<Domain.ApplicationSettingCategory>>())).Returns(pagedResultDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<ApplicationSettingCategoryDto>>())).Returns(new ServiceResponse<PagedResultDto<ApplicationSettingCategoryDto>>(pagedResultDto, true));
            var service = CreateApplicationSettingCategoryService();

            var response = await service.SearchAsync(applicationSettingGetRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<ApplicationSettingCategoryDto>>(response.Result.Items);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingCategoryService")]
        public async Task GetApplicationSettingList_ApplicationSettingNotNull_ReturnsSuccessfulResponse()
        {
            var pagedRequest = new PagedRequestDto()
            {
                Orders = new List<PagedRequestOrderDto>(),
                PageIndex = 0,
                PageSize = 10,
            };
            var query = new List<Domain.ApplicationSettingCategory>() { new Domain.ApplicationSettingCategory() }.AsQueryable().BuildMock();
            _ = _applicationSettingCategoryRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.ApplicationSettingCategory, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IOrderedQueryable<Domain.ApplicationSettingCategory>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSettingCategory>, IIncludableQueryable<Domain.ApplicationSettingCategory, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<ApplicationSettingCategoryDto>>())).Returns(new ServiceResponse<PagedResultDto<ApplicationSettingCategoryDto>>(new PagedResultDto<ApplicationSettingCategoryDto>(),  true));
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.ApplicationSetting>, PagedResultDto<ApplicationSettingCategoryDto>>(It.IsAny<IPagedList<Domain.ApplicationSetting>>())).Returns(new PagedResultDto<ApplicationSettingCategoryDto>());
            var service = CreateApplicationSettingCategoryService();

            var response = await service.GetApplicationSettingCategoryListAsync(pagedRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        private ApplicationSettingCategoryService CreateApplicationSettingCategoryService(List<Claim> claims = null)
        {
            var localizationMoq = new Mock<IKsStringLocalizer<ApplicationSettingCategoryService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<ApplicationSettingCategoryService>()).Returns(localizationMoq.Object);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);

            var service = new ApplicationSettingCategoryService(
                    applicationSettingCategoryRepository: _applicationSettingCategoryRepositoryMoq.Object,
                    mapper: _mapperMoq.Object,
                    dataManager: _dataManagerMoq.Object,
                    i18N: iKsI18Nmoq.Object,
                    serviceResponseHelper: _serviceResponseHelperMoq.Object,
                    keyNormalizer: _keyNormalizerMoq.Object);

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");

                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);
            }

            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:HistoryLimit"]).Returns("10");
            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:ExpireDays"]).Returns("45");

            return service;
        }
    }
}