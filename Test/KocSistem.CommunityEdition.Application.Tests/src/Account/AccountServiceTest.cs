﻿// <copyright file="AccountServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions;
using KocSistem.CommunityEdition.Application.Abstractions.Account.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplateTranslation.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Notification;
using KocSistem.CommunityEdition.Application.Abstractions.User.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.UserPasswordHistory;
using KocSistem.CommunityEdition.Application.Account;
using KocSistem.CommunityEdition.Application.Tests.Helpers;
using KocSistem.CommunityEdition.Common.Authentication;
using KocSistem.CommunityEdition.Common.Constants;
using KocSistem.CommunityEdition.Common.Helpers;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.Infrastructure.Helpers;
using KocSistem.CommunityEdition.Infrastructure.Helpers.Captcha;
using KocSistem.CommunityEdition.TestCommon.Helpers;
using KocSistem.OneFrame.Authentication.Utilities;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.Account
{
    public class AccountServiceTest
    {
        private readonly Mock<ICaptchaValidator> _captchaValidatorMoq;
        private readonly Mock<IClaimManager> _claimManagerMoq;
        private readonly string _claimValue = "anyClaim";
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IEmailNotificationService> _emailNotificationServiceMoq;
        private readonly Mock<IEmailTemplateService> _emailTemplateServiceMoq;
        private readonly Mock<IHttpContextAccessor> _httpContextAccessorMoq;
        private readonly IOptions<IdentityOptions> _identityManagerMoq;
        private readonly Mock<ILookupNormalizer> _keyNormalizerMoq;
        private readonly Mock<ILoginAuditLogService> _loginAuditLogServiceMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly string _password = "123456";
        private readonly Mock<RoleManager<ApplicationRole>> _roleManagerMoq;
        private readonly string _roleName = "testRole";
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<SignInManager<ApplicationUser>> _signInManagerMoq;
        private readonly Mock<ITokenHelper> _tokenHelperMoq;
        private readonly Mock<UserManager<ApplicationUser>> _userManagerMoq;
        private readonly string _userName = "anyName";
        private readonly Mock<IUserPasswordHistoryService> _userPasswordHistoryServiceMoq;

        public AccountServiceTest()
        {
            _userManagerMoq = IdentityMockHelpers.MockUserManager<ApplicationUser>();
            _signInManagerMoq = IdentityMockHelpers.MockSignInManager<ApplicationUser>();
            _roleManagerMoq = IdentityMockHelpers.MockRoleManager<ApplicationRole>();
            _configurationMoq = new Mock<IConfiguration>();

            _claimManagerMoq = new Mock<IClaimManager>();
            _keyNormalizerMoq = new Mock<ILookupNormalizer>();
            _mapperMoq = new Mock<IMapper>();
            _userPasswordHistoryServiceMoq = new Mock<IUserPasswordHistoryService>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();

            _tokenHelperMoq = new Mock<ITokenHelper>();

            _emailNotificationServiceMoq = new Mock<IEmailNotificationService>();
            _ = _emailNotificationServiceMoq.Setup(n => n.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.CompletedTask);

            _identityManagerMoq = Options.Create(new IdentityOptions());
            _identityManagerMoq.Value.Password = new PasswordOptions() { RequiredLength = 5 };
            _captchaValidatorMoq = new Mock<ICaptchaValidator>();
            _loginAuditLogServiceMoq = new Mock<ILoginAuditLogService>();
            _httpContextAccessorMoq = new Mock<IHttpContextAccessor>();
            _emailTemplateServiceMoq = new Mock<IEmailTemplateService>();
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task AddClaimToUser_AddClaimFailCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<List<ClaimDto>>())).Returns(new ServiceResponse<List<ClaimDto>>(new List<ClaimDto>(), true));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            _ = _userManagerMoq.Setup(r => r.AddClaimAsync(It.IsAny<ApplicationUser>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Failed());
            var service = CreateAccountService();

            var response = await service.AddClaimToUserAsync(_userName, _claimValue).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task AddClaimToUser_AddClaimSuccessCase_ReturnsSuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<List<ClaimDto>>())).Returns(new ServiceResponse<List<ClaimDto>>(new List<ClaimDto>(), true));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse());
            _ = _userManagerMoq.Setup(r => r.AddClaimAsync(It.IsAny<ApplicationUser>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Success);
            var service = CreateAccountService();

            var response = await service.AddClaimToUserAsync(_userName, _claimValue).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task AddClaimToUser_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(false) { Error = new ErrorInfo(StatusCodes.Status204NoContent) });
            var service = CreateAccountService();

            var response = await service.AddClaimToUserAsync(_userName, _claimValue).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ChangePassword_ChangePasswordFailCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userPasswordHistoryServiceMoq.Setup(r => r.PasswordHistoryValidation(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(true);
            _ = _userManagerMoq.Setup(r => r.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed(new IdentityError() { Code = "0", Description = "Desc" }));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(false) { Error = new ErrorInfo(StatusCodes.Status400BadRequest) });
            var service = CreateAccountService();

            var response = await service.ChangePasswordAsync(_userName, _password, _password).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ChangePassword_ChangePasswordSuccessCase_ReturnsSuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userPasswordHistoryServiceMoq.Setup(r => r.PasswordHistoryValidation(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(true);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            _ = _userManagerMoq.Setup(r => r.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            var service = CreateAccountService();

            var response = await service.ChangePasswordAsync(_userName, _password, _password).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ChangePassword_UpdateAsyncFailCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userPasswordHistoryServiceMoq.Setup(r => r.PasswordHistoryValidation(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(true);
            _ = _userManagerMoq.Setup(r => r.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.SetupSequence(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(false) { Error = new ErrorInfo(StatusCodes.Status400BadRequest) });
            var service = CreateAccountService();

            var response = await service.ChangePasswordAsync(_userName, _password, _password).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ChangePassword_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(false) { Error = new ErrorInfo(StatusCodes.Status204NoContent) });
            var service = CreateAccountService();

            var response = await service.ChangePasswordAsync(_userName, _password, _password).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ChangePasswordExpired_NotExpiredPasswordCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser() { LastPasswordChangedDate = DateTime.UtcNow });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(false) { Error = new ErrorInfo(StatusCodes.Status400BadRequest) });

            var service = CreateAccountService();

            var response = await service.ChangePasswordExpiredAsync(_userName, _password, _password).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ChangePasswordExpired_SuccessCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userPasswordHistoryServiceMoq.Setup(r => r.PasswordHistoryValidation(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(true);
            _ = _userManagerMoq.Setup(r => r.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            var service = CreateAccountService();

            var response = await service.ChangePasswordExpiredAsync(_userName, _password, _password).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ChangePasswordExpired_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(false) { Error = new ErrorInfo(StatusCodes.Status204NoContent) });
            var service = CreateAccountService();

            var response = await service.ChangePasswordExpiredAsync(_userName, _password, _password).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task DeleteUser_DeleteUserFailCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(false) { Error = new ErrorInfo(StatusCodes.Status400BadRequest) });
            var service = CreateAccountService();

            var response = await service.DeleteUserAsync(_userName, It.IsAny<string>()).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task DeleteUser_DeleteUserSuccessCase_ReturnsSuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            var service = CreateAccountService();

            var response = await service.DeleteUserAsync(_userName, It.IsAny<string>()).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task DeleteUser_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(false) { Error = new ErrorInfo(StatusCodes.Status204NoContent) });

            var service = CreateAccountService();

            var response = await service.DeleteUserAsync(_userName, It.IsAny<string>()).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ForgotPassword_ForgotPasswordFailCase_ReturnsSuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByEmailAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser { UserName = "anyName" });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));

            var emailTemplateDto = GetNewEmailTemplateDto();

            _ = _emailTemplateServiceMoq.Setup(r => r.GetEmailTemplateByNameAsync(EmailTemplateNameConstant.ForgotPassword)).Returns(Task.FromResult(new ServiceResponse<EmailTemplateDto>(emailTemplateDto, true)));

            var service = CreateAccountService();
            var response = await service.SendResetPasswordMailAsync(_userName, "scheme").ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ForgotPassword_ForgotPasswordSuccessCase_ReturnsNull()
        {
            _ = _userManagerMoq.Setup(r => r.FindByEmailAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>())).ReturnsAsync("anyString");

            var emailTemplateDto = GetNewEmailTemplateDto();

            _ = _emailTemplateServiceMoq.Setup(r => r.GetEmailTemplateByNameAsync(EmailTemplateNameConstant.ForgotPassword)).Returns(Task.FromResult(new ServiceResponse<EmailTemplateDto>(emailTemplateDto, true)));

            var service = CreateAccountService();

            var response = await service.SendResetPasswordMailAsync(_password, "scheme").ConfigureAwait(false);

            Assert.Null(response);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetClaimsInUser_SuccessCase_ReturnsSuccessfulResponse()
        {
            var serviceResponse = new ServiceResponse<List<ClaimDto>>(new List<ClaimDto>() { new ClaimDto() { Name = _claimValue } }, true);
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<List<ClaimDto>>())).Returns(serviceResponse);
            var service = CreateAccountService();

            var response = await service.GetClaimsInUserAsync(_userName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.NotEmpty(response.Result);
            Assert.Single(response.Result);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetClaimsInUser_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<List<ClaimDto>>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<List<ClaimDto>>((List<ClaimDto>)null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateAccountService();

            var response = await service.GetClaimsInUserAsync(_userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetRoleAssignments_GetRoleAssignmentsSuccessCase_ReturnsSuccessfulResponse()
        {
            var serviceResponse = new ServiceResponse<List<RoleAssignmentDto>>(new List<RoleAssignmentDto>() { new RoleAssignmentDto() { RoleName = _roleName, IsAssigned = true } }, true);
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string>() { _roleName });
            _ = _roleManagerMoq.Setup(s => s.Roles).Returns(new List<ApplicationRole>() { new ApplicationRole(_roleName) }.AsQueryable());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<RoleAssignmentDto>>(It.IsAny<List<RoleAssignmentDto>>())).Returns(serviceResponse);
            var service = CreateAccountService();

            var response = await service.GetRoleAssignmentsAsync(_userName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.NotEmpty(response.Result);
            Assert.Single(response.Result);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetRoleAssignments_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<List<RoleAssignmentDto>>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<List<RoleAssignmentDto>>((List<RoleAssignmentDto>)null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateAccountService();

            var response = await service.GetRoleAssignmentsAsync(_userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetUserByUserName_SuccessCase_ReturnsSuccessfulResponse()
        {
            var mapperResponse = TestConstants.UserDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _mapperMoq.Setup(r => r.Map<ApplicationUser, UserDto>(It.IsAny<ApplicationUser>())).Returns(mapperResponse);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<UserDto>(mapperResponse)).Returns(new ServiceResponse<UserDto>(mapperResponse, true));
            var service = CreateAccountService();

            var response = await service.GetUserByUsernameAsync(_userName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(mapperResponse.Email, response.Result.Email);
            Assert.Equal(mapperResponse.Name, response.Result.Name);
            Assert.Equal(mapperResponse.Surname, response.Result.Surname);
            Assert.Equal(mapperResponse.Id, response.Result.Id);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetUserByUserName_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<UserDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<UserDto>((UserDto)null, new ErrorInfo(StatusCodes.Status204NoContent), false)); ;
            var service = CreateAccountService();

            var response = await service.GetUserByUsernameAsync(_userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetUserClaimsTree_NullParameterCase_ReturnsSuccessfulResponse()
        {
            string param = null;
            var service = CreateAccountService();
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<ClaimTreeViewItemDto>>(It.IsAny<List<ClaimTreeViewItemDto>>())).Returns(new ServiceResponse<List<ClaimTreeViewItemDto>>(new List<ClaimTreeViewItemDto>(), true));

            var response = await service.GetUserClaimsTreeAsync(param).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.IsAssignableFrom<List<ClaimTreeViewItemDto>>(response.Result);
            Assert.Empty(response.Result);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetUserClaimsTree_SuccessCase_ReturnsSuccessfulResponse()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Email = "test@test.com",
                Name = "Jane",
                Surname = "Deo",
            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                new Claim(JwtRegisteredClaimNames.FamilyName, user.Surname),
                new Claim(KsPermissionPolicy.ManagementRoleCreate, "Create"),
                new Claim(KsPermissionPolicy.ManagementRoleAddClaim, "AddClaim"),
            };
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(claims);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<ClaimTreeViewItemDto>>(It.IsAny<List<ClaimTreeViewItemDto>>())).Returns(new ServiceResponse<List<ClaimTreeViewItemDto>>(new List<ClaimTreeViewItemDto>(), true));
            var service = CreateAccountService();

            var response = await service.GetUserClaimsTreeAsync(_userName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.IsAssignableFrom<List<ClaimTreeViewItemDto>>(response.Result);
            Assert.Empty(response.Result);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetUserClaimsTree_UserNotFoundCase_ReturnsSuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<ClaimTreeViewItemDto>>(It.IsAny<List<ClaimTreeViewItemDto>>())).Returns(new ServiceResponse<List<ClaimTreeViewItemDto>>(new List<ClaimTreeViewItemDto>(), true));
            var service = CreateAccountService();

            var response = await service.GetUserClaimsTreeAsync(_userName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.IsAssignableFrom<List<ClaimTreeViewItemDto>>(response.Result);
            Assert.Empty(response.Result);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetUserList_NoOrderCase_ReturnsSuccessfulResponse()
        {
            var req = new PagedRequestDto() { PageIndex = 0, PageSize = 10 };
            var mapperResponse = new PagedResultDto<UserDto>() { PageIndex = 0, PageSize = 10, Items = new List<UserDto>() };
            var users = new List<ApplicationUser>() { new ApplicationUser() { Name = _userName } }.AsQueryable().BuildMock();
            _ = _userManagerMoq.Setup(s => s.Users).Returns(users);
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<ApplicationUser>, PagedResultDto<UserDto>>(It.IsAny<IPagedList<ApplicationUser>>())).Returns(mapperResponse);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<PagedResultDto<UserDto>>(mapperResponse)).Returns(new ServiceResponse<PagedResultDto<UserDto>>(mapperResponse, true));
            var service = CreateAccountService();

            var response = await service.GetUserListAsync(req).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<UserDto>>(response.Result.Items);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task GetUserList_WithOrderCase_ReturnsSuccessfulResponse()
        {
            var req = new PagedRequestDto() { PageIndex = 0, PageSize = 10, Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { ColumnName = "Name", DirectionDesc = true } } };
            var mapperResponse = new PagedResultDto<UserDto>() { PageIndex = 0, PageSize = 10, Items = new List<UserDto>() };
            var users = new List<ApplicationUser>() { new ApplicationUser() { Name = _userName } }.AsQueryable().BuildMock();
            _ = _userManagerMoq.Setup(s => s.Users).Returns(users);
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<ApplicationUser>, PagedResultDto<UserDto>>(It.IsAny<IPagedList<ApplicationUser>>())).Returns(mapperResponse);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<PagedResultDto<UserDto>>(mapperResponse)).Returns(new ServiceResponse<PagedResultDto<UserDto>>(mapperResponse, true));
            var service = CreateAccountService();

            var response = await service.GetUserListAsync(req).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<UserDto>>(response.Result.Items);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Login_EmailNotConfirmedCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<LoginDto>(It.IsAny<LoginDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<LoginDto>((LoginDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = CreateAccountService();

            var model = new LoginRequestDto() { Email = "testMail", Password = _password };
            var response = await service.LoginAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Login_ExpiredPasswordCase_ReturnsUnsuccessfulResponse()
        {
            _ = _signInManagerMoq.Setup(r => r.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), false, true)).Returns(Task.FromResult(SignInResult.Success));
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser() { EmailConfirmed = true });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<LoginDto>(It.IsAny<LoginDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<LoginDto>((LoginDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = CreateAccountService();

            var model = new LoginRequestDto() { Email = "testMail", Password = _password };
            var response = await service.LoginAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Login_LockedOutUserCase_ReturnsUnsuccessfulResponse()
        {
            _ = _signInManagerMoq.Setup(r => r.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), false, true)).Returns(Task.FromResult(SignInResult.LockedOut));
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser() { EmailConfirmed = true, IsActive = true });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<LoginDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<LoginDto>((LoginDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = CreateAccountService();

            var model = new LoginRequestDto() { Email = "testMail", Password = _password };
            var response = await service.LoginAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Login_SuccessfulCase_ReturnsSuccessfulResponse()
        {
            var user = new ApplicationUser() { UserName = _userName, Email = "testMail", Name = _userName, Surname = _userName, LastPasswordChangedDate = DateTime.UtcNow, EmailConfirmed = true, IsActive = true };
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);

            _ = _signInManagerMoq.Setup(r => r.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(Task.FromResult(SignInResult.Success));
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(user)).ReturnsAsync(new List<string>() { _roleName });
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(_roleName)).ReturnsAsync(new ApplicationRole() { Name = _roleName });
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<Claim>>(It.IsAny<List<Claim>>())).Returns(new ServiceResponse<List<Claim>>(TestConstants.ClaimList));
            _ = _tokenHelperMoq.Setup(r => r.BuildToken(It.IsAny<List<Claim>>(), null, null)).Returns(new ServiceResponse<AccessToken>(new AccessToken() { Token = "token", RefreshToken = "refreshToken" }, true));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<LoginDto>(It.IsAny<LoginDto>())).Returns(new ServiceResponse<LoginDto>(new LoginDto() { Token = "token", RefreshToken = "refreshToken", Claims = new List<ClaimDto>() }, true));
            var service = CreateAccountService();

            var model = new LoginRequestDto() { Email = "testMail", Password = _password };

            var response = await service.LoginAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal("token", response.Result.Token);
            Assert.Equal("refreshToken", response.Result.RefreshToken);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Login_UnsuccessfulBuildTokenCase_ReturnsUnsuccessfulResponse()
        {
            var user = new ApplicationUser() { UserName = _userName, Email = "testMail", Name = _userName, Surname = _userName, EmailConfirmed = true, LastPasswordChangedDate = DateTime.UtcNow, IsActive = true };
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);

            _ = _signInManagerMoq.Setup(r => r.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(Task.FromResult(SignInResult.Success));
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(user)).ReturnsAsync(new List<string>() { _roleName });
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(_roleName)).ReturnsAsync(new ApplicationRole() { Name = _roleName });
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<List<Claim>>())).Returns(new ServiceResponse<List<Claim>>(TestConstants.ClaimList));
            _ = _tokenHelperMoq.Setup(r => r.BuildToken(It.IsAny<List<Claim>>(), null, null)).Returns(new ServiceResponse<AccessToken>((AccessToken)null, false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<LoginDto>(), It.IsAny<ErrorInfo>(), true)).Returns(new ServiceResponse<LoginDto>((LoginDto)null, false));
            var service = CreateAccountService();

            var model = new LoginRequestDto() { Email = "testMail", Password = _password };
            var response = await service.LoginAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Login_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<LoginDto>(It.IsAny<LoginDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<LoginDto>((LoginDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = CreateAccountService();

            var model = new LoginRequestDto() { Email = "testMail", Password = _password };
            var response = await service.LoginAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Login_WrongPasswordCase_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser() { LastPasswordChangedDate = DateTime.UtcNow, EmailConfirmed = true, IsActive = true });

            _ = _signInManagerMoq.Setup(r => r.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(Task.FromResult(SignInResult.Failed));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<LoginDto>(It.IsAny<LoginDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<LoginDto>((LoginDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var model = new LoginRequestDto() { Email = "testMail", Password = _password };
            var response = await service.LoginAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Post_CreateUserSuccessful_ReturnsSuccessfulResponse()
        {
            var userDto = TestConstants.UserDto;
            var emailTemplateDto = GetNewEmailTemplateDto();
            emailTemplateDto.Name = EmailTemplateNameConstant.EmailActivation;
            var emailTemplateResponse = new ServiceResponse<EmailTemplateTranslationDto>(null)
            {
                IsSuccessful = true,
                Result = new EmailTemplateTranslationDto { Subject = "Something", EmailContent = "Some Content" }
            };

            _ = _userManagerMoq.Setup(r => r.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.FindByEmailAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser() { UserName = userDto.Username, Email = userDto.Email,LastPasswordChangedDate = DateTime.UtcNow, EmailConfirmed = userDto.EmailConfirmed, IsActive = userDto.IsActive });
            _emailTemplateServiceMoq.Setup(p => p.GetEmailTemplateByNameAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<EmailTemplateDto>(emailTemplateDto, true));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationUser>())).Returns(new ServiceResponse<ApplicationUser>(new ApplicationUser()));
            _ = _mapperMoq.Setup(r => r.Map<UserDto>(It.IsAny<ApplicationUser>())).Returns(userDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<UserDto>())).Returns(new ServiceResponse<UserDto>(userDto, true));

            var service = CreateAccountService();

            var response = await service.PostAsync(userDto, "scheme", It.IsAny<string>()).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(userDto.Email, response.Result.Email);
            Assert.Equal(userDto.Name, response.Result.Name);
            Assert.Equal(userDto.Surname, response.Result.Surname);
            Assert.Equal(userDto.Username, response.Result.Username);
            Assert.Equal(userDto.Id, response.Result.Id);
            Assert.Equal(userDto.EmailConfirmed, response.Result.EmailConfirmed);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Post_CreateUserUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            var userDto = TestConstants.UserDto;
            _ = _userManagerMoq.Setup(r => r.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationUser>(null, It.IsAny<string>(), StatusCodes.Status400BadRequest, true)).Returns(new ServiceResponse<ApplicationUser>((ApplicationUser)null, false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<UserDto>(null, It.IsAny<ErrorInfo>(), true)).Returns(new ServiceResponse<UserDto>((UserDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var response = await service.PostAsync(userDto, "scheme", It.IsAny<string>()).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Put_SuccessCase_ReturnsSuccessfulResponse()
        {
            var userDto = TestConstants.UserDto;
            var userUpdateDto = TestConstants.UserUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string>() { _roleName });
            _ = _userManagerMoq.Setup(r => r.AddToRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<IEnumerable<string>>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.RemoveFromRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<IEnumerable<string>>())).ReturnsAsync(IdentityResult.Success);
            _ = _mapperMoq.Setup(r => r.Map<ApplicationUser, UserDto>(It.IsAny<ApplicationUser>())).Returns(userDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<UserDto>(It.IsAny<UserDto>())).Returns(new ServiceResponse<UserDto>(userDto, true));
            var service = CreateAccountService();

            var response = await service.PutAsync(_userName, userUpdateDto, It.IsAny<string>()).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(userDto.Email, response.Result.Email);
            Assert.Equal(userDto.Name, response.Result.Name);
            Assert.Equal(userDto.Surname, response.Result.Surname);
            Assert.Equal(userDto.Username, response.Result.Username);
            Assert.Equal(userDto.Id, response.Result.Id);
            Assert.Equal(userDto.EmailConfirmed, response.Result.EmailConfirmed);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Put_UpdateAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            var userUpdateDto = TestConstants.UserUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<UserDto>(It.IsAny<UserDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<UserDto>((UserDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var response = await service.PutAsync(_userName, userUpdateDto, It.IsAny<string>()).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Put_UserNotFoundCase_ReturnsUnsuccessfulResponse()
        {
            var userUpdateDto = TestConstants.UserUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<UserDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<UserDto>((UserDto)null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateAccountService();

            var response = await service.PutAsync(_userName, userUpdateDto, It.IsAny<string>()).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task PutForUserRole_AddToRolesUnsuccessfulwithAssignedRoles_ReturnsUnsuccessfulResponse()
        {
            var userUpdateDto = TestConstants.UserRoleUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string>() { "assignedRole1", "assignedRole2" });
            _ = _userManagerMoq.Setup(r => r.AddToRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<IEnumerable<string>>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<UserDto>((UserDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var response = await service.PutForUserRoleAsync(_userName, userUpdateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task PutForUserRole_AddToRolesUnsuccessfulwithNoAssignedRoles_ReturnsUnsuccessfulResponse()
        {
            var userUpdateDto = TestConstants.UserRoleUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string>());
            _ = _userManagerMoq.Setup(r => r.AddToRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<IEnumerable<string>>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<UserDto>((UserDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var response = await service.PutForUserRoleAsync(_userName, userUpdateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task PutForUserRole_RemoveFromRolesAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            var userUpdateDto = TestConstants.UserRoleUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string>() { "testRole2" });
            _ = _userManagerMoq.Setup(r => r.AddToRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<IEnumerable<string>>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.RemoveFromRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<List<string>>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<UserDto>((UserDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var response = await service.PutForUserRoleAsync(_userName, userUpdateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task PutForUserRole_SuccessCase_ReturnsSuccessfulResponse()
        {
            var userUpdateDto = TestConstants.UserRoleUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string>() { "testRole2" });
            _ = _userManagerMoq.Setup(r => r.AddToRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<IEnumerable<string>>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.RemoveFromRolesAsync(It.IsAny<ApplicationUser>(), It.IsAny<List<string>>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse());
            var service = CreateAccountService();

            var response = await service.PutForUserRoleAsync(_userName, userUpdateDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task PutForUserProfile_SuccessCase_ReturnsSuccessfulResponse()
        {
            var userDto = TestConstants.UserDto;
            var userUpdateDto = TestConstants.UserUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _mapperMoq.Setup(r => r.Map<ApplicationUser, UserDto>(It.IsAny<ApplicationUser>())).Returns(userDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<UserDto>(It.IsAny<UserDto>())).Returns(new ServiceResponse<UserDto>(userDto, true));
            var service = CreateAccountService();

            var response = await service.PutForUserProfileAsync(_userName, userUpdateDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(userDto.Email, response.Result.Email);
            Assert.Equal(userDto.Name, response.Result.Name);
            Assert.Equal(userDto.Surname, response.Result.Surname);
            Assert.Equal(userDto.Username, response.Result.Username);
            Assert.Equal(userDto.Id, response.Result.Id);
            Assert.Equal(userDto.EmailConfirmed, response.Result.EmailConfirmed);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task PutForUserProfile_UpdateAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            var userUpdateDto = TestConstants.UserUpdateDto;
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<UserDto>(It.IsAny<UserDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<UserDto>((UserDto)null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var response = await service.PutForUserProfileAsync(_userName, userUpdateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public void Refresh_ValidateRefreshTokenSuccessful_ReturnsSuccessfulResponse()
        {
            _ = _tokenHelperMoq.Setup(r => r.ValidateRefreshToken(It.IsAny<string>())).Returns(new ServiceResponse<AccessToken>(new AccessToken() { Token = "anyToken", RefreshToken = "anyToken" }));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<LoginDto>(It.IsAny<LoginDto>())).Returns(new ServiceResponse<LoginDto>(new LoginDto() { Token = "anyToken", RefreshToken = "anyToken" }, true));
            var service = CreateAccountService();

            var response = service.Refresh("anyToken");

            Assert.True(response.IsSuccessful);
            Assert.Equal("anyToken", response.Result.Token);
            Assert.Equal("anyToken", response.Result.RefreshToken);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public void Refresh_ValidateRefreshTokenUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            _ = _tokenHelperMoq.Setup(r => r.ValidateRefreshToken(It.IsAny<string>())).Returns(new ServiceResponse<AccessToken>((AccessToken)null, false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<LoginDto>(It.IsAny<LoginDto>(), It.IsAny<ErrorInfo>(), It.IsAny<bool>())).Returns(new ServiceResponse<LoginDto>((LoginDto)null, false));
            var service = CreateAccountService();

            var response = service.Refresh("anyToken");

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Register_BuildTokenSuccessful_ReturnsSuccessfulResponse()
        {
            var serviceResponse = new RegisterDto()
            {
                Claims = new List<ClaimDto>(),
                PasswordExpired = true,
                Token = "token",
                RefreshToken = "token",
                EmailConfirmed = true,
                UserId = "id",
                Username = _userName,
            };
            var userDto = TestConstants.UserDto;
            _ = _userManagerMoq.Setup(r => r.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<ApplicationUser>(It.IsAny<ApplicationUser>())).Returns(new ServiceResponse<ApplicationUser>(TestConstants.AppliationUser));
            _ = _signInManagerMoq.Setup(r => r.SignInAsync(It.IsAny<ApplicationUser>(), true, null)).Returns(Task.CompletedTask);
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string> { _roleName });
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole() { Name = _roleName });
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _claimManagerMoq.Setup(r => r.SetClaims(It.IsAny<List<Claim>>(), It.IsAny<List<Claim>>(), It.IsAny<string>())).Verifiable();
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<Claim>>(It.IsAny<List<Claim>>())).Returns(new ServiceResponse<List<Claim>>(TestConstants.ClaimList));
            _ = _tokenHelperMoq.Setup(r => r.BuildToken(It.IsAny<List<Claim>>(), null, null)).Returns(new ServiceResponse<AccessToken>(new AccessToken() { Token = "anyToken", RefreshToken = "anyToken" }));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<RegisterDto>(It.IsAny<RegisterDto>())).Returns(new ServiceResponse<RegisterDto>(serviceResponse, true));
            var service = CreateAccountService();

            var response = await service.RegisterAsync(userDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(serviceResponse.Token, response.Result.Token);
            Assert.Equal(serviceResponse.Token, response.Result.RefreshToken);
            Assert.Equal(serviceResponse.UserId, response.Result.UserId);
            Assert.Equal(serviceResponse.Username, response.Result.Username);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Register_BuildTokenUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            var userDto = TestConstants.UserDto;
            _ = _userManagerMoq.Setup(r => r.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationUser>())).Returns(new ServiceResponse<ApplicationUser>(TestConstants.AppliationUser));
            _ = _signInManagerMoq.Setup(r => r.SignInAsync(It.IsAny<ApplicationUser>(), true, null)).Returns(Task.CompletedTask);
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string> { _roleName });
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole() { Name = _roleName });
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _claimManagerMoq.Setup(r => r.SetClaims(It.IsAny<List<Claim>>(), It.IsAny<List<Claim>>(), It.IsAny<string>())).Verifiable();
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<List<Claim>>())).Returns(new ServiceResponse<List<Claim>>(TestConstants.ClaimList));
            _ = _tokenHelperMoq.Setup(r => r.BuildToken(It.IsAny<List<Claim>>(), null, null)).Returns(new ServiceResponse<AccessToken>((AccessToken)null, false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<RegisterDto>(), It.IsAny<ErrorInfo>(), true)).Returns(new ServiceResponse<RegisterDto>((RegisterDto)null, false));

            var service = CreateAccountService();

            var response = await service.RegisterAsync(userDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Register_CreateUserUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            var userDto = TestConstants.UserDto;
            _ = _userManagerMoq.Setup(r => r.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationUser>(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationUser>((ApplicationUser)null, false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<RegisterDto>(It.IsAny<RegisterDto>(), It.IsAny<ErrorInfo>(), true)).Returns(new ServiceResponse<RegisterDto>((RegisterDto)null, false));
            var service = CreateAccountService();

            var response = await service.RegisterAsync(userDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task RemoveClaimFromUser_ClaimNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync((IList<Claim>)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<bool>(It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<bool>(false, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateAccountService();

            var response = await service.RemoveClaimFromUserAsync(_userName, _claimValue).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task RemoveClaimFromUser_DeletedClaimNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<bool>(It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<bool>(false, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateAccountService();

            var response = await service.RemoveClaimFromUserAsync(_userName, _claimValue).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task RemoveClaimFromUser_RemoveClaimAsyncSuccessful_ReturnsSuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _userManagerMoq.Setup(r => r.RemoveClaimAsync(It.IsAny<ApplicationUser>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<bool>(true)).Returns(new ServiceResponse<bool>(true, new ErrorInfo(StatusCodes.Status200OK), true));
            var service = CreateAccountService();

            var response = await service.RemoveClaimFromUserAsync(_userName, "claimValue").ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task RemoveClaimFromUser_RemoveClaimAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _userManagerMoq.Setup(r => r.RemoveClaimAsync(It.IsAny<ApplicationUser>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<bool>(It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<bool>(false, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var response = await service.RemoveClaimFromUserAsync(_userName, "claimValue").ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task RemoveClaimFromUser_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<bool>(It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<bool>(false, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateAccountService();

            var response = await service.RemoveClaimFromUserAsync(_userName, _claimValue).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ResetPassword_ResetPasswordAsyncSuccessful_ReturnsSuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByEmailAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.IsLockedOutAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(true);
            _ = _userManagerMoq.Setup(r => r.SetLockoutEndDateAsync(It.IsAny<ApplicationUser>(), It.IsAny<System.DateTimeOffset>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<string>(It.IsAny<string>())).Returns(new ServiceResponse<string>(string.Empty, true));
            var service = CreateAccountService();

            var response = await service.ResetPasswordAsync(TestConstants.ResetPasswordDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ResetPassword_ResetPasswordAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByEmailAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<string>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<string>(string.Empty, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateAccountService();

            var response = await service.ResetPasswordAsync(TestConstants.ResetPasswordDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ResetPassword_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(r => r.FindByEmailAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<string>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<string>(string.Empty, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateAccountService();

            var response = await service.ResetPasswordAsync(TestConstants.ResetPasswordDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task SaveUserClaims_Success_ReturnsSuccessfulResponse()
        {
            var claimList = new List<Claim>() { new Claim(ApplicationPolicyType.KsPermission, KsPermissionPolicy.ManagementRoleAddClaim), new Claim(ApplicationPolicyType.KsPermission, KsPermissionPolicy.ManagementRoleAddUser) };
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(claimList);
            _ = _userManagerMoq.Setup(r => r.RemoveClaimsAsync(It.IsAny<ApplicationUser>(), It.IsAny<IEnumerable<Claim>>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.AddClaimAsync(It.IsAny<ApplicationUser>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            var service = CreateAccountService();

            var response = await service.SaveUserClaimsAsync(TestConstants.SaveUserClaimsDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task SaveUserClaims_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            var model = new SaveUserClaimsDto() { Name = _userName };
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateAccountService();

            var response = await service.SaveUserClaimsAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task Search_Success_ReturnsSuccessfulResponse()
        {
            var model = new UserSearchDto() { Username = _userName };
            var pagedResultDto = new PagedResultDto<UserDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<UserDto>(),
            };
            var users = new List<ApplicationUser>() { new ApplicationUser() { NormalizedUserName = "ANYNAME" } }.AsQueryable().BuildMock();
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("anyString");
            _ = _userManagerMoq.Setup(r => r.Users).Returns(users);
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<ApplicationUser>, PagedResultDto<UserDto>>(It.IsAny<IPagedList<ApplicationUser>>())).Returns(pagedResultDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<PagedResultDto<UserDto>>(It.IsAny<PagedResultDto<UserDto>>())).Returns(new ServiceResponse<PagedResultDto<UserDto>>(pagedResultDto, true));
            var service = CreateAccountService();

            var response = await service.SearchAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<UserDto>>(response.Result.Items);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task CreateEmailActivationUrl_Success_ReturnsSuccessfulResponse()
        {
            var user = new ApplicationUser { Email = "test@test.com", IsActive = true, Name = "John", Surname = "Doe" };
            var urlResponse = new ServiceResponse<string>(null)
            {
                IsSuccessful = true,
                Result = "http://asd.com.tr"
            };

            _ = _userManagerMoq
                .Setup(r => r.FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(user);

            _ = _userManagerMoq
                .Setup(r => r.GenerateEmailConfirmationTokenAsync(It.IsAny<ApplicationUser>()))
                .ReturnsAsync("token");

            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetSuccess(It.IsAny<string>()))
                .Returns(urlResponse);

            var service = CreateAccountService();

            var response = await service.CreateEmailActivationUrlAsync("test@test.com").ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task CreateEmailActivationUrl_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            ApplicationUser user = null;

            _ = _userManagerMoq
                .Setup(r => r.FindByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(user);

            _ = _userManagerMoq
                .Setup(r => r.GenerateEmailConfirmationTokenAsync(It.IsAny<ApplicationUser>()))
                .ReturnsAsync("token");

            var errResponse = new ServiceResponse<string>(null)
            {
                IsSuccessful = false,
                Error = new ErrorInfo
                {
                    Code = StatusCodes.Status400BadRequest
                }
            };

            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError<string>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(errResponse);

            var service = CreateAccountService();

            var response = await service.CreateEmailActivationUrlAsync("test@test.com").ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(400, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public void GetAuthenticationProperties_Success_ReturnsSuccessfulResponse()
        {
            _ = _signInManagerMoq.Setup(r => r.ConfigureExternalAuthenticationProperties(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new AuthenticationProperties());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<AuthenticationProperties>(It.IsAny<AuthenticationProperties>())).Returns(new ServiceResponse<AuthenticationProperties>(new AuthenticationProperties(), true));

            var service = CreateAccountService();

            var response = service.GetAuthenticationProperties(It.IsAny<string>(), It.IsAny<string>());

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AccountService")]
        public async Task ExternalLogin_Success_ReturnsSuccessfulResponse()
        {
            List<Claim> claimList = new() { new Claim("emailaddress", "adminuser@kocsistem.com.tr") };

            var externalLoginInfo = new ExternalLoginInfo(new ClaimsPrincipal(new ClaimsIdentity(claimList)), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());
            var user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                Email = "adminuser@kocsistem.com.tr",
                UserName = "adminuser@kocsistem.com.tr",
                Name = "John",
                Surname = "Doe"
            };

            _ = _signInManagerMoq.Setup(r => r.GetExternalLoginInfoAsync(It.IsAny<string>())).ReturnsAsync(externalLoginInfo);
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            _ = _userManagerMoq.Setup(r => r.SetAuthenticationTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(new List<string>() { _roleName });
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(_roleName)).ReturnsAsync(new ApplicationRole() { Name = _roleName });
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _userManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<Claim>>(It.IsAny<List<Claim>>())).Returns(new ServiceResponse<List<Claim>>(TestConstants.ClaimList));
            _ = _tokenHelperMoq.Setup(r => r.BuildToken(It.IsAny<List<Claim>>(), null, null)).Returns(new ServiceResponse<AccessToken>(new AccessToken() { Token = "token", RefreshToken = "refreshToken" }, true));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<LoginDto>(It.IsAny<LoginDto>())).Returns(new ServiceResponse<LoginDto>(new LoginDto() { Token = "token", RefreshToken = "refreshToken", Claims = new List<ClaimDto>() }, true));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<LoginDto>(It.IsAny<LoginDto>())).Returns(new ServiceResponse<LoginDto>(new LoginDto(), true));

            var service = CreateAccountService();

            var response = await service.ExternalLoginAsync().ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        private static EmailTemplateDto GetNewEmailTemplateDto()
        {
            var emailTemplateDto = new EmailTemplateDto
            {
                Id = Guid.NewGuid(),
                Name = EmailTemplateNameConstant.ForgotPassword,
                To = "test@test.com",
                Bcc = "test@test.com",
                Cc = "test@test.com",
                Translations = new List<EmailTemplateTranslationDto>() {
                   new EmailTemplateTranslationDto
                   {
                    Id = Guid.NewGuid(),
                    EmailContent = "content",
                    Language = Common.Enums.LanguageType.en,
                    Subject = "subject"
                   },
                   new EmailTemplateTranslationDto
                   {
                    Id = Guid.NewGuid(),
                    EmailContent = "content",
                    Language = Common.Enums.LanguageType.tr,
                    Subject = "subject"
                   },
                   new EmailTemplateTranslationDto
                   {
                    Id = Guid.NewGuid(),
                    EmailContent = "content",
                    Language = Common.Enums.LanguageType.ar,
                    Subject = "subject"
                   }
                }
            };
            return emailTemplateDto;
        }

        private AccountService CreateAccountService(List<Claim> claims = null)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("tr-TR");
            var localizationMoq = new Mock<IKsStringLocalizer<AccountService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<AccountService>()).Returns(localizationMoq.Object);
            _ = iKsI18Nmoq.Setup(r => r.UICulture).Returns(CultureInfo.CurrentUICulture);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);

            var service = new AccountService(
                    serviceProvider: serviceProviderMock.Object,
                    signInManager: _signInManagerMoq.Object,
                    configuration: _configurationMoq.Object,
                    userManager: _userManagerMoq.Object,
                    roleManager: _roleManagerMoq.Object,
                    tokenHelper: _tokenHelperMoq.Object,
                    claimManager: _claimManagerMoq.Object,
                    emailNotificationService: _emailNotificationServiceMoq.Object,
                    keyNormalizer: _keyNormalizerMoq.Object,
                    userPasswordHistoryService: _userPasswordHistoryServiceMoq.Object,
                    identityOptions: _identityManagerMoq,
                    captchaValidator: _captchaValidatorMoq.Object,
                    loginAuditLogService: _loginAuditLogServiceMoq.Object,
                    httpContextAccessor: _httpContextAccessorMoq.Object,
                    emailTemplateService: _emailTemplateServiceMoq.Object,
                    i18N: iKsI18Nmoq.Object);

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");

                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);
            }

            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:HistoryLimit"]).Returns("10");
            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:ExpireDays"]).Returns("45");
            _ = _configurationMoq.SetupGet(m => m["EmailConfirmationSettings:Schema"]).Returns("schema");
            _ = _configurationMoq.SetupGet(m => m["EmailConfirmationSettings:Host"]).Returns("host");
            _ = _configurationMoq.SetupGet(m => m["EmailConfirmationSettings:Controller"]).Returns("controller");
            _ = _configurationMoq.SetupGet(m => m["EmailConfirmationSettings:Action"]).Returns("action");

            return service;
        }
    }
}