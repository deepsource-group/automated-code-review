﻿// <copyright file="AuthenticationServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Application.Abstractions.Account.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplateTranslation.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Notification;
using KocSistem.CommunityEdition.Application.Account;
using KocSistem.CommunityEdition.Common.Constants;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.TestCommon.Helpers;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using KocSistem.OneFrame.Notification.Sms.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.Account
{
    public class AuthenticationServiceTest
    {
        private readonly Mock<IEmailNotificationService> _emailNotificationServiceMoq;
        private readonly Mock<ISmsNotificationService> _smsNotificationServiceMoq;
        private readonly Mock<UserManager<ApplicationUser>> _userManagerMoq;
        private readonly Mock<IRepository<IdentityUserToken>> _identityUserTokenMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly TwoFactorVerificationDto _twoFactorVerification;
        private readonly Mock<IEmailTemplateService> _emailTemplateServiceMoq;
        private readonly Mock<IApplicationSettingService> _applicationSettingServiceMoq;


        public AuthenticationServiceTest()
        {
            var user = new ApplicationUser()
            {
                UserName = "anyName",
                Email = "test@test.com",
                PhoneNumber = "12345678901",
                Name = "Jane",
                Surname = "Deo",
                Id = Guid.NewGuid()
            };

            _twoFactorVerification = new TwoFactorVerificationDto() { Username = user.UserName, VerificationCode = "someText" };

            _emailNotificationServiceMoq = new Mock<IEmailNotificationService>();
            _smsNotificationServiceMoq = new Mock<ISmsNotificationService>();
            _userManagerMoq = IdentityMockHelpers.MockUserManager<ApplicationUser>();
            _ = _userManagerMoq.Setup(n => n.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            _ = _userManagerMoq.Setup(n => n.GenerateTwoFactorTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(It.IsAny<string>());
            _identityUserTokenMoq = new Mock<IRepository<IdentityUserToken>>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _configurationMoq = new Mock<IConfiguration>();
            _emailTemplateServiceMoq = new Mock<IEmailTemplateService>();
            _applicationSettingServiceMoq= new Mock<IApplicationSettingService>();
        }

        public AuthenticationService CreateAuthenticationService()
        {
            var localizationMoq = new Mock<IKsStringLocalizer<AuthenticationService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<AuthenticationService>()).Returns(localizationMoq.Object);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);
            return new AuthenticationService(serviceProviderMock.Object, _userManagerMoq.Object, _emailNotificationServiceMoq.Object, _identityUserTokenMoq.Object, _smsNotificationServiceMoq.Object, _configurationMoq.Object, _emailTemplateServiceMoq.Object, _applicationSettingServiceMoq.Object);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task SendVerificationCode_SendSMS_ReturnsSuccessfulResponse()
        {
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Sms;
            _ = _smsNotificationServiceMoq.Setup(n => n.SendSmsAsync(It.IsAny<SendSmsRequest>())).ReturnsAsync(new SendSmsResponse(It.IsAny<Guid>(), true));
            _ = _applicationSettingServiceMoq.Setup(s => s.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto() { Value = 30, Key = ConfigurationConstant.Identity2FaSettingsVerificationTime }));
            _ = _userManagerMoq.Setup(n => n.SetAuthenticationTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _identityUserTokenMoq.Setup(n => n.GetFirstOrDefault(
                It.IsAny<Expression<Func<IdentityUserToken, bool>>>(), 
                It.IsAny<Func<IQueryable<IdentityUserToken>, IOrderedQueryable<IdentityUserToken>>>(),
                It.IsAny<Func<IQueryable<IdentityUserToken>, IIncludableQueryable<IdentityUserToken, object>>>(),
                It.IsAny<bool>())).Returns(new IdentityUserToken() { SentDate = DateTime.UtcNow.AddDays(-1)});
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<string>())).Returns(new ServiceResponse<string>(It.IsAny<string>(), true));
            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.SendVerificationCodeAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task SendVerificationCode_SendSMS_ReturnsUnsuccessfulResponse()
        {
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Sms;

            _ = _smsNotificationServiceMoq.Setup(n => n.SendSmsAsync(It.IsAny<SendSmsRequest>())).ReturnsAsync(new SendSmsResponse(It.IsAny<Guid>(), false));
            _ = _userManagerMoq.Setup(n => n.SetAuthenticationTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _applicationSettingServiceMoq.Setup(s => s.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto() { Value = 30, Key = ConfigurationConstant.Identity2FaSettingsVerificationTime }));
            _ = _identityUserTokenMoq.Setup(n => n.GetFirstOrDefault(
                It.IsAny<Expression<Func<IdentityUserToken, bool>>>(),
                It.IsAny<Func<IQueryable<IdentityUserToken>, IOrderedQueryable<IdentityUserToken>>>(),
                It.IsAny<Func<IQueryable<IdentityUserToken>, IIncludableQueryable<IdentityUserToken, object>>>(),
                It.IsAny<bool>())).Returns(new IdentityUserToken() { SentDate = DateTime.UtcNow.AddDays(-1) });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<string>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<string>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.SendVerificationCodeAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task SendVerificationCode_SendEmail_ReturnsSuccessfulResponse()
        {
            var emailTemplateDto = GetNewEmailTemplateDto();
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Email;
            _ = _emailNotificationServiceMoq.Setup(n => n.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.CompletedTask);
            _ = _userManagerMoq.Setup(n => n.SetAuthenticationTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _applicationSettingServiceMoq.Setup(s => s.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto() { Value = 30, Key = ConfigurationConstant.Identity2FaSettingsVerificationTime }));
            _ = _identityUserTokenMoq.Setup(n => n.GetFirstOrDefault(
                It.IsAny<Expression<Func<IdentityUserToken, bool>>>(),
                It.IsAny<Func<IQueryable<IdentityUserToken>, IOrderedQueryable<IdentityUserToken>>>(),
                It.IsAny<Func<IQueryable<IdentityUserToken>, IIncludableQueryable<IdentityUserToken, object>>>(),
                It.IsAny<bool>())).Returns(new IdentityUserToken() { SentDate = DateTime.UtcNow.AddDays(-1) });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<string>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<string>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<string>())).Returns(new ServiceResponse<string>(null, true));
            _ = _emailTemplateServiceMoq.Setup(r => r.GetEmailTemplateByNameAsync(EmailTemplateNameConstant.TwoFAVerificationCode)).Returns(Task.FromResult(new ServiceResponse<EmailTemplateDto>(emailTemplateDto, true)));

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.SendVerificationCodeAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task SendVerificationCode_SendEmail_ReturnsUnsuccessfulResponse()
        {
            var emailTemplateDto = GetNewEmailTemplateDto();
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Email;
            _ = _emailNotificationServiceMoq.Setup(n => n.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.CompletedTask);
            _ = _userManagerMoq.Setup(n => n.SetAuthenticationTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _applicationSettingServiceMoq.Setup(s => s.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto() { Value = 30, Key = ConfigurationConstant.Identity2FaSettingsVerificationTime }));
            _ = _identityUserTokenMoq.Setup(n => n.GetFirstOrDefault(
                It.IsAny<Expression<Func<IdentityUserToken, bool>>>(),
                It.IsAny<Func<IQueryable<IdentityUserToken>, IOrderedQueryable<IdentityUserToken>>>(),
                It.IsAny<Func<IQueryable<IdentityUserToken>, IIncludableQueryable<IdentityUserToken, object>>>(),
                It.IsAny<bool>())).Returns(new IdentityUserToken() { SentDate = DateTime.UtcNow.AddDays(-1) });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<string>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<string>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<string>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<string>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            _ = _emailTemplateServiceMoq.Setup(r => r.GetEmailTemplateByNameAsync(EmailTemplateNameConstant.TwoFAVerificationCode)).Returns(Task.FromResult(new ServiceResponse<EmailTemplateDto>(emailTemplateDto, true)));

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.SendVerificationCodeAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task SendVerificationCode_VerificationTimeExpired_ReturnsSuccessfulResponse()
        {
            _ = _applicationSettingServiceMoq.Setup(s => s.GetByKeyAsync(It.IsAny<string>())).ReturnsAsync(new ServiceResponse<ApplicationSettingDto>(new ApplicationSettingDto() { Value = 30, Key = ConfigurationConstant.Identity2FaSettingsVerificationTime }));
            _ = _identityUserTokenMoq.Setup(n => n.GetFirstOrDefaultAsync(
               It.IsAny<Expression<Func<IdentityUserToken, bool>>>(),
               It.IsAny<Func<IQueryable<IdentityUserToken>, IOrderedQueryable<IdentityUserToken>>>(),
               It.IsAny<Func<IQueryable<IdentityUserToken>, IIncludableQueryable<IdentityUserToken, object>>>(),
               It.IsAny<bool>())).ReturnsAsync(new IdentityUserToken() { SentDate = DateTime.UtcNow.AddDays(1) });

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.SendVerificationCodeAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.Null(response);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task GenerateAuthenticatorSharedKey_ReturnsSuccessfulResponse()
        {
            _ = _userManagerMoq.Setup(n => n.GetAuthenticatorKeyAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(It.IsAny<string>());
            _ = _userManagerMoq.Setup(n => n.ResetAuthenticatorKeyAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<AuthenticatorResponseDto>())).Returns(new ServiceResponse<AuthenticatorResponseDto>(new AuthenticatorResponseDto(), true));

            _identityUserTokenMoq.SetReturnsDefault(new IdentityUserToken());

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.GenerateAuthenticatorSharedKeyAsync(It.IsAny<string>()).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.False(response.Result.IsActivated);
            Assert.False(response.Result.HasAuthenticatorKey);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task TwoFactorVerification_SendSMS_ReturnsSuccessfulResponse()
        {
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Sms;

            var userToken = new IdentityUserToken() { Value = "someText" };
            _identityUserTokenMoq.SetReturnsDefault(userToken);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.TwoFactorVerificationAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task TwoFactorVerification_SendSMS_ReturnsUnsuccessfulResponse()
        {
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Sms;

            var userToken = new IdentityUserToken() { Value = "invalidValue" };

            _identityUserTokenMoq.SetReturnsDefault(userToken);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.TwoFactorVerificationAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task TwoFactorVerification_SendEmail_ReturnsSuccessfulResponse()
        {
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Email;

            var userToken = new IdentityUserToken() { Value = "someText" };

            _identityUserTokenMoq.SetReturnsDefault(userToken);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.TwoFactorVerificationAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task TwoFactorVerification_SendEmail_ReturnsUnsuccessfulResponse()
        {
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Email;

            var userToken = new IdentityUserToken() { Value = "invalidValue" };

            _identityUserTokenMoq.SetReturnsDefault(userToken);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.TwoFactorVerificationAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task TwoFactorVerification_SendAuthenticator_ReturnsSuccessfulResponse()
        {
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Authenticator;

            var userToken = new IdentityUserToken() { Value = "someText" };
            _identityUserTokenMoq.SetReturnsDefault(userToken);
            _ = _userManagerMoq.Setup(n => n.VerifyTwoFactorTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(true);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.TwoFactorVerificationAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "AuthenticationService")]
        public async Task TwoFactorVerification_SendAuthenticator_ReturnsUnsuccessfulResponse()
        {
            _twoFactorVerification.VerificationType = Common.Enums.Login2Fa.Authenticator;

            var userToken = new IdentityUserToken() { Value = "invalidValue" };
            _identityUserTokenMoq.SetReturnsDefault(userToken);
            _ = _userManagerMoq.Setup(n => n.VerifyTwoFactorTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(false);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var authenticationService = CreateAuthenticationService();
            var response = await authenticationService.TwoFactorVerificationAsync(_twoFactorVerification).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        private static EmailTemplateDto GetNewEmailTemplateDto()
        {
            var emailTemplateDto = new EmailTemplateDto
            {
                Id = Guid.NewGuid(),
                Name = EmailTemplateNameConstant.TwoFAVerificationCode,
                To = "test@test.com",
                Bcc = "test@test.com",
                Cc = "test@test.com",
                Translations = new List<EmailTemplateTranslationDto>() {
                   new EmailTemplateTranslationDto
                   {
                    Id = Guid.NewGuid(),
                    EmailContent = "content",
                    Language = Common.Enums.LanguageType.en,
                    Subject = "subject"
                   }
                }
            };
            return emailTemplateDto;
        }
    }
}