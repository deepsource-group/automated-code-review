﻿// <copyright file="RoleServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Account.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Role.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.RoleTranslation.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.User.Contracts;
using KocSistem.CommunityEdition.Application.Role;
using KocSistem.CommunityEdition.Application.Tests.Helpers;
using KocSistem.CommunityEdition.Common.Authentication;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.TestCommon.Helpers;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.Role
{
    public class RoleServiceTest
    {
        private readonly string _claimValue = "claimValue";
        private readonly Mock<ILookupNormalizer> _keyNormalizerMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<RoleManager<ApplicationRole>> _roleManagerMoq;
        private readonly string _roleName = "testRole";
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<UserManager<ApplicationUser>> _userManagerMoq;
        private readonly string _userName = "testUser";

        public RoleServiceTest()
        {
            _userManagerMoq = IdentityMockHelpers.MockUserManager<ApplicationUser>();
            _roleManagerMoq = IdentityMockHelpers.MockRoleManager<ApplicationRole>();

            _keyNormalizerMoq = new Mock<ILookupNormalizer>();
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task AddClaimToRole_AddClaimAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.AddClaimAsync(It.IsAny<ApplicationRole>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Failed());
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(new List<Claim>());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<List<ClaimDto>>())).Returns(new ServiceResponse<List<ClaimDto>>(new List<ClaimDto>(), true));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.AddClaimToRoleAsync(TestConstants.RoleClaimDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task AddClaimToRole_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationRole)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.AddClaimToRoleAsync(TestConstants.RoleClaimDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task AddClaimToRole_SuccessCase_ReturnsSuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.AddClaimAsync(It.IsAny<ApplicationRole>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<List<ClaimDto>>())).Returns(new ServiceResponse<List<ClaimDto>>(new List<ClaimDto>(), true));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            var service = CreateRoleService();

            var response = await service.AddClaimToRoleAsync(TestConstants.RoleClaimDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task AddUserToRole_AddToRoleAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.AddToRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.AddUserToRoleAsync(_roleName, _userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task AddUserToRole_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationRole)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.AddUserToRoleAsync(_roleName, _userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task AddUserToRole_SuccessCase_ReturnsSuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.AddToRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            var service = CreateRoleService();

            var response = await service.AddUserToRoleAsync(_roleName, _userName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task AddUserToRole_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.AddUserToRoleAsync(_roleName, _userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Delete_DeleteAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.DeleteAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.DeleteAsync(_roleName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Delete_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationRole)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.DeleteAsync(_roleName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Delete_SuccessCase_ReturnsSuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.DeleteAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse());
            var service = CreateRoleService();

            var response = await service.DeleteAsync(_roleName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public void GetApplicationRoles_Success_ReturnsSuccessfulResponse()
        {
            var roles = new List<ApplicationRole>() { new ApplicationRole() }.AsQueryable().BuildMock();
            var roleDtoList = new List<ApplicationRoleDto>() { TestConstants.ApplicationRoleDto };
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _mapperMoq.Setup(r => r.Map<List<ApplicationRoleDto>>(It.IsAny<List<ApplicationRole>>())).Returns(roleDtoList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<ApplicationRoleDto>>(It.IsAny<List<ApplicationRoleDto>>())).Returns(new ServiceResponse<List<ApplicationRoleDto>>(roleDtoList));
            var service = CreateRoleService();

            var response = service.GetApplicationRoles();

            Assert.True(response.IsSuccessful);
            Assert.Single(response.Result);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetApplicationRoles_WithPagedRequestEmptyRolesListCaseUnsuccess_ReturnsUnsuccessfulResponse()
        {
            var roles = new List<ApplicationRole>().AsQueryable().BuildMock();
            var model = new PagedRequestDto() { PageIndex = 0, PageSize = 10 };
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<ApplicationRole>, PagedResultDto<ApplicationRoleDto>>(It.IsAny<IPagedList<ApplicationRole>>())).Returns(new PagedResultDto<ApplicationRoleDto>());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<PagedResultDto<ApplicationRoleDto>>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<PagedResultDto<ApplicationRoleDto>>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.GetApplicationRolesAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetApplicationRoles_WithPagedRequestWithOrdersDirectionDescFalseCaseSuccess_ReturnsSuccessfulResponse()
        {
            var roles = new List<ApplicationRole>() { new ApplicationRole() }.AsQueryable().BuildMock();
            var model = new PagedRequestDto() { PageIndex = 0, PageSize = 10, Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { DirectionDesc = false, ColumnName = "Name" } } };
            var pagedResultDto = new PagedResultDto<ApplicationRoleDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<ApplicationRoleDto>(),
            };
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<ApplicationRole>, PagedResultDto<ApplicationRoleDto>>(It.IsAny<IPagedList<ApplicationRole>>())).Returns(pagedResultDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<PagedResultDto<ApplicationRoleDto>>(It.IsAny<PagedResultDto<ApplicationRoleDto>>())).Returns(new ServiceResponse<PagedResultDto<ApplicationRoleDto>>(pagedResultDto, true));
            var service = CreateRoleService();

            var response = await service.GetApplicationRolesAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<ApplicationRoleDto>>(response.Result.Items);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetApplicationRoles_WithPagedRequestWithOrdersDirectionDescTrueCaseSuccess_ReturnsSuccessfulResponse()
        {
            var roles = new List<ApplicationRole>() { new ApplicationRole() }.AsQueryable().BuildMock();
            var model = new PagedRequestDto() { PageIndex = 0, PageSize = 10, Orders = new List<PagedRequestOrderDto>() { new PagedRequestOrderDto() { DirectionDesc = true, ColumnName = "Name" } } };
            var pagedResultDto = new PagedResultDto<ApplicationRoleDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<ApplicationRoleDto>(),
            };
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<ApplicationRole>, PagedResultDto<ApplicationRoleDto>>(It.IsAny<IPagedList<ApplicationRole>>())).Returns(pagedResultDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<PagedResultDto<ApplicationRoleDto>>(It.IsAny<PagedResultDto<ApplicationRoleDto>>())).Returns(new ServiceResponse<PagedResultDto<ApplicationRoleDto>>(pagedResultDto, true));
            var service = CreateRoleService();

            var response = await service.GetApplicationRolesAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<ApplicationRoleDto>>(response.Result.Items);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetApplicationRoles_WithPagedRequestWithoutOrdersCaseSuccess_ReturnsSuccessfulResponse()
        {
            var roles = new List<ApplicationRole>() { new ApplicationRole() }.AsQueryable().BuildMock();
            var model = new PagedRequestDto() { PageIndex = 0, PageSize = 10 };
            var pagedResultDto = new PagedResultDto<ApplicationRoleDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<ApplicationRoleDto>(),
            };
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<ApplicationRole>, PagedResultDto<ApplicationRoleDto>>(It.IsAny<IPagedList<ApplicationRole>>())).Returns(pagedResultDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<PagedResultDto<ApplicationRoleDto>>(It.IsAny<PagedResultDto<ApplicationRoleDto>>())).Returns(new ServiceResponse<PagedResultDto<ApplicationRoleDto>>(pagedResultDto, true));
            var service = CreateRoleService();

            var response = await service.GetApplicationRolesAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<ApplicationRoleDto>>(response.Result.Items);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetClaimsInRole_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationRole)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<List<ClaimDto>>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<List<ClaimDto>>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.GetClaimsInRoleAsync(_roleName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetClaimsInRole_Success_ReturnsSuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<ClaimDto>>(It.IsAny<List<ClaimDto>>())).Returns(new ServiceResponse<List<ClaimDto>>(null, true));
            var service = CreateRoleService();

            var response = await service.GetClaimsInRoleAsync(_roleName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);

        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetRoleByName_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            SetupRoles();

            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationRoleDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationRoleDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.GetRoleByNameAsync(_roleName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetRoleByName_Success_ReturnsSuccessfulResponse()
        {
            SetupRoles(new List<string> { _roleName });

            _ = _mapperMoq.Setup(r => r.Map<ApplicationRoleDto>(It.IsAny<ApplicationRole>())).Returns(new ApplicationRoleDto());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<ApplicationRoleDto>(It.IsAny<ApplicationRoleDto>())).Returns(new ServiceResponse<ApplicationRoleDto>(new ApplicationRoleDto(), true));
            var service = CreateRoleService();

            var response = await service.GetRoleByNameAsync(_roleName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetRoleClaimsTree_NullParameterCase_ReturnsSuccessfulResponse()
        {
            string model = null;
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<ClaimTreeViewItemDto>>(It.IsAny<List<ClaimTreeViewItemDto>>())).Returns(new ServiceResponse<List<ClaimTreeViewItemDto>>(null, true));
            var service = CreateRoleService();

            var response = await service.GetRoleClaimsTreeAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetRoleClaimsTree_RoleNotFound_ReturnsSuccessfulResponse()
        {
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns(_roleName);
            var roles = new List<ApplicationRole>().AsQueryable().BuildMock();
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<ClaimTreeViewItemDto>>(It.IsAny<List<ClaimTreeViewItemDto>>())).Returns(new ServiceResponse<List<ClaimTreeViewItemDto>>(null, true));
            var service = CreateRoleService();

            var response = await service.GetRoleClaimsTreeAsync(_roleName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetRoleClaimsTree_Success_ReturnsSuccessfulResponse()
        {
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns(_roleName);
            var roles = new List<ApplicationRole>() { new ApplicationRole() { NormalizedName = _roleName } }.AsQueryable().BuildMock();
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<ClaimTreeViewItemDto>>(It.IsAny<List<ClaimTreeViewItemDto>>())).Returns(new ServiceResponse<List<ClaimTreeViewItemDto>>(null, true));
            var service = CreateRoleService();

            var response = await service.GetRoleClaimsTreeAsync(_roleName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetRoleUserInfo_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationRole)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<List<UserRoleInfoDto>>(It.IsAny<List<UserRoleInfoDto>>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<List<UserRoleInfoDto>>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.GetRoleUserInfoAsync(_roleName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetRoleUserInfo_Success_ReturnsSuccessfulResponse()
        {
            var user = new ApplicationUser()
            {
                Id = Guid.NewGuid(),
            };
            var userList = new List<ApplicationUser>() { user };
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            var users = userList.AsQueryable().BuildMock();
            _ = _userManagerMoq.Setup(r => r.Users).Returns(users);
            _ = _userManagerMoq.Setup(r => r.GetUsersInRoleAsync(It.IsAny<string>())).ReturnsAsync(userList);
            var userRoleInfoDto = new UserRoleInfoDto()
            {
                Email = "test@test.com",
                Id = user.Id.ToString(),
                IsInRole = true,
                Name = "testName",
                Surname = "testSurname",
                Username = "testName",
            };
            _ = _mapperMoq.Setup(r => r.Map<IList<ApplicationUser>, List<UserRoleInfoDto>>(It.IsAny<List<ApplicationUser>>())).Returns(new List<UserRoleInfoDto>() { userRoleInfoDto });
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<UserRoleInfoDto>>(It.IsAny<List<UserRoleInfoDto>>())).Returns(new ServiceResponse<List<UserRoleInfoDto>>(new List<UserRoleInfoDto>(), true));
            var service = CreateRoleService();

            var response = await service.GetRoleUserInfoAsync(_roleName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetUsersInRole_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationRole)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<List<UserDto>>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<List<UserDto>>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.GetUsersInRoleAsync(_roleName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task GetUsersInRole_Success_ReturnsSuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _userManagerMoq.Setup(r => r.GetUsersInRoleAsync(It.IsAny<string>())).ReturnsAsync(new List<ApplicationUser>());
            _ = _mapperMoq.Setup(r => r.Map<List<UserDto>>(It.IsAny<List<UserDto>>())).Returns(new List<UserDto>());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<List<UserDto>>(It.IsAny<List<UserDto>>())).Returns(new ServiceResponse<List<UserDto>>(new List<UserDto>(), true));
            var service = CreateRoleService();

            var response = await service.GetUsersInRoleAsync(_roleName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Empty(response.Result);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Post_CreateAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            var model = new ApplicationRoleDto();
            _ = _mapperMoq.Setup(r => r.Map<ApplicationRole>(It.IsAny<ApplicationRoleDto>())).Returns(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.CreateAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Failed(new IdentityError() { Description = "Desc" }));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationRoleDto>(null,It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationRoleDto>(null,new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.PostAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Post_Success_ReturnsSuccessfulResponse()
        {
            var model = new ApplicationRoleDto();
            _ = _mapperMoq.Setup(r => r.Map<ApplicationRole>(It.IsAny<ApplicationRoleDto>())).Returns(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.CreateAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationRoleDto>())).Returns(new ServiceResponse<ApplicationRoleDto>(new ApplicationRoleDto(), true));
            var service = CreateRoleService();

            var response = await service.PostAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Put_AddToRoleAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            SetupRoles(new List<string> { TestConstants.RoleUpdateDto.RoleName });

            _ = _roleManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.IsInRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(false);
            _ = _userManagerMoq.Setup(r => r.AddToRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationRoleDto>(null, It.IsAny<ErrorInfo>(), true)).Returns(new ServiceResponse<ApplicationRoleDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.PutAsync(TestConstants.RoleUpdateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Put_RemoveFromRoleAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            SetupRoles(new List<string> { TestConstants.RoleUpdateDto.RoleName });

            _ = _roleManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.SetupSequence(r => r.IsInRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(false).ReturnsAsync(true);
            _ = _userManagerMoq.Setup(r => r.AddToRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.RemoveFromRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationRoleDto>(It.IsAny<ApplicationRoleDto>(), It.IsAny<ErrorInfo>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationRoleDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.PutAsync(TestConstants.RoleUpdateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Put_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            var model = new RoleUpdateDto();
            SetupRoles(new List<string> { model.RoleName });

            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationRoleDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationRoleDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.PutAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Put_Success_ReturnsSuccessfulResponse()
        {
            SetupRoles(new List<string> { TestConstants.RoleUpdateDto.RoleName });

            _ = _roleManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.SetupSequence(r => r.IsInRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(false).ReturnsAsync(true);
            _ = _userManagerMoq.Setup(r => r.AddToRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.RemoveFromRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse());
            _ = _mapperMoq.Setup(r => r.Map<ApplicationRole, ApplicationRoleDto>(It.IsAny<ApplicationRole>())).Returns(TestConstants.ApplicationRoleDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationRoleDto>())).Returns(new ServiceResponse<ApplicationRoleDto>(new ApplicationRoleDto(), true));
            var service = CreateRoleService();

            var response = await service.PutAsync(TestConstants.RoleUpdateDto).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Put_UpdateAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            SetupRoles(new List<string> { TestConstants.RoleUpdateDto.RoleName });

            _ = _roleManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationRoleDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationRoleDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.PutAsync(TestConstants.RoleUpdateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Put_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            var model = new RoleUpdateDto()
            {
                RoleName = _roleName,
                Translations = new List<ApplicationRoleTranslationDto>
                {
                    new ApplicationRoleTranslationDto { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                },
                UsersInRole = new List<string>() { "userId1" },
                UsersNotInRole = new List<string>() { "userId2" },
            };

            SetupRoles(new List<string> { _roleName });

            _ = _roleManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.Setup(r => r.FindByIdAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), StatusCodes.Status204NoContent, true)).Returns(new ServiceResponse(null, false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationRoleDto>(null, It.IsAny<ErrorInfo>(), true)).Returns(new ServiceResponse<ApplicationRoleDto>(null, false));
            var service = CreateRoleService();

            var response = await service.PutAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Put_UserNotFoundInRemoveUserFromRole_ReturnsUnsuccessfulResponse()
        {
            SetupRoles(new List<string> { TestConstants.RoleUpdateDto.RoleName });

            _ = _roleManagerMoq.Setup(r => r.UpdateAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(IdentityResult.Success);
            _ = _userManagerMoq.SetupSequence(r => r.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser()).ReturnsAsync((ApplicationUser)null);
            _ = _userManagerMoq.Setup(r => r.IsInRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(false);
            _ = _userManagerMoq.Setup(r => r.AddToRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), StatusCodes.Status204NoContent, true)).Returns(new ServiceResponse(null, false));
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<ApplicationRoleDto>(null, It.IsAny<ErrorInfo>(), true)).Returns(new ServiceResponse<ApplicationRoleDto>(null, false));
            var service = CreateRoleService();

            var response = await service.PutAsync(TestConstants.RoleUpdateDto).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveClaimFromRole_ClaimsNotFound_ReturnsUnsuccessfulResponse()
        {
            IList<Claim> claims = null;
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(claims);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.RemoveClaimFromRoleAsync(_roleName, _claimValue).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveClaimFromRole_MatchingClaimNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.RemoveClaimFromRoleAsync(_roleName, "notClaimValue").ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveClaimFromRole_RemoveClaimAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _roleManagerMoq.Setup(r => r.RemoveClaimAsync(It.IsAny<ApplicationRole>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.RemoveClaimFromRoleAsync(_roleName, _claimValue).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveClaimFromRole_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationRole)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.RemoveClaimFromRoleAsync(_roleName, _claimValue).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveClaimFromRole_Success_ReturnsSuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(TestConstants.ClaimList);
            _ = _roleManagerMoq.Setup(r => r.RemoveClaimAsync(It.IsAny<ApplicationRole>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            var service = CreateRoleService();

            var response = await service.RemoveClaimFromRoleAsync(_roleName, _claimValue).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);

        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveUserFromRole_RemoveFromRoleAsyncUnsuccessful_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.RemoveFromRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateRoleService();

            var response = await service.RemoveUserFromRoleAsync(_roleName, _userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveUserFromRole_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationRole)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.RemoveUserFromRoleAsync(_roleName, _userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveUserFromRole_Success_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _userManagerMoq.Setup(r => r.RemoveFromRoleAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            var service = CreateRoleService();

            var response = await service.RemoveUserFromRoleAsync(_roleName, _userName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task RemoveUserFromRole_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _roleManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationRole());
            _ = _userManagerMoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.RemoveUserFromRoleAsync(_roleName, _userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task SaveRoleClaims_RoleNotFound_ReturnsUnsuccessfulResponse()
        {
            var model = new SaveRoleClaimsDto() { Name = _claimValue };
            var roles = new List<ApplicationRole>().AsQueryable().BuildMock();
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateRoleService();

            var response = await service.SaveRoleClaimsAsync(model).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task SaveRoleClaims_Success_ReturnsSuccessfulResponse()
        {
            var claimList = new List<Claim>() { new Claim(ApplicationPolicyType.KsPermission, KsPermissionPolicy.ManagementRoleAddClaim), new Claim(ApplicationPolicyType.KsPermission, KsPermissionPolicy.ManagementRoleAddUser) };
            var model = new SaveRoleClaimsDto() { Name = _roleName, SelectedRoleClaimList = new List<string>() { KsPermissionPolicy.ManagementRoleClaimList } };
            var roles = new List<ApplicationRole>() { new ApplicationRole() { NormalizedName = _roleName } }.AsQueryable().BuildMock();
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns(_roleName);
            _ = _roleManagerMoq.Setup(r => r.GetClaimsAsync(It.IsAny<ApplicationRole>())).ReturnsAsync(claimList);
            _ = _roleManagerMoq.Setup(r => r.RemoveClaimAsync(It.IsAny<ApplicationRole>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Success);
            _ = _roleManagerMoq.Setup(r => r.AddClaimAsync(It.IsAny<ApplicationRole>(), It.IsAny<Claim>())).ReturnsAsync(IdentityResult.Success);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse(true));
            var service = CreateRoleService();

            var response = await service.SaveRoleClaimsAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "RoleService")]
        public async Task Search_Success_ReturnsSuccessfulResponse()
        {
            var model = new RoleSearchDto() { Name = _roleName };
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("anyString");
            var roles = new List<ApplicationRole>() { new ApplicationRole() { NormalizedName = "ANYNAME" } }.AsQueryable().BuildMock();
            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<ApplicationRole>, PagedResultDto<ApplicationRoleDto>>(It.IsAny<IPagedList<ApplicationRole>>())).Returns(new PagedResultDto<ApplicationRoleDto>());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<ApplicationRoleDto>>())).Returns(new ServiceResponse<PagedResultDto<ApplicationRoleDto>>(new PagedResultDto<ApplicationRoleDto>() { PageSize = 10, PageIndex = 0 }, true));
            var service = CreateRoleService();

            var response = await service.SearchAsync(model).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        private RoleService CreateRoleService()
        {
            var localizationMoq = new Mock<IKsStringLocalizer<RoleService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<RoleService>()).Returns(localizationMoq.Object);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);

            var service = new RoleService(
                serviceProvider: serviceProviderMock.Object,
                userManager: _userManagerMoq.Object,
                roleManager: _roleManagerMoq.Object,
                keyNormalizer: _keyNormalizerMoq.Object,
                i18N: iKsI18Nmoq.Object);

            return service;
        }

        private void SetupRoles(List<string> roleList = null)
        {
            var roleMoqList = new List<ApplicationRole>();
            if (roleList != null)
            {
                foreach (var item in roleList)
                {
                    roleMoqList.Add(new ApplicationRole(item)
                    {
                        NormalizedName = item,
                        Translations = new List<ApplicationRoleTranslation>
                        {
                            new ApplicationRoleTranslation() { Description = "anyDescription", DisplayText = "anyDisplayText", Language = Common.Enums.LanguageType.en },
                        },
                    });
                }
            }

            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns(roleMoqList.FirstOrDefault()?.NormalizedName ?? _roleName);
            var roles = roleMoqList.AsQueryable().BuildMock();

            _ = _roleManagerMoq.Setup(r => r.Roles).Returns(roles);
        }
    }
}