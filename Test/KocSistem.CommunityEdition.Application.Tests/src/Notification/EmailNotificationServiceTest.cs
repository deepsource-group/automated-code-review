﻿// <copyright file="EmailNotificationServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Notification;
using KocSistem.OneFrame.Data;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using KocSistem.OneFrame.Notification.Email;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.Notification
{
    public class EmailNotificationServiceTest
    {
        private readonly Mock<IEmailNotification> _emailNotificationMoq;
        private readonly Mock<IServiceProvider> _serviceProviderMoq;
        private readonly Mock<IRepository<Domain.EmailNotification>> _emailNotificationRepositoryMoq;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<ILookupNormalizer> _keyNormalizerMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<IKsStringLocalizer<EmailNotificationService>> _localizeMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IDataManager> _dataManagerMoq;
        private readonly Mock<ILogger<EmailNotificationService>> _loggerMoq;
        private readonly Mock<IApplicationSettingService> _applicationSettingServiceMoq;

        public EmailNotificationServiceTest()
        {
            _emailNotificationMoq = new Mock<IEmailNotification>();
            _serviceProviderMoq = new Mock<IServiceProvider>();
            _emailNotificationRepositoryMoq = new Mock<IRepository<Domain.EmailNotification>>();
            _configurationMoq = new Mock<IConfiguration>();
            _keyNormalizerMoq = new Mock<ILookupNormalizer>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _localizeMoq = new Mock<IKsStringLocalizer<EmailNotificationService>>();
            _mapperMoq = new Mock<IMapper>();
            _dataManagerMoq = new Mock<IDataManager>();
            _loggerMoq = new Mock<ILogger<EmailNotificationService>>();
            _applicationSettingServiceMoq = new Mock<IApplicationSettingService>();
        }

        [Fact]
        [Trait("Category", "EmailNotificationService")]
        public void SendEmail_Successful()
        {
            var emailNotificationResponse = new EmailNotificationResponse(Guid.NewGuid(), true);
            _ = _emailNotificationMoq.Setup(r => r.SendAsync(It.IsAny<EmailContent>(), It.IsAny<List<KocSistem.OneFrame.Notification.Email.User>>(), It.IsAny<CancellationToken>())).ReturnsAsync(emailNotificationResponse);
            var service = CreateEmailNotificationService();
            var result = service.SendEmailAsync("subject", "content", "emailAddress");
            Assert.True(true);
        }

        [Fact]
        [Trait("Category", "EmailNotificationService")]
        public async Task GetList_EmailNotificationNotNull_ReturnsSuccessfulResponse()
        {
            var pagedRequest = new PagedRequestDto()
            {
                Orders = new List<PagedRequestOrderDto>(),
                PageIndex = 0,
                PageSize = 10,
            };

            var responseData = new PagedResultDto<EmailNotificationDto>()
            {
                Items = new List<EmailNotificationDto>()
                {
                    new EmailNotificationDto
                    {
                        To = "To",
                        Subject = "Subject",
                        From = "From",
                    }
                }
            };

            var query = new List<Domain.EmailNotification>() { new Domain.EmailNotification() }.AsQueryable().BuildMock();
            _ = _emailNotificationRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.EmailNotification, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.EmailNotification>, IOrderedQueryable<Domain.EmailNotification>>>(),
                  It.IsAny<Func<IQueryable<Domain.EmailNotification>, IIncludableQueryable<Domain.EmailNotification, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<EmailNotificationDto>>())).Returns(new ServiceResponse<PagedResultDto<EmailNotificationDto>>(responseData, true));
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.EmailNotification>, PagedResultDto<EmailNotificationDto>>(It.IsAny<IPagedList<Domain.EmailNotification>>())).Returns(new PagedResultDto<EmailNotificationDto>());
            var service = CreateEmailNotificationService();

            var response = await service.GetEmailNotificationsAsync(pagedRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(responseData.PageIndex, response.Result.PageIndex);
            Assert.Equal(responseData.PageSize, response.Result.PageSize);
            Assert.Equal(responseData.Items, response.Result.Items);
        }

        [Fact]
        [Trait("Category", "EmailNotificationService")]
        public async Task Search_Success_ReturnsSuccessfulResponse()
        {
            var emailNotificationGetRequest = new EmailNotificationSearchRequestDto() { Value = "anykey" };
            var pagedResultDto = new PagedResultDto<EmailNotificationDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<EmailNotificationDto>(),
            };
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("anykey");
            var query = new List<Domain.EmailNotification>() { new Domain.EmailNotification() { To = "anykey" } }.AsQueryable().BuildMock();
            _ = _emailNotificationRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.EmailNotification, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.EmailNotification>, IOrderedQueryable<Domain.EmailNotification>>>(),
                  It.IsAny<Func<IQueryable<Domain.EmailNotification>, IIncludableQueryable<Domain.EmailNotification, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.EmailNotification>, PagedResultDto<EmailNotificationDto>>(It.IsAny<IPagedList<Domain.EmailNotification>>())).Returns(pagedResultDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<EmailNotificationDto>>())).Returns(new ServiceResponse<PagedResultDto<EmailNotificationDto>>(pagedResultDto, true));
            var service = CreateEmailNotificationService();

            var response = await service.SearchNotificationAsync(emailNotificationGetRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<IList<EmailNotificationDto>>(response.Result.Items);
        }

        private EmailNotificationService CreateEmailNotificationService(List<Claim> claims = null)
        {
            var localizationMoq = new Mock<IKsStringLocalizer<EmailNotificationService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<EmailNotificationService>()).Returns(localizationMoq.Object);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);

            var service = new EmailNotificationService(
                   emailNotification: _emailNotificationMoq.Object,
                   emailNotificationRepository: _emailNotificationRepositoryMoq.Object,
                   mapper: _mapperMoq.Object,
                   serviceResponseHelper: _serviceResponseHelperMoq.Object,
                   keyNormalizer: _keyNormalizerMoq.Object,
                   localize: _localizeMoq.Object,
                   configuration: _configurationMoq.Object,
                   logger: _loggerMoq.Object,
                   applicationSettingService: _applicationSettingServiceMoq.Object,
                   serviceProvider: _serviceProviderMoq.Object
               );

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");

                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);
            }

            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:HistoryLimit"]).Returns("10");
            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:ExpireDays"]).Returns("45");

            return service;
        }
    }
}
