﻿// <copyright file="UserServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.User.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.UserConfirmationHistory;
using KocSistem.CommunityEdition.Application.Abstractions.UserConfirmationHistory.Contract;
using KocSistem.CommunityEdition.Application.User;
using KocSistem.CommunityEdition.Common.Enums;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.TestCommon.Helpers;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using KocSistem.OneFrame.Notification.Sms.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.User
{
    public class UserServiceTest
    {
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<UserManager<ApplicationUser>> _userManagermoq;
        private readonly string _userName = "anyName";
        private readonly Mock<IUserConfirmationHistoryService> _userConfirmationHistoryServiceMoq;
        private readonly Mock<ISmsNotificationService> _smsNotificationServiceMoq;
        private readonly Mock<IConfiguration> _configurationMoq;

        public UserServiceTest()
        {
            _userManagermoq = IdentityMockHelpers.MockUserManager<ApplicationUser>();
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _userConfirmationHistoryServiceMoq = new Mock<IUserConfirmationHistoryService>();
            _smsNotificationServiceMoq = new Mock<ISmsNotificationService>();
            _configurationMoq = new Mock<IConfiguration>();
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task GetCurrentUserInfoAsync_Success_ReturnsSuccessfulResponse()
        {
            _ = _userManagermoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new ApplicationUser());
            _ = _mapperMoq.Setup(r => r.Map<UserDto>(It.IsAny<ApplicationUser>())).Returns(new UserDto());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess<UserDto>(It.IsAny<UserDto>())).Returns(new ServiceResponse<UserDto>(new UserDto() { Username = _userName }, true));
            var service = CreateUserService();

            var response = await service.GetCurrentUserInfoAsync(_userName).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
            Assert.Equal(_userName, response.Result.Username);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task GetCurrentUserInfoAsync_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            _ = _userManagermoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync((ApplicationUser)null);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError<UserDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<UserDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateUserService();

            var response = await service.GetCurrentUserInfoAsync(_userName).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task GetConfirmationCode_Success_ReturnsSuccessfulResponse()
        {
            var user = CreateDummyUser(true);
            var confirmationHistoryModel = CreateDummyConfirmationHistory();
            _ = _userManagermoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            _ = _userConfirmationHistoryServiceMoq
                .Setup(r => r.GetActiveCodeAsync(It.IsAny<Guid>(), It.IsAny<string>()))
                .ReturnsAsync(new ServiceResponse<UserConfirmationHistoryDto>(confirmationHistoryModel));
            _ = _serviceResponseHelperMoq
                .Setup(s => s.SetSuccess(It.IsAny<UserConfirmationHistoryDto>()))
                .Returns(new ServiceResponse<UserConfirmationHistoryDto>(confirmationHistoryModel, true));

            var service = CreateUserService();

            var response = await service.GetConfirmationCodeAsync("test@test.com", "0000000000").ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task GetConfirmationCode_UserNotFound_ReturnsUnsuccessfulResponse()
        {
            ApplicationUser user = null;
            _ = _userManagermoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError<UserConfirmationHistoryDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new ServiceResponse<UserConfirmationHistoryDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));

            var service = CreateUserService();

            var response = await service.GetConfirmationCodeAsync("test@test.com", "0000000000").ConfigureAwait(false);

            Assert.Equal(204, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task GetConfirmationCode_PassiveUser_ReturnsUnsuccessfulResponse()
        {
            var user = CreateDummyUser();
            _ = _userManagermoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError<UserConfirmationHistoryDto>(null, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new ServiceResponse<UserConfirmationHistoryDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = CreateUserService();

            var response = await service.GetConfirmationCodeAsync("test@test.com", "0000000000").ConfigureAwait(false);

            Assert.Equal(400, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task SendConfirmationCode_Success_ReturnsSuccessfulResponse()
        {
            var confirmationHistoryModel = CreateDummyConfirmationHistory();
            var smsResponse = new SendSmsResponse(Guid.Parse("11111111-1111-1111-1111-111111111111"), true);
            _smsNotificationServiceMoq
                .Setup(r => r.SendSmsAsync(It.IsAny<SendSmsRequest>()))
                .ReturnsAsync(smsResponse);
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetSuccess())
                .Returns(new ServiceResponse());

            var service = CreateUserService();

            var response = await service.SendConfirmationCodeAsync(confirmationHistoryModel).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task SendConfirmationCode_InternalError_ReturnsUnsuccessfulResponse()
        {
            var confirmationHistoryModel = CreateDummyConfirmationHistory();
            var smsResponse = new SendSmsResponse(Guid.Parse("11111111-1111-1111-1111-111111111111"), false);
            _ = _smsNotificationServiceMoq
                .Setup(r => r.SendSmsAsync(It.IsAny<SendSmsRequest>()))
                .ReturnsAsync(smsResponse);
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new ServiceResponse(new ErrorInfo()));

            var service = CreateUserService();

            var response = await service.SendConfirmationCodeAsync(confirmationHistoryModel).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task CheckConfirmationCode_Success_ReturnsSuccessfulResponse()
        {
            var confirmationHistoryModel = CreateDummyConfirmationHistory();
            _ = _userConfirmationHistoryServiceMoq
                .Setup(r => r.CheckCodeAsSentAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new ServiceResponse<UserConfirmationHistoryDto>(confirmationHistoryModel));
            _ = _serviceResponseHelperMoq
                .Setup(s => s.SetSuccess())
                .Returns(new ServiceResponse());

            var service = CreateUserService();

            var response = await service.CheckConfirmationCodeAsync(confirmationHistoryModel).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task CheckConfirmationCode_CodeNotFound_ReturnsUnsuccessfulResponse()
        {
            var confirmationHistoryModel = CreateDummyConfirmationHistory();
            _ = _userConfirmationHistoryServiceMoq
                .Setup(r => r.CheckCodeAsSentAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new ServiceResponse<UserConfirmationHistoryDto>(null, new ErrorInfo { Code = StatusCodes.Status400BadRequest }, false));
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError(It.IsAny<ErrorInfo>(), It.IsAny<bool>()))
                .Returns(new ServiceResponse(new ErrorInfo()));

            var service = CreateUserService();

            var response = await service.CheckConfirmationCodeAsync(confirmationHistoryModel).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task ConfirmCode_Success_ReturnSuccessfulResponse()
        {
            var confirmationHistoryModel = CreateDummyConfirmationHistory();
            _ = _userConfirmationHistoryServiceMoq
                .Setup(r => r.ConfirmCodeAsync(It.IsAny<UserConfirmationHistoryDto>()))
                .ReturnsAsync(new ServiceResponse<UserConfirmationHistoryDto>(confirmationHistoryModel));
            _ = _serviceResponseHelperMoq
                .Setup(s => s.SetSuccess())
                .Returns(new ServiceResponse());

            var service = CreateUserService();

            var response = await service.ConfirmCodeAsync(confirmationHistoryModel).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task ConfirmCode_CodeNotFound_ReturnsUnsuccessfulResponse()
        {
            var confirmationHistoryModel = CreateDummyConfirmationHistory();
            _ = _userConfirmationHistoryServiceMoq
                .Setup(r => r.ConfirmCodeAsync(It.IsAny<UserConfirmationHistoryDto>()))
                .ReturnsAsync(new ServiceResponse<UserConfirmationHistoryDto>(null, new ErrorInfo { Code = StatusCodes.Status400BadRequest }, false));
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError(It.IsAny<ErrorInfo>(), It.IsAny<bool>()))
                .Returns(new ServiceResponse(new ErrorInfo()));

            var service = CreateUserService();

            var response = await service.ConfirmCodeAsync(confirmationHistoryModel).ConfigureAwait(false);

            Assert.False(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task CheckUserConfirmation_Success_ReturnSuccessfulResponse()
        {
            var user = CreateDummyUser(true);
            _ = _userManagermoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetSuccess())
                .Returns(new ServiceResponse());

            var service = CreateUserService();

            var response = await service.CheckUserConfirmationAsync(user.Email).ConfigureAwait(false);

            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task CheckUserConfirmation_UserNotFound_ReturnUnsuccessfulResponse()
        {
            ApplicationUser user = null;
            _ = _userManagermoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status204NoContent), false));

            var service = CreateUserService();

            var response = await service.CheckUserConfirmationAsync("test@test.com").ConfigureAwait(false);

            Assert.Equal(204, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "UserService")]
        public async Task CheckUserConfirmation_PassiveUser_ReturnUnsuccessfulResponse()
        {
            var user = CreateDummyUser();
            _ = _userManagermoq.Setup(r => r.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            _ = _serviceResponseHelperMoq
                .Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new ServiceResponse(new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = CreateUserService();

            var response = await service.CheckUserConfirmationAsync("test@test.com").ConfigureAwait(false);

            Assert.Equal(400, response.Error.Code);
        }

        private static ApplicationUser CreateDummyUser(bool isActive = false)
        {
            return new ApplicationUser()
            {
                Id = Guid.Parse("00000000-0000-0000-0000-000000000000"),
                Email = "test@test.com",
                PhoneNumber = "0000000000",
                IsActive = isActive
            };
        }

        private static UserConfirmationHistoryDto CreateDummyConfirmationHistory(bool isUsed = false)
        {
            var expiredDate = DateTime.UtcNow.AddMinutes(2);

            return new UserConfirmationHistoryDto()
            {
                Code = "123456",
                CodeType = (int)ConfirmationType.PhoneNumber,
                ExpiredDate = expiredDate,
                Id = Guid.Parse("9c15bad3-56d6-4c8e-9b03-5d3201194737"),
                IsUsed = isUsed,
                PhoneNumber = "0000000000",
                UserId = Guid.Parse("00000000-0000-0000-0000-000000000000")
            };
        }

        private UserService CreateUserService()
        {
            var localizationMoq = new Mock<IKsStringLocalizer<UserService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<UserService>()).Returns(localizationMoq.Object);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);

            var service = new UserService(
                serviceProvider: serviceProviderMock.Object,
                userManager: _userManagermoq.Object,
                userConfirmationHistoryService: _userConfirmationHistoryServiceMoq.Object,
                smsNotificationService: _smsNotificationServiceMoq.Object,
                configuration: _configurationMoq.Object);
            return service;
        }
    }
}