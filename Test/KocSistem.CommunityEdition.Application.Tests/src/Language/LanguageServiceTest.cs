﻿using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Language.Contracts;
using KocSistem.CommunityEdition.Application.Language;
using KocSistem.OneFrame.Data;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Localization;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Application.Tests.Language
{
    public class LanguageServiceTest
    {
        private readonly Mock<IRepository<Domain.Language>> _languageRepositoryMoq;
        private readonly Mock<IDataManager> _dataManagerMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<ILookupNormalizer> _keyNormalizerMoq;

        public LanguageServiceTest()
        {
            _languageRepositoryMoq = new Mock<IRepository<Domain.Language>>();
            _dataManagerMoq = new Mock<IDataManager>();
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _keyNormalizerMoq = new Mock<ILookupNormalizer>();
        }

        [Fact]
        [Trait("Category", "LanguageService")]
        public async Task GetList_LanguageNotNull_ReturnsSuccessfulResponse()
        {
            var pagedRequest = new PagedRequestDto()
            {
                Orders = new List<PagedRequestOrderDto>(),
                PageIndex = 0,
                PageSize = 10,
            };
            var query = new List<Domain.Language>() { new Domain.Language() }.AsQueryable().BuildMock();
            _ = _languageRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.Language, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IOrderedQueryable<Domain.Language>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IIncludableQueryable<Domain.Language, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<LanguageDto>>())).Returns(new ServiceResponse<PagedResultDto<LanguageDto>>(new PagedResultDto<LanguageDto>(), true));
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.Language>, PagedResultDto<LanguageDto>>(It.IsAny<IPagedList<Domain.Language>>())).Returns(new PagedResultDto<LanguageDto>());
            var service = CreateLanguageService();

            var response = await service.GetLanguageListAsync(pagedRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "LanguageService")]
        public async Task CreateLanguage_ReturnsSuccessfulResponse()
        {
            var result = new Domain.Language();
            var LanguageDto = new LanguageDto()
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "base64",
                IsActive = true,
                IsDefault = false,
            };
            var query = new List<Domain.Language>() { new Domain.Language() { Name = "English" } }.AsQueryable().BuildMock();
            _ = _languageRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.Language, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IOrderedQueryable<Domain.Language>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IIncludableQueryable<Domain.Language, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _languageRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.Language, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IOrderedQueryable<Domain.Language>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IIncludableQueryable<Domain.Language, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(result);

            _ = _mapperMoq.Setup(r => r.Map<Domain.Language, LanguageDto>(It.IsAny<Domain.Language>())).Returns(LanguageDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<LanguageDto>())).Returns(new ServiceResponse<LanguageDto>(LanguageDto, true));
            var service = CreateLanguageService();

            var response = await service.CreateAsync(LanguageDto).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(LanguageDto.Id, response.Result.Id);
            Assert.Equal(LanguageDto.Name, response.Result.Name);
            Assert.Equal(LanguageDto.Code, response.Result.Code);
            Assert.Equal(LanguageDto.Image, response.Result.Image);
            Assert.Equal(LanguageDto.IsActive, response.Result.IsActive);
            Assert.Equal(LanguageDto.IsDefault, response.Result.IsDefault);
            Assert.Equal(LanguageDto.UpdatedUser, response.Result.UpdatedUser);
            Assert.Equal(LanguageDto.UpdatedDate, response.Result.UpdatedDate);
        }        

        [Fact]
        [Trait("Category", "LanguageService")]
        public async Task UpdateLanguage_ReturnsSuccessfulResponse()
        {
            var result = new Domain.Language();
            var LanguageDto = new LanguageDto()
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "base64",
                IsActive = true,
                IsDefault = false,
            };
            var query = new List<Domain.Language>() { new Domain.Language() { Name = "English" } }.AsQueryable().BuildMock();
            _ = _languageRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.Language, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IOrderedQueryable<Domain.Language>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IIncludableQueryable<Domain.Language, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _languageRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.Language, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IOrderedQueryable<Domain.Language>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IIncludableQueryable<Domain.Language, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(result);

            _ = _mapperMoq.Setup(r => r.Map<Domain.Language, LanguageDto>(It.IsAny<Domain.Language>())).Returns(LanguageDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<LanguageDto>())).Returns(new ServiceResponse<LanguageDto>(LanguageDto, true));
            var service = CreateLanguageService();

            var response = await service.UpdateAsync(LanguageDto).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(LanguageDto.Id, response.Result.Id);
            Assert.Equal(LanguageDto.Name, response.Result.Name);
            Assert.Equal(LanguageDto.Code, response.Result.Code);
            Assert.Equal(LanguageDto.Image, response.Result.Image);
            Assert.Equal(LanguageDto.IsActive, response.Result.IsActive);
            Assert.Equal(LanguageDto.IsDefault, response.Result.IsDefault);
            Assert.Equal(LanguageDto.UpdatedUser, response.Result.UpdatedUser);
            Assert.Equal(LanguageDto.UpdatedDate, response.Result.UpdatedDate);
        }

        [Fact]
        [Trait("Category", "LanguageService")]
        public async Task DeleteLanguage_LanguageToDeleteNull_ReturnsUnsuccessfulResponse()
        {
            _ = _languageRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(null, null, null, true)).ReturnsAsync(new Domain.Language());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<LanguageDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<LanguageDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateLanguageService();

            var response = await service.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "LanguageService")]
        public async Task DeleteLanguage_IsDefaultTrue_ReturnsUnsuccessfulResponse()
        {
            var languageToDelete = new Domain.Language
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "base64",
                IsActive = true,
                IsDefault = true,
            };

            var LanguageDto = new LanguageDto()
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "base64",
                IsActive = true,
                IsDefault = false,
            };
            _ = _languageRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                         It.IsAny<Expression<Func<Domain.Language, bool>>>(),
                         It.IsAny<Func<IQueryable<Domain.Language>, IOrderedQueryable<Domain.Language>>>(),
                         It.IsAny<Func<IQueryable<Domain.Language>, IIncludableQueryable<Domain.Language, object>>>(),
                         It.IsAny<bool>())).ReturnsAsync(languageToDelete);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<LanguageDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<LanguageDto>(LanguageDto, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = this.CreateLanguageService();

            var response = await service.DeleteAsync(languageToDelete.Id).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "LanguageService")]
        public async Task DeleteLanguage_ReturnsSuccessfulResponse()
        {
            var languageToDelete = new Domain.Language()
            {
                Id = new Guid("5a41be5e-0cb9-4a3e-a1a7-0244b53134cc"),
                Name = "English",
                Code = "en-En",
                Image = "base64",
                IsActive = true,
                IsDefault = false,
            };

            _ = _languageRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                 It.IsAny<Expression<Func<Domain.Language, bool>>>(),
                 It.IsAny<Func<IQueryable<Domain.Language>, IOrderedQueryable<Domain.Language>>>(),
                 It.IsAny<Func<IQueryable<Domain.Language>, IIncludableQueryable<Domain.Language, object>>>(),
                 It.IsAny<bool>())).ReturnsAsync(languageToDelete);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse<LanguageDto>(null, true));
            var service = CreateLanguageService();

            var response = await service.DeleteAsync(languageToDelete.Id).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "LanguageService")]
        public async Task Search_Success_ReturnsSuccessfulResponse()
        {
            var applicationSettingGetRequest = new LanguageSearchDto() { Key = "anykey" };
            var pagedResultDto = new PagedResultDto<LanguageDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<LanguageDto>(),
            };
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("anykey");
            var query = new List<Domain.Language>() { new Domain.Language() { Name = "anykey" } }.AsQueryable().BuildMock();
            _ = _languageRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.Language, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IOrderedQueryable<Domain.Language>>>(),
                  It.IsAny<Func<IQueryable<Domain.Language>, IIncludableQueryable<Domain.Language, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.Language>, PagedResultDto<LanguageDto>>(It.IsAny<IPagedList<Domain.Language>>())).Returns(pagedResultDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<LanguageDto>>())).Returns(new ServiceResponse<PagedResultDto<LanguageDto>>(pagedResultDto, true));
            var service = CreateLanguageService();

            var response = await service.SearchAsync(applicationSettingGetRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<LanguageDto>>(response.Result.Items);
        }

        private LanguageService CreateLanguageService()
        {
            var localizationMoq = new Mock<IKsStringLocalizer<LanguageService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<LanguageService>()).Returns(localizationMoq.Object);

            var service = new LanguageService(
                    languageRepository: _languageRepositoryMoq.Object,
                    mapper: _mapperMoq.Object,
                    dataManager: _dataManagerMoq.Object,
                    i18N: iKsI18Nmoq.Object,
                    serviceResponseHelper: _serviceResponseHelperMoq.Object,
                    keyNormalizer: _keyNormalizerMoq.Object);

            return service;
        }
    }
}
