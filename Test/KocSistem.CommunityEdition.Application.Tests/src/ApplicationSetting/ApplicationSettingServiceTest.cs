﻿// <copyright file="ApplicationSettingServiceTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using KocSistem.OneFrame.Data;
using Xunit;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using KocSistem.CommunityEdition.Application.Setting;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting;
using System.Linq.Expressions;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using MockQueryable.Moq;
using KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting.Contracts;
using KocSistem.CommunityEdition.Common.Helpers.ApplicationSetting;
using Microsoft.Extensions.Configuration;
using KocSistem.OneFrame.DesignObjects.Models;

namespace KocSistem.CommunityEdition.Application.Tests.ApplicationSetting
{
    public class ApplicationSettingServiceTest
    {
        private readonly Mock<IRepository<Domain.ApplicationSetting>> _applicationSettingRepositoryMoq;
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly Mock<IDataManager> _dataManagerMoq;
        private readonly Mock<ILookupNormalizer> _keyNormalizerMoq;
        private readonly Mock<IMapper> _mapperMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<IApplicationSettingConfig> _applicationSettingConfigMoq;

        public ApplicationSettingServiceTest()
        {
            _applicationSettingRepositoryMoq = new Mock<IRepository<Domain.ApplicationSetting>>();
            _configurationMoq = new Mock<IConfiguration>();
            _dataManagerMoq = new Mock<IDataManager>();
            _keyNormalizerMoq = new Mock<ILookupNormalizer>();
            _mapperMoq = new Mock<IMapper>();
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _applicationSettingConfigMoq = new Mock<IApplicationSettingConfig>();
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task CreateApplicationSetting_ApplicationSettingDtoNull_ReturnsUnsuccessfulResponse()
        {
            ApplicationSettingDto applicationSettingDto = null;
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateApplicationSettingService();

            var response = await service.CreateAsync(applicationSettingDto).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task CreateApplicationSetting_IsExistTrue_ReturnsUnsuccessfulResponse()
        {
            var applicationSetting = new Domain.ApplicationSetting() { Key = "" };
            var applicationSettingDto = new ApplicationSettingDto()
            {
                Status = "teststatus",
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            var query = new List<Domain.ApplicationSetting>() { applicationSetting }.AsQueryable().BuildMock();
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(), It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(applicationSetting);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));

            var service = CreateApplicationSettingService();

            var response = await service.CreateAsync(applicationSettingDto).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task CreateApplicationSetting_ReturnsSuccessfulResponse()
        {
            var result = new Domain.ApplicationSetting();
            var applicationSettingDto = new ApplicationSettingDto
            {
                Status = "teststatus",
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            var query = new List<Domain.ApplicationSetting>() { new Domain.ApplicationSetting() { Key = "anykey" } }.AsQueryable().BuildMock();
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, Domain.ApplicationSetting>>>(),
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(result);

            _ = _mapperMoq.Setup(r => r.Map<Domain.ApplicationSetting, ApplicationSettingDto>(It.IsAny<Domain.ApplicationSetting>())).Returns(applicationSettingDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationSettingDto>())).Returns(new ServiceResponse<ApplicationSettingDto>(applicationSettingDto, true));
            var service = CreateApplicationSettingService();

            var response = await service.CreateAsync(applicationSettingDto).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(applicationSettingDto.Id, response.Result.Id);
            Assert.Equal(applicationSettingDto.Key, response.Result.Key);
            Assert.Equal(applicationSettingDto.Value, response.Result.Value);
            Assert.Equal(applicationSettingDto.CategoryId, response.Result.CategoryId);
            Assert.Equal(applicationSettingDto.ValueType, response.Result.ValueType);
            Assert.Equal(applicationSettingDto.Status, response.Result.Status);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task DeleteApplicationSetting_ApplicationSettingToDeleteNull_ReturnsUnsuccessfulResponse()
        {
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(null, null, null, true)).ReturnsAsync(new Domain.ApplicationSetting());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateApplicationSettingService();

            var response = await service.DeleteAsync(Guid.NewGuid()).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task DeleteApplicationSetting_IsStaticTrue_ReturnsUnsuccessfulResponse()
        {
            var applicationSettingToDelete = new Domain.ApplicationSetting
            {
                Category = new Domain.ApplicationSettingCategory(),
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            var applicationSettingDto = new ApplicationSettingDto
            {
                Status = "teststatus",
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync<Domain.ApplicationSetting>(
                It.IsAny<Expression<Func<Domain.ApplicationSetting, Domain.ApplicationSetting>>>(),
                It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                It.IsAny<bool>())).ReturnsAsync(applicationSettingToDelete);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingDto>(applicationSettingDto, new ErrorInfo(StatusCodes.Status400BadRequest), false)); ;
            var service = this.CreateApplicationSettingService();

            var response = await service.DeleteAsync(applicationSettingToDelete.Id).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task DeleteApplicationSetting_ReturnsSuccessfulResponse()
        {
            var applicationSettingToDelete = new Domain.ApplicationSetting()
            {
                Category = new Domain.ApplicationSettingCategory(),
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = false,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, Domain.ApplicationSetting>>>(),
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(applicationSettingToDelete);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess()).Returns(new ServiceResponse<ApplicationSettingDto>(null, true));
            var service = CreateApplicationSettingService();

            var response = await service.DeleteAsync(applicationSettingToDelete.Id).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task GetById_ApplicationSettingNotNull_ReturnsSuccessfulResponse()
        {
            var applicationSetting = new Domain.ApplicationSetting
            {
                Category = new Domain.ApplicationSettingCategory(),
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            var applicationSettingDto = new ApplicationSettingDto
            {
                Status = "teststatus",
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(applicationSetting);

            _ = _mapperMoq.Setup(r => r.Map<Domain.ApplicationSetting, ApplicationSettingDto>(It.IsAny<Domain.ApplicationSetting>())).Returns(applicationSettingDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationSettingDto>())).Returns(new ServiceResponse<ApplicationSettingDto>(applicationSettingDto, true));

            var service = CreateApplicationSettingService();

            var response = await service.GetByIdAsync(applicationSetting.Id).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(applicationSettingDto.Id, response.Result.Id);
            Assert.Equal(applicationSettingDto.Key, response.Result.Key);
            Assert.Equal(applicationSettingDto.Value, response.Result.Value);
            Assert.Equal(applicationSettingDto.CategoryId, response.Result.CategoryId);
            Assert.Equal(applicationSettingDto.ValueType, response.Result.ValueType);
            Assert.Equal(applicationSettingDto.Status, response.Result.Status);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task GetById_ApplicationSettingNull_ReturnsUnsuccessfulResponse()
        {
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(null, null, null, true)).ReturnsAsync(new Domain.ApplicationSetting());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateApplicationSettingService();

            var response = await service.GetByIdAsync(Guid.NewGuid()).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task GetByKey_ApplicationSettingNotNull_ReturnsSuccessfulResponse()
        {
            var applicationSetting = new Domain.ApplicationSetting
            {
                Category = new Domain.ApplicationSettingCategory(),
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            var applicationSettingDto = new ApplicationSettingDto
            {
                Status = "teststatus",
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            var key = "testkey";
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                It.IsAny<bool>())).ReturnsAsync(applicationSetting);
            _ = _mapperMoq.Setup(r => r.Map<Domain.ApplicationSetting, ApplicationSettingDto>(It.IsAny<Domain.ApplicationSetting>())).Returns(applicationSettingDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationSettingDto>())).Returns(new ServiceResponse<ApplicationSettingDto>(applicationSettingDto, true));

            var service = CreateApplicationSettingService();

            var response = await service.GetByKeyAsync(key).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(applicationSettingDto.Id, response.Result.Id);
            Assert.Equal(applicationSettingDto.Key, response.Result.Key);
            Assert.Equal(applicationSettingDto.Value, response.Result.Value);
            Assert.Equal(applicationSettingDto.CategoryId, response.Result.CategoryId);
            Assert.Equal(applicationSettingDto.ValueType, response.Result.ValueType);
            Assert.Equal(applicationSettingDto.Status, response.Result.Status);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task GetByKey_ApplicationSettingNull_ReturnsUnsuccessfulResponse()
        {
            var key = "testkey";
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(null, null, null, true)).ReturnsAsync(new Domain.ApplicationSetting());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateApplicationSettingService();

            var response = await service.GetByKeyAsync(key).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task GetList_ApplicationSettingNotNull_ReturnsSuccessfulResponse()
        {
            var pagedRequest = new PagedRequestDto()
            {
                Orders = new List<PagedRequestOrderDto>(),
                PageIndex = 0,
                PageSize = 10,
            };
            var query = new List<Domain.ApplicationSetting>() { new Domain.ApplicationSetting() }.AsQueryable().BuildMock();
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<ApplicationSettingDetailDto>>())).Returns(new ServiceResponse<PagedResultDto<ApplicationSettingDetailDto>>(new PagedResultDto<ApplicationSettingDetailDto>(), true));
            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.ApplicationSetting>, PagedResultDto<ApplicationSettingDetailDto>>(It.IsAny<IPagedList<Domain.ApplicationSetting>>())).Returns(new PagedResultDto<ApplicationSettingDetailDto>());
            var service = CreateApplicationSettingService();

            var response = await service.GetListAsync(pagedRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task Search_Success_ReturnsSuccessfulResponse()
        {
            var applicationSettingGetRequest = new ApplicationSettingSearchDto() { Key = "anykey" };
            var pagedResultDto = new PagedResultDto<ApplicationSettingDetailDto>()
            {
                PageIndex = 0,
                PageSize = 10,
                Items = new List<ApplicationSettingDetailDto>(),
            };
            _ = _keyNormalizerMoq.Setup(r => r.NormalizeName(It.IsAny<string>())).Returns("anykey");
            var query = new List<Domain.ApplicationSetting>() { new Domain.ApplicationSetting() { Key = "anykey" } }.AsQueryable().BuildMock();
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetQueryable(
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                  It.IsAny<bool>())).Returns(query);

            _ = _mapperMoq.Setup(r => r.Map<IPagedList<Domain.ApplicationSetting>, PagedResultDto<ApplicationSettingDetailDto>>(It.IsAny<IPagedList<Domain.ApplicationSetting>>())).Returns(pagedResultDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<PagedResultDto<ApplicationSettingDetailDto>>())).Returns(new ServiceResponse<PagedResultDto<ApplicationSettingDetailDto>>(pagedResultDto, true));
            var service = CreateApplicationSettingService();

            var response = await service.SearchAsync(applicationSettingGetRequest).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(0, response.Result.PageIndex);
            Assert.Equal(10, response.Result.PageSize);
            Assert.IsAssignableFrom<List<ApplicationSettingDetailDto>>(response.Result.Items);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task UpdateApplicationSetting_ApplicationSettingDtoNull_ReturnsUnsuccessfulResponse()
        {
            ApplicationSettingDto applicationSettingdto = null;
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingDto>(null, new ErrorInfo(StatusCodes.Status400BadRequest), false));
            var service = CreateApplicationSettingService();

            var response = await service.UpdateAsync(applicationSettingdto).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status400BadRequest, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task UpdateApplicationSetting_ApplicationSettingToUpdateNull_ReturnsUnsuccessfulResponse()
        {
            var applicationSettingDto = new ApplicationSettingDto();
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(null, null, null, true)).ReturnsAsync(new Domain.ApplicationSetting());
            _ = _serviceResponseHelperMoq.Setup(r => r.SetError(It.IsAny<ApplicationSettingDto>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new ServiceResponse<ApplicationSettingDto>(null, new ErrorInfo(StatusCodes.Status204NoContent), false));
            var service = CreateApplicationSettingService();

            var response = await service.UpdateAsync(applicationSettingDto).ConfigureAwait(false);
            Assert.False(response.IsSuccessful);
            Assert.Equal(StatusCodes.Status204NoContent, response.Error.Code);
        }

        [Fact]
        [Trait("Category", "ApplicationSettingService")]
        public async Task UpdateApplicationSetting_ReturnsSuccessfulResponse()
        {
            var applicationSettingToUpdate = new Domain.ApplicationSetting();
            var applicationSettingDto = new ApplicationSettingDto
            {
                Status = "teststatus",
                CategoryId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsStatic = true,
                Key = "testkey",
                Value = "testvalue",
                ValueType = "testvaluetype",
            };
            _ = _applicationSettingRepositoryMoq.Setup(r => r.GetFirstOrDefaultAsync(
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, Domain.ApplicationSetting>>>(),
                  It.IsAny<Expression<Func<Domain.ApplicationSetting, bool>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IOrderedQueryable<Domain.ApplicationSetting>>>(),
                  It.IsAny<Func<IQueryable<Domain.ApplicationSetting>, IIncludableQueryable<Domain.ApplicationSetting, object>>>(),
                  It.IsAny<bool>())).ReturnsAsync(applicationSettingToUpdate);

            _ = _mapperMoq.Setup(r => r.Map<Domain.ApplicationSetting, ApplicationSettingDto>(It.IsAny<Domain.ApplicationSetting>())).Returns(applicationSettingDto);
            _ = _serviceResponseHelperMoq.Setup(r => r.SetSuccess(It.IsAny<ApplicationSettingDto>())).Returns(new ServiceResponse<ApplicationSettingDto>(applicationSettingDto, true));
            var service = CreateApplicationSettingService();

            var response = await service.UpdateAsync(applicationSettingDto).ConfigureAwait(false);
            Assert.True(response.IsSuccessful);
            Assert.Equal(applicationSettingDto.CategoryId, applicationSettingDto.CategoryId);
            Assert.Equal(applicationSettingDto.Key, applicationSettingDto.Key);
            Assert.Equal(applicationSettingDto.Value, applicationSettingDto.Value);
            Assert.Equal(applicationSettingDto.ValueType, applicationSettingDto.ValueType);
            Assert.Equal(applicationSettingDto.IsStatic, applicationSettingDto.IsStatic);
        }

        private ApplicationSettingService CreateApplicationSettingService(List<Claim> claims = null)
        {
            var localizationMoq = new Mock<IKsStringLocalizer<ApplicationSettingService>>();
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));

            var iKsI18Nmoq = new Mock<IKsI18N>();
            _ = iKsI18Nmoq.Setup(r => r.GetLocalizer<ApplicationSettingService>()).Returns(localizationMoq.Object);

            var serviceProviderMock = new Mock<IServiceProvider>();
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IMapper))).Returns(_mapperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IServiceResponseHelper))).Returns(_serviceResponseHelperMoq.Object);
            _ = serviceProviderMock.Setup(serviceProvider => serviceProvider.GetService(typeof(IKsI18N))).Returns(iKsI18Nmoq.Object);

            _ = _applicationSettingConfigMoq.Setup(s => s.CategoryNameList).Returns(new List<string> { "categoryName" });

            var service = new ApplicationSettingService(
                    applicationSettingRepository: _applicationSettingRepositoryMoq.Object,
                    mapper: _mapperMoq.Object,
                    dataManager: _dataManagerMoq.Object,
                    i18N: iKsI18Nmoq.Object,
                    serviceResponseHelper: _serviceResponseHelperMoq.Object,
                    keyNormalizer: _keyNormalizerMoq.Object,
                    applicationSettingConfig: _applicationSettingConfigMoq.Object);

            if (claims != null)
            {
                var identity = new ClaimsIdentity(claims: claims, authenticationType: "Test");

                var mockPrincipal = new Mock<IPrincipal>();
                _ = mockPrincipal.Setup(expression: x => x.Identity).Returns(value: identity);
                _ = mockPrincipal.Setup(expression: x => x.IsInRole(It.IsAny<string>())).Returns(value: true);
            }

            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:HistoryLimit"]).Returns("10");
            _ = _configurationMoq.SetupGet(m => m["Identity:Policy:Password:ExpireDays"]).Returns("45");

            return service;
        }
    }
}