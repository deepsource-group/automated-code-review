﻿// -----------------------------------------------------------------------
// <copyright file="ExcelExportHelperTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
// -----------------------------------------------------------------------

using System;
using KocSistem.CommunityEdition.Common.Extensions;
using Xunit;

namespace KocSistem.CommunityEdition.Common.Tests
{
    public class DateTimeExtensionsTest
    {
        public DateTimeExtensionsTest()
        {
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromTimeZone_ToUtc_ReturnsLocalTime()
        {
            var timeZoneDate = new DateTime(2022, 01, 01, 00, 00, 00);

            var utc = timeZoneDate.FromTimeZoneToUtc("America/Anchorage");

            Assert.Equal(new DateTime(2022, 01, 01, 09, 00, 00), utc);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromTimeZone_ToUtc_ReturnsLocalTimeDayLight()
        {
            var timeZoneDate = new DateTime(2022, 07, 01, 00, 00, 00);

            var utc = timeZoneDate.FromTimeZoneToUtc("America/Anchorage");

            Assert.Equal(new DateTime(2022, 07, 01, 08, 00, 00), utc);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromTimeZone_ToUtc_ReturnsLocalTimeWithRegion()
        {
            var timeZoneDate = new DateTime(2022, 01, 01, 00, 00, 00);

            var utc = timeZoneDate.FromTimeZoneToUtc("Central Standard Time");

            Assert.Equal(new DateTime(2022, 01, 01, 06, 00, 00), utc);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromTimeZone_ToUtc_ReturnsLocalWithCulture()
        {
            var timeZoneDate = new DateTime(2022, 07, 01, 12, 00, 00);

            var utc = timeZoneDate.FromTimeZoneToUtcWithCulture("Central Standard Time", "en-US");

            Assert.Equal("7/1/2022 5:00:00 PM", utc);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromTimeZone_ToUtc_ReturnsLocalWithCulture2()
        {
            var timeZoneDate = new DateTime(2022, 07, 01, 12, 00, 00);

            var utc = timeZoneDate.FromTimeZoneToUtcWithCulture("Central Standard Time", "tr-TR");

            Assert.Equal("1.07.2022 17:00:00", utc);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromTimeZone_ToUtc_ReturnsLocalWithCulture3()
        {
            var timeZoneDate = new DateTime(2022, 07, 01, 12, 00, 00);

            var utc = timeZoneDate.FromTimeZoneToUtcWithCulture("Central Standard Time");

            Assert.Equal("7/1/2022 5:00:00 PM", utc);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromTimeZone_ToUtc_ReturnsUtcForError()
        {
            var dateTime = new DateTime(2022, 01, 01, 12, 00, 00);

            Assert.Throws<TimeZoneNotFoundException>(() => dateTime.FromTimeZoneToUtc("Somewhere Standard Time"));
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromUtc_ToTimeZone_ReturnsLocalTime()
        {
            var dateTime = new DateTime(2022, 01, 01, 00, 00, 00);

            var localTime = dateTime.ToTimeZone("America/Anchorage");

            Assert.Equal(new DateTime(2021, 12, 31, 15, 00, 00), localTime);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromUtc_ToTimeZone_ReturnsLocalTimeDayLight()
        {
            var dateTimeUtc = new DateTime(2022, 07, 01, 00, 00, 00);

            var localTime = dateTimeUtc.ToTimeZone("America/Anchorage");

            Assert.Equal(new DateTime(2022, 06, 30, 16, 00, 00), localTime);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromUtc_ToTimeZone_ReturnsLocalTimeWithRegion()
        {
            var dateTime = new DateTime(2022, 01, 01, 00, 00, 00);

            var localTime = dateTime.ToTimeZone("Central Standard Time");

            Assert.Equal(new DateTime(2021, 12, 31, 18, 00, 00), localTime);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromUtc_ToTimeZone_ReturnsLocalWithCulture()
        {
            var dateTimeUtc = new DateTime(2022, 07, 01, 12, 00, 00);

            var localTime = dateTimeUtc.ToTimeZoneWithCulture("Central Standard Time", "en-US");

            Assert.Equal("7/1/2022 7:00:00 AM", localTime);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromUtc_ToTimeZone_ReturnsLocalWithCulture2()
        {
            var dateTimeUtc = new DateTime(2022, 07, 01, 12, 00, 00);

            var localTime = dateTimeUtc.ToTimeZoneWithCulture("Central Standard Time", "tr-TR");

            Assert.Equal("1.07.2022 07:00:00", localTime);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromUtc_ToTimeZone_ReturnsLocalWithCulture3()
        {
            var dateTimeUtc = new DateTime(2022, 07, 01, 12, 00, 00);

            var localTime = dateTimeUtc.ToTimeZoneWithCulture("Central Standard Time");

            Assert.Equal("7/1/2022 7:00:00 AM", localTime);
        }

        [Fact]
        [Trait("Category", "DateTimeExtensions")]
        public void FromUtc_ToTimeZone_ReturnsUtcForError()
        {
            var dateTime = new DateTime(2022, 01, 01, 12, 00, 00);

            Assert.Throws<TimeZoneNotFoundException>(() => dateTime.ToTimeZone("Somewhere Standard Time"));
        }
    }
}