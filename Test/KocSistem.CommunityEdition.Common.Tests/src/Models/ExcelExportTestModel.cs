﻿// <copyright file="ExcelExportTestModel.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Common.Tests.Models
{
    public class ExcelExportTestModel
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Phone { get; set; }
    }
}
