﻿// -----------------------------------------------------------------------
// <copyright file="ExcelExportHelperTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
// -----------------------------------------------------------------------

using ClosedXML.Excel;
using KocSistem.CommunityEdition.Common.Helpers.ExcelExport;
using KocSistem.CommunityEdition.Common.Tests.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Xunit;
using ClosedXML.Graphics;

namespace KocSistem.CommunityEdition.Common.Tests
{
    public class ExcelExportHelperTest
    {
        private readonly Mock<IConfiguration> _configurationMoq;

        public ExcelExportHelperTest()
        {
            _configurationMoq = new Mock<IConfiguration>();
        }

        [Fact]
        [Trait("Category", "ExcelExportHelper")]
        public byte[] ExportExcel_GenericModel()
        {
            var model = new List<ExcelExportTestModel>()
            {
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                },
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                }
            };

            Assert.True(true);

            return new Byte[64];
        }

        [Fact]
        [Trait("Category", "ExcelExportHelper")]
        public byte[] ExportExcel_GenericType()
        {
            var model = new List<ExcelExportTestModel>()
            {
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                },
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                }
            };

            Assert.True(true);

            return new Byte[64];
        }

        [Fact]
        [Trait("Category", "ExcelExportHelper")]
        public byte[] ExportExcel()
        {
            LoadOptions.DefaultGraphicEngine = new DefaultGraphicEngine("DejaVu Sans");
            if (OperatingSystem.IsWindows())
            {
                LoadOptions.DefaultGraphicEngine = new DefaultGraphicEngine("Arial");
            }
            else if (OperatingSystem.IsMacOS())
            {
                LoadOptions.DefaultGraphicEngine = new DefaultGraphicEngine("Helvetica");
            }

            var dataTable = new ExcelDataTable();
            var showRowNo = true;
            var columnsToTake = Array.Empty<string>();

            byte[] result = null;
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add(dataTable, "Shhet1");

                foreach (var item in worksheet.Tables)
                {
                    item.Theme = XLTableTheme.None;
                }

                if (showRowNo)
                {
                    dataTable = new ExcelDataTable();
                }

                var cells = worksheet.Range(1, 1, 1, columnsToTake.Length);

                cells.CellsUsed().ToList().ForEach(x => { x.Style.Font.Bold = true; });

                _ = worksheet.Columns().AdjustToContents();

                foreach (var item in worksheet.CellsUsed().ToList())
                {
                    if (!item.Value.IsBlank && !item.Value.IsError && item.Value.GetType() == typeof(DateTime))
                    {
                        item.Style.NumberFormat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                    }
                }

                using (var msA = new MemoryStream())
                {
                    workbook.SaveAs(msA);
                    result = msA.ToArray();
                }
            }

            Assert.True(true);

            return result;
        }

        [Fact]
        [Trait("Category", "ExcelExportHelper")]
        public string[] GetColumnHeaders_GenericModel()
        {
            var model = new List<ExcelExportTestModel>()
            {
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                },
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                }
            };

            var properties = TypeDescriptor.GetProperties(model);
            var columnHeaders = new List<string>();
            foreach (PropertyDescriptor property in properties)
            {
                if (!string.IsNullOrEmpty(property.DisplayName) && property.SerializationVisibility != DesignerSerializationVisibility.Hidden)
                {
                    columnHeaders.Add(property.DisplayName);
                }
            }
            Assert.True(true);

            return columnHeaders.ToArray();
        }

        [Trait("Category", "ExcelExportHelper")]
        public static string[] GetColumnHeaders_GenericType<TKey, TValue>(Dictionary<TKey, TValue> data)
        {
            var columnHeaders = new List<string>();
            for (var i = 0; i < data.Count; i++)
            {
                columnHeaders.Add(data.ElementAt(i).Key.ToString());
            }

            Assert.True(true);

            return columnHeaders.ToArray();
        }

        [Fact]
        [Trait("Category", "ExcelExportHelper")]
        public ExcelDataTable AddRowNumbers()
        {
            var dataTable = new ExcelDataTable();

            dataTable.Clear();
            _ = dataTable.Columns.Add("Name");
            object[] o = { "Test" };
            _ = dataTable.Rows.Add(o);

            var dataColumn = dataTable.Columns.Add("#", typeof(int));
            dataColumn.SetOrdinal(0);
            var index = 1;
            foreach (DataRow item in dataTable.Rows)
            {
                index++;
            }

            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                row[0] = index;
            }

            Assert.True(true);

            return dataTable;
        }

        [Fact]
        [Trait("Category", "ExcelExportHelper")]
        public IXLWorksheet RemoveColumns()
        {
            var dataTable = new ExcelDataTable();

            var columnsToTake = Array.Empty<string>();
            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add(dataTable, "Sheet1");

            var finishIndex = true ? 1 : 0;
            for (var i = dataTable.Columns.Count - 1; i >= finishIndex; i--)
            {
                if (!columnsToTake.Contains(dataTable.Columns[i].ColumnName))
                {
                    worksheet.Columns(i, i).Delete();
                }
            }

            Assert.True(true);

            return worksheet;
        }

        [Fact]
        [Trait("Category", "ExcelExportHelper")]
        public ExcelDataTable ToTheDataTable_GenericModel()
        {
            var model = new List<ExcelExportTestModel>()
            {
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                },
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                }
            };

            var properties = TypeDescriptor.GetProperties(typeof(ExcelExportTestModel));

            var excelDataTable = new ExcelDataTable();

            foreach (PropertyDescriptor property in properties)
            {
                _ = excelDataTable.Columns.Add(property.DisplayName, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
            }

            var values = new object[properties.Count];

            foreach (var item in model)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }

                _ = excelDataTable.Rows.Add(values);
            }

            Assert.True(true);

            return excelDataTable;
        }

        [Fact]
        [Trait("Category", "ExcelExportHelper")]
        public ExcelDataTable ToTheDataTable_GenericType()
        {
            var model = new List<ExcelExportTestModel>()
            {
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                },
                new ExcelExportTestModel()
                {
                    Name = "Name",
                    Surname = "Surname",
                    Phone = "Phone"
                }
            };

            var properties = TypeDescriptor.GetProperties(typeof(ExcelExportTestModel));

            var excelDataTable = new ExcelDataTable();

            foreach (PropertyDescriptor property in properties)
            {
                _ = excelDataTable.Columns.Add(property.DisplayName, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
            }

            var values = new object[properties.Count];

            foreach (var item in model)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }

                _ = excelDataTable.Rows.Add(values);
            }

            Assert.True(true);
            return excelDataTable;
        }
    }
}