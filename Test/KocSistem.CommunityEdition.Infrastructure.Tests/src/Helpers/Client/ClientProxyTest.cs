﻿// <copyright file="ClientProxyTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers.Client;
using KocSistem.OneFrame.Common.Proxy;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Infrastructure.Tests.Helpers.Client
{
    public class ClientProxyTest
    {
        private readonly Mock<IConfiguration> _configurationMoq;
        private readonly DefaultHttpContext _context;
        private readonly Mock<IHttpClientFactory> _httpClientFactoryMoq;
        private readonly Mock<IHttpContextAccessor> _httpContextAccessorMoq;
        private readonly Mock<IProxyHelper> _proxyHelperMoq;
        private readonly Mock<IServiceProvider> _serviceProviderMoq;

        public ClientProxyTest()
        {
            _configurationMoq = new Mock<IConfiguration>();
            _context = new DefaultHttpContext();
            _httpClientFactoryMoq = new Mock<IHttpClientFactory>();
            _httpContextAccessorMoq = new Mock<IHttpContextAccessor>();
            _proxyHelperMoq = new Mock<IProxyHelper>();
            _serviceProviderMoq = new Mock<IServiceProvider>();

            _ = _httpContextAccessorMoq.Setup(r => r.HttpContext).Returns(_context);
            _ = _serviceProviderMoq.Setup(s => s.GetService(It.IsAny<Type>())).Returns(_httpClientFactoryMoq.Object);
            _ = _httpContextAccessorMoq.Setup(r => r.HttpContext.RequestServices).Returns(_serviceProviderMoq.Object);
        }

        [Fact]
        public void GetJwtIssuerHttpClient_ReturnsHttpClient()
        {
            var clientProxy = new ClientProxy(_httpContextAccessorMoq.Object, _configurationMoq.Object, _proxyHelperMoq.Object);
            _ = _httpClientFactoryMoq.Setup(r => r.CreateClient(It.IsAny<string>())).Returns(new HttpClient());

            var result = clientProxy.GetJwtIssuerHttpClient();
            _ = Assert.IsAssignableFrom<HttpClient>(result);
        }

        [Fact]
        public async Task AccessTokenRefreshWrapper_StatusCodeOK_Returns()
        {
            _ = _httpContextAccessorMoq.Setup(r => r.HttpContext.Request.Headers).Returns(new HeaderDictionary());
            var clientProxy = new ClientProxy(_httpContextAccessorMoq.Object, _configurationMoq.Object, _proxyHelperMoq.Object);

            var result = await clientProxy.AccessTokenRefreshWrapper<object>(() => Task.FromResult(new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent(@"{'test':'test'}") }), true, true, true).ConfigureAwait(false);
            _ = Assert.IsAssignableFrom<object>(result);
        }
    }
}