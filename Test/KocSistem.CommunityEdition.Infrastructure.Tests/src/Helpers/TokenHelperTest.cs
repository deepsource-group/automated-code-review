﻿// <copyright file="TokenHelperTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Helpers;
using KocSistem.OneFrame.Authentication.Interfaces;
using KocSistem.OneFrame.Authentication.Utilities;
using KocSistem.OneFrame.DesignObjects.Models;
using KocSistem.OneFrame.DesignObjects.Services;
using KocSistem.OneFrame.I18N;
using Microsoft.Extensions.Localization;
using Moq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Xunit;

namespace KocSistem.CommunityEdition.Infrastructure.Tests.Helpers
{
    public class TokenHelperTest
    {
        private readonly Mock<IKsI18N> _localizationMoq;
        private readonly Mock<IServiceResponseHelper> _serviceResponseHelperMoq;
        private readonly Mock<ITokenBuilderService> _tokenBuilderServiceMoq;

        public TokenHelperTest()
        {
            _serviceResponseHelperMoq = new Mock<IServiceResponseHelper>();
            _tokenBuilderServiceMoq = new Mock<ITokenBuilderService>();

            _localizationMoq = new Mock<IKsI18N>();
            _ = _localizationMoq.SetupGet(x => x.GetLocalizer<TokenHelper>()[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
        }

        [Fact]
        [Trait("Category", "Helpers")]
        public void BuildToken_Success_ReturnsIsSuccessfulTrue()
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            var token = "anyToken";
            var accessToken = new AccessToken() { Token = token, RefreshToken = token };
            var tokenHelper = CreateTokenHelper();
            _ = _tokenBuilderServiceMoq.Setup(s => s.BuildToken(claims, null, null)).Returns(new ServiceResponse<AccessToken>(accessToken));
            _ = _serviceResponseHelperMoq.Setup(s => s.SetSuccess(It.IsAny<AccessToken>())).Returns(new ServiceResponse<AccessToken>(accessToken));

            var ret = tokenHelper.BuildToken(claims);

            Assert.True(ret.IsSuccessful);
            Assert.Equal(token, ret.Result.Token);
        }

        [Fact]
        [Trait("Category", "Helpers")]
        public void BuildToken_AccessTokenUnsuccessful_ReturnsIsSuccessfulFalse()
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            var tokenHelper = CreateTokenHelper();
            _ = _tokenBuilderServiceMoq.Setup(s => s.BuildToken(claims, null, null)).Returns(new ServiceResponse<AccessToken>((AccessToken)null, false));
            _ = _serviceResponseHelperMoq.Setup(s => s.SetError((AccessToken)null, It.IsAny<ErrorInfo>(), false)).Returns(new ServiceResponse<AccessToken>((AccessToken)null, false));

            var ret = tokenHelper.BuildToken(claims);

            Assert.False(ret.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "Helpers")]
        public void ValidateRefreshToken_InvalidToken_ReturnsIsSuccessfulFalse()
        {
            var tokenHelper = CreateTokenHelper();
            _ = _tokenBuilderServiceMoq.Setup(s => s.ValidateRefreshToken(It.IsAny<string>())).Returns(new ServiceResponse<AccessToken>((AccessToken)null, new ErrorInfo("errorMessage"), false));
            _ = _serviceResponseHelperMoq.Setup(s => s.SetError<AccessToken>(null, It.IsAny<ErrorInfo>(), true)).Returns(new ServiceResponse<AccessToken>(new AccessToken(), new ErrorInfo()));

            var ret = tokenHelper.ValidateRefreshToken("anyToken");

            Assert.False(ret.IsSuccessful);
        }

        [Fact]
        [Trait("Category", "Helpers")]
        public void ValidateRefreshToken_ValidToken_ReturnsIsSuccessfulTrue()
        {
            var tokenHelper = CreateTokenHelper();
            _ = _tokenBuilderServiceMoq.Setup(s => s.ValidateRefreshToken(It.IsAny<string>())).Returns(new ServiceResponse<AccessToken>(new AccessToken() { Token = "token", RefreshToken = "refreshToken" }));

            var ret = tokenHelper.ValidateRefreshToken("anyToken");

            Assert.Equal("token", ret.Result.Token);
        }

        private TokenHelper CreateTokenHelper()
        {
            return new TokenHelper(_localizationMoq.Object, _serviceResponseHelperMoq.Object, _tokenBuilderServiceMoq.Object);
        }
    }
}