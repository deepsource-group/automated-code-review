﻿// <copyright file="CustomPasswordValidatorTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.Infrastructure.Helpers.Identity;
using KocSistem.OneFrame.I18N;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using Moq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace KocSistem.CommunityEdition.Infrastructure.Tests.Helpers.Identity
{
    public class CustomPasswordValidatorTest
    {
        private readonly Mock<IKsStringLocalizer<object>> _localizationMoq;
        private readonly Mock<IKsI18N> _iKsI18Nmoq;

        public CustomPasswordValidatorTest()
        {
            _localizationMoq = new Mock<IKsStringLocalizer<object>>();
            _iKsI18Nmoq = new Mock<IKsI18N>();

            _ = _localizationMoq.SetupGet(s => s[It.IsAny<string>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = _localizationMoq.SetupGet(s => s[It.IsAny<string>(), It.IsAny<object[]>()]).Returns(new LocalizedString("sometext", "sometext"));
            _ = _iKsI18Nmoq.Setup(r => r.GetLocalizer<object>()).Returns(_localizationMoq.Object);
        }

        [Fact]
        public async Task ValidateAsync_ReturnIdentityResultSuccess()
        {
            var password = "testPassword";
            var userManagerMoq = new Mock<IUserStore<ApplicationUser>>();
            _ = userManagerMoq.Setup(r => r.GetUserNameAsync(It.IsAny<ApplicationUser>(), It.IsAny<CancellationToken>())).ReturnsAsync("username");
            var userManager = new UserManager<ApplicationUser>(userManagerMoq.Object, null, null, null, null, null, null, null, null);
            var customPasswordValidator = new CustomPasswordValidator<ApplicationUser>(_localizationMoq.Object);

            var result = await customPasswordValidator.ValidateAsync(userManager, new ApplicationUser(), password).ConfigureAwait(false);
            Assert.True(result.Succeeded);
        }

        [Fact]
        public async Task ValidateAsync_UsernameEqualPassword_ReturnIdentityResultFailed()
        {
            var password = "username";
            var userManagerMoq = new Mock<IUserStore<ApplicationUser>>();
            _ = userManagerMoq.Setup(r => r.GetUserNameAsync(It.IsAny<ApplicationUser>(), It.IsAny<CancellationToken>())).ReturnsAsync("username");
            var userManager = new UserManager<ApplicationUser>(userManagerMoq.Object, null, null, null, null, null, null, null, null);
            var customPasswordValidator = new CustomPasswordValidator<ApplicationUser>(_localizationMoq.Object);

            var result = await customPasswordValidator.ValidateAsync(userManager, new ApplicationUser(), password).ConfigureAwait(false);
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task ValidateAsync_UsernameEqualnonAllowedPasswordList_ReturnIdentityResultFailed()
        {
            var password = "123456";
            var userManagerMoq = new Mock<IUserStore<ApplicationUser>>();
            _ = userManagerMoq.Setup(r => r.GetUserNameAsync(It.IsAny<ApplicationUser>(), It.IsAny<CancellationToken>())).ReturnsAsync("username");
            var userManager = new UserManager<ApplicationUser>(userManagerMoq.Object, null, null, null, null, null, null, null, null);
            var customPasswordValidator = new CustomPasswordValidator<ApplicationUser>(_localizationMoq.Object);

            var result = await customPasswordValidator.ValidateAsync(userManager, new ApplicationUser(), password).ConfigureAwait(false);
            Assert.False(result.Succeeded);
        }

        [Fact]
        public async Task ValidateAsync_PasswordNull_ReturnIdentityResultFailed()
        {
            string password = null;
            var userManagerMoq = new Mock<IUserStore<ApplicationUser>>();
            _ = userManagerMoq.Setup(r => r.GetUserNameAsync(It.IsAny<ApplicationUser>(), It.IsAny<CancellationToken>())).ReturnsAsync("username");
            var userManager = new UserManager<ApplicationUser>(userManagerMoq.Object, null, null, null, null, null, null, null, null);
            var customPasswordValidator = new CustomPasswordValidator<ApplicationUser>(_localizationMoq.Object);

            var result = await customPasswordValidator.ValidateAsync(userManager, new ApplicationUser(), password).ConfigureAwait(false);
            Assert.False(result.Succeeded);
        }
    }
}