﻿// <copyright file="ActionExecutionExtensionsTest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Infrastructure.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace KocSistem.CommunityEdition.Infrastructure.Tests.Extensions
{
    public class ActionExecutionExtensionsTest
    {
        [Fact]
        [Trait("Category", "Extensions")]
        public void ValidateModel_ContextNotNull_ReturnsList()
        {
            var httpContext = new DefaultHttpContext();
            var context = new ActionExecutingContext(new ActionContext(httpContext, new RouteData(), new ActionDescriptor()), new List<IFilterMetadata>(), new Dictionary<string, object>(), new object());
            var result = context.ValidateModel();
            _ = Assert.IsAssignableFrom<List<ValidationResult>>(result);
        }

        [Fact]
        [Trait("Category", "Extensions")]
        public void ValidateModel_ContextNull_Throws()
        {
            ActionExecutingContext context = null;
            _ = Assert.Throws<ArgumentNullException>(() => context.ValidateModel());
        }
    }
}
