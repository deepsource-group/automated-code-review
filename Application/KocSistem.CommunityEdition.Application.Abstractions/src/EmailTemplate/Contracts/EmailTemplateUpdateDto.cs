﻿// <copyright file="EmailTemplateUpdateDto.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
using KocSistem.CommunityEdition.Application.Abstractions.EmailTemplateTranslation.Contracts;
using KocSistem.OneFrame.Data.Relational;
using System;
using System.Collections.Generic;

namespace KocSistem.CommunityEdition.Application.Abstractions.EmailTemplate.Contracts
{
    public class EmailTemplateUpdateDto : IUpdateAuditing
    {
        public Guid Id { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string To { get; set; }

        public string Cc { get; set; }

        public string Bcc { get; set; }

        public List<EmailTemplateTranslationDto> Translations { get; set; }
    }
}
