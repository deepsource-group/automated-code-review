﻿// <copyright file="ILanguageService.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Language.Contracts;
using KocSistem.OneFrame.Data.Relational;
using KocSistem.OneFrame.DesignObjects;
using KocSistem.OneFrame.DesignObjects.Services;
using System;
using System.Threading.Tasks;

namespace KocSistem.CommunityEdition.Application.Abstractions.Language
{
    public interface ILanguageService : IApplicationCrudServiceAsync<Domain.Language, LanguageDto, Guid>, IApplicationService
    {
        Task<ServiceResponse<PagedResultDto<LanguageDto>>> GetLanguageListAsync(PagedRequestDto pagedRequest);

        Task<ServiceResponse<PagedResultDto<LanguageDto>>> SearchAsync(LanguageSearchDto languageGetRequest);
    }
}
