﻿// <copyright file="ApplicationSettingSearchDto.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;

namespace KocSistem.CommunityEdition.Application.Abstractions.ApplicationSetting.Contracts
{
    public class ApplicationSettingSearchDto : PagedRequestDto
    {
        public string Key { get; set; }
    }
}
