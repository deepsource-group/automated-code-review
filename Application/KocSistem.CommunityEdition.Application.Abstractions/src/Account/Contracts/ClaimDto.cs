﻿// <copyright file="ClaimDto.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Application.Abstractions.Account.Contracts
{
    public class ClaimDto
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}