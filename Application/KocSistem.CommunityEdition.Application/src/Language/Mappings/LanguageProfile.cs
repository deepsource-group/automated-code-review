﻿// <copyright file="LanguageProfile.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.Common.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.Language.Contracts;

using KocSistem.OneFrame.Data.Relational;

namespace KocSistem.CommunityEdition.Application.Language.Mappings
{
    /// <summary>
    ///  Definition Language Entity AutoMapper Profiles.
    /// </summary>
    /// <seealso cref="Profile" />
    public class LanguageProfile : Profile
    {
        public LanguageProfile()
        {
            this.CreateMap<Domain.Language, LanguageDto>().ReverseMap();

            this.CreateMap<IPagedList<Domain.Language>, PagedResultDto<LanguageDto>>().ReverseMap();
        }
    }
}
