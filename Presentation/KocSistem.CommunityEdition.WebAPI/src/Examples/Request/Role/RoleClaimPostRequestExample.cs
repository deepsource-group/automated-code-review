﻿// <copyright file="RoleClaimPostRequestExample.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.WebAPI.Model.Role;
using Swashbuckle.AspNetCore.Filters;
using System.Diagnostics.CodeAnalysis;

namespace KocSistem.CommunityEdition.WebAPI.Examples.Request
{
    [ExcludeFromCodeCoverage(Justification = "not necessary")]
    [SuppressMessage("Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "for SwaggerUI")]
    internal class RoleClaimPostRequestExample : IExamplesProvider<RoleClaimPostRequest>
    {
        public RoleClaimPostRequest GetExamples()
        {
            return new RoleClaimPostRequest
            {
                Name = "Claim Name",
            };
        }
    }
}