﻿// <copyright file="UserProfile.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using AutoMapper;
using KocSistem.CommunityEdition.Application.Abstractions.User.Contracts;
using KocSistem.CommunityEdition.Application.Abstractions.UserConfirmationHistory.Contract;
using KocSistem.CommunityEdition.Domain;
using KocSistem.CommunityEdition.WebAPI.Model.Paging;
using KocSistem.CommunityEdition.WebAPI.Model.User;
using KocSistem.OneFrame.Data.Relational;

namespace KocSistem.CommunityEdition.WebAPI.Mappings
{
    /// <summary>
    ///  Definition ApplicationUser Entity And UserDto AutoMapper Profiles.
    /// </summary>
    /// <seealso cref="Profile" />
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class UserProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserProfile"/> class.
        /// </summary>
        public UserProfile()
        {
            _ = CreateMap<ApplicationUser, UserGetResponse>().ReverseMap();
            _ = CreateMap<UserDto, UserGetResponse>().ReverseMap();
            _ = CreateMap<IPagedList<ApplicationUser>, PagedResult<UserGetResponse>>().ReverseMap();
            _ = CreateMap<ApplicationUser, UserRoleInfoResponse>().ReverseMap();
            _ = CreateMap<UserDto, BasicUserInfoResponse>().ReverseMap();
            _ = CreateMap<UserDto, UserInRoleResponse>().ReverseMap();

            _ = CreateMap<UserConfirmationHistoryDto, ConfirmationCodeResponse>().ReverseMap();
            _ = CreateMap<UserConfirmationHistoryDto, ConfirmationCodeRequest>().ReverseMap();
        }
    }
}