﻿// <copyright file="RoleGetResponse.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.WebAPI.Model.Role
{
    public class RoleGetResponse
    {
        public string Description { get; set; }

        public string DisplayText { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }
    }
}
