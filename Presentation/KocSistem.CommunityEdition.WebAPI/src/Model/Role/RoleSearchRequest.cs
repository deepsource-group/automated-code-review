﻿// <copyright file="RoleSearchRequest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.WebAPI.Model.Paging;

namespace KocSistem.CommunityEdition.WebAPI.Model.Role
{
    public class RoleSearchRequest : PagedRequest
    {
        public string Name { get; set; }
    }
}
