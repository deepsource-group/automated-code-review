﻿// <copyright file="EmailTemplatePostRequest.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using System.ComponentModel.DataAnnotations;

namespace KocSistem.CommunityEdition.WebAPI.Model.EmailTemplate
{
    public class EmailTemplatePostRequest
    {
        [Required]
        public string Name { get; set; }

        public string To { get; set; }

        public string Cc { get; set; }

        public string Bcc { get; set; }

        public bool IsDeleted { get; set; }
    }
}