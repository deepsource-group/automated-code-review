﻿// <copyright file="EmailNotificationController.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

using KocSistem.CommunityEdition.Common.Authentication;
using KocSistem.CommunityEdition.Mvc.Controllers;
using KocSistem.CommunityEdition.Mvc.Helpers;
using KocSistem.CommunityEdition.Mvc.Models.DataTables;
using KocSistem.CommunityEdition.Mvc.Models.Paging;
using KocSistem.OneFrame.DesignObjects.Services;
using Microsoft.AspNetCore.Authorization;

namespace KocSistem.CommunityEdition.Mvc
{
    [Route("email-notifications")]
    public class EmailNotificationController : BaseController<EmailNotificationController>
    {
        private readonly IKsStringLocalizer<EmailNotificationController> _ksStringLocalizer;

        public EmailNotificationController(IKsI18N i18N)
             : base(i18N)
        {
            _ksStringLocalizer = i18N.GetLocalizer<EmailNotificationController>();
        }

        [HttpGet]
        [Authorize(Policy = KsPermissionPolicy.ReportEmailNotificationList)]
        public IActionResult Get()
        {
            return View();
        }

        [HttpGet("list")]
        [Authorize(Policy = KsPermissionPolicy.ReportEmailNotificationList)]
        public async Task<IActionResult> GetListAsync([DataTablesRequest] DataTablesRequest request)
        {
            var pagedRequest = GetPagedRequest(request);

            if (!string.IsNullOrEmpty(request.Search?.Value))
            {
                var emailNotificationSearchRequest = ConvertToEmailNotificationSearchRequest(request);
                return await SearchAsync(emailNotificationSearchRequest).ConfigureAwait(false);
            }

            var response = await GetApiRequestWithCookiesAsync<ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>>(ApiEndpoints.EmailNotificationPagedList, pagedRequest).ConfigureAwait(false);

            if (!response.IsSuccessful)
            {
                return ToastError(response.Error);
            }

            return Ok(JsonDataTable(response.Result));
        }

        [HttpGet("search")]
        [Authorize(Policy = KsPermissionPolicy.ReportEmailNotificationList)]
        public async Task<IActionResult> SearchAsync([FromQuery] EmailNotificationSearchRequest searchRequest)
        {
            var response = await GetApiRequestAsync<ServiceResponse<PagedResult<EmailNotificationGetResponseModel>>>(ApiEndpoints.EmailNotificationSearch, searchRequest).ConfigureAwait(false);

            if (!response.IsSuccessful)
            {
                return ToastError(response.Error);
            }

            return Ok(JsonDataTable(response.Result));
        }

        [HttpGet("send")]
        [Authorize(Policy = KsPermissionPolicy.ReportEmailNotificationSend)]
        public async Task<IActionResult> SendEmailByIdAsync(Guid id)
        {
            var response = await GetApiRequestAsync<ServiceResponse<bool>>(string.Format(ApiEndpoints.EmailNotificationSend, id)).ConfigureAwait(false);

            if (!response.IsSuccessful)
            {
                return ToastError(response.Error);
            }

            return ToastSuccessForRedirect(_ksStringLocalizer["EmailNotificationSendSuccess"], MvcEndpoints.EmailNotificationBaseRoute);
        }

        private static EmailNotificationSearchRequest ConvertToEmailNotificationSearchRequest(DataTablesRequest requestModel)
        {
            var emailNotificationSearchRequest = new EmailNotificationSearchRequest()
            {
                Value = requestModel.Search.Value,
                PageIndex = requestModel.Start / requestModel.Length,
                PageSize = requestModel.Length,
            };
            return emailNotificationSearchRequest;
        }
    }
}
