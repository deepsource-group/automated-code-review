﻿// <copyright file="ApplicationCookiePolicy.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Mvc.Models.Other
{
    public class ApplicationCookiePolicy
    {
        public bool Required { get; set; }

        public bool ThirdPartyPartners { get; set; }
    }
}
