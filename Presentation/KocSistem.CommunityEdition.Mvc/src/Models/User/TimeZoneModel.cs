﻿// <copyright file="TimeZoneModel.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Mvc.Models.User
{
    public class TimeZoneModel
    {
        public string Id { get; set; }

        public string DisplayName { get; set; }
    }
}
