﻿// <copyright file="ProfilePhotoModel.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Mvc.Models.Profile
{
    public class ProfilePhotoModel
    {
        public string Photo { get; set; }

        public int Size { get; set; }
    }
}