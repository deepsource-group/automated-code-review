﻿// <copyright file="ApplicationPolicyType.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Common.Authentication
{
    public static class ApplicationPolicyType
    {
        /// <summary>
        /// Generic KS Permission.
        /// </summary>
        public const string KsPermission = "KsPermission";
    }
}
