﻿// <copyright file="ITranslateResolver.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Common.Helpers.AutoMapper
{
    /// <summary>
    /// Translate Resolver interface.
    /// </summary>
    public interface ITranslateResolver
    {
    }
}
