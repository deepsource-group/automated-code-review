﻿// <copyright file="FileConstants.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>

namespace KocSistem.CommunityEdition.Application.Constants
{
    public static class FileConstants
    {
        public const string ExcelFileName = "LoginAuditLog.xlsx";
        public const string PdfFileName = "LoginAuditLog.pdf";
        public const string PdfTitle = "Audit Log";
        public const string PdfDefaultEncodingType = "CP1254";
    }
}
