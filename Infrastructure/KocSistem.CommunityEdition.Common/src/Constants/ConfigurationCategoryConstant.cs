﻿// <copyright file="ConfigurationCategoryConstant.cs" company="KocSistem">
// Copyright (c) KocSistem. All rights reserved.
// Licensed under the Proprietary license. See LICENSE file in the project root for full license information.
// </copyright>
namespace KocSistem.CommunityEdition.Common.Constants
{
    public static class ConfigurationCategoryConstant
    {
        public const string SystemShared = "system-shared";
        public const string SystemWebApi = "system-web-api";
        public const string SystemMvcUi = "system-mvc-ui";
        public const string SystemReact = "system-react";
    }
}